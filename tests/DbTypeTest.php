<?php
// DbTypeTest.php
// Created: 2021-04-28
// Updated: 2025-01-18

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\Db\{DbTools,DbType};

#[CoversClass(DbTools::class)]
#[CoversClass(DbType::class)]
final class DbTypeTest extends TestCase {
    public function testDetectType(): void {
        $this->assertEquals(DbType::Null, DbTools::detectType(null));
        $this->assertEquals(DbType::Int, DbTools::detectType(12345));
        $this->assertEquals(DbType::Float, DbTools::detectType(123.45));
        $this->assertEquals(DbType::String, DbTools::detectType('This is a string.'));

        $blob = fopen('php://memory', 'r+b');
        if($blob === false)
            throw new RuntimeException('failed to fopen a memory stream');

        fwrite($blob, 'This is a string inside a memory stream.');
        fseek($blob, 0);

        $this->assertEquals(DbType::Blob, DbTools::detectType($blob));
    }
}

<?php
// CacheBackendsTest.php
// Created: 2024-04-10
// Updated: 2025-01-18

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\{CoversClass,UsesClass};
use Index\Cache\CacheBackends;
use Index\Cache\ArrayCache\{ArrayCacheBackend,ArrayCacheProvider};
use Index\Cache\Memcached\MemcachedBackend;
use Index\Cache\Valkey\ValkeyBackend;
use Index\Net\{DnsEndPoint,IpEndPoint,UnixEndPoint};

#[CoversClass(CacheBackends::class)]
#[CoversClass(ArrayCacheBackend::class)]
#[CoversClass(ArrayCacheProvider::class)]
#[CoversClass(MemcachedBackend::class)]
#[CoversClass(ValkeyBackend::class)]
#[UsesClass(DnsEndPoint::class)]
#[UsesClass(IpEndPoint::class)]
#[UsesClass(UnixEndPoint::class)]
final class CacheBackendsTest extends TestCase {
    public function testBasicDSN(): void {
        $arrayCache = CacheBackends::create('array:');
        $this->assertInstanceOf(ArrayCacheProvider::class, $arrayCache);
    }

    public function testMemcachedDSN(): void {
        $memcached = new MemcachedBackend;

        // unix socket
        $info = $memcached->parseDsn('memcached://:unix:/prefix/test/?server=/var/run/memcached.sock');

        $this->assertEquals(1, count($info->endPoints));
        $this->assertInstanceOf(UnixEndPoint::class, $info->endPoints[0][0]);
        $this->assertEquals('/var/run/memcached.sock', $info->endPoints[0][0]->path);
        $this->assertEquals(0, $info->endPoints[0][1]);
        $this->assertEquals('prefix:test:', $info->prefixKey);
        $this->assertFalse($info->persistent);
        $this->assertNull($info->persistentName);
        $this->assertTrue($info->useBinaryProto);
        $this->assertTrue($info->enableCompression);
        $this->assertTrue($info->tcpNoDelay);

        // ipv4
        $info = $memcached->parseDsn('memcached://127.0.0.1?compress=no&nodelay=off&persist=test');

        $this->assertEquals(1, count($info->endPoints));
        $this->assertInstanceOf(IpEndPoint::class, $info->endPoints[0][0]);
        $this->assertEquals('127.0.0.1', (string)$info->endPoints[0][0]->address);
        $this->assertEquals(0, $info->endPoints[0][0]->port);
        $this->assertEquals(0, $info->endPoints[0][1]);
        $this->assertEquals('', $info->prefixKey);
        $this->assertTrue($info->persistent);
        $this->assertEquals('test', $info->persistentName);
        $this->assertTrue($info->useBinaryProto);
        $this->assertFalse($info->enableCompression);
        $this->assertFalse($info->tcpNoDelay);

        // ipv6
        $info = $memcached->parseDsn('memcached://[::1]:9001?proto=ascii');

        $this->assertEquals(1, count($info->endPoints));
        $this->assertInstanceOf(IpEndPoint::class, $info->endPoints[0][0]);
        $this->assertEquals('::1', (string)$info->endPoints[0][0]->address);
        $this->assertEquals(9001, $info->endPoints[0][0]->port);
        $this->assertEquals(0, $info->endPoints[0][1]);
        $this->assertEquals('', $info->prefixKey);
        $this->assertFalse($info->persistent);
        $this->assertNull($info->persistentName);
        $this->assertFalse($info->useBinaryProto);
        $this->assertTrue($info->enableCompression);
        $this->assertTrue($info->tcpNoDelay);

        // dns
        $info = $memcached->parseDsn('memcache://localhost');

        $this->assertEquals(1, count($info->endPoints));
        $this->assertInstanceOf(DnsEndPoint::class, $info->endPoints[0][0]);
        $this->assertEquals('localhost', $info->endPoints[0][0]->host);
        $this->assertEquals(0, $info->endPoints[0][0]->port);
        $this->assertEquals(0, $info->endPoints[0][1]);
        $this->assertEquals('', $info->prefixKey);
        $this->assertFalse($info->persistent);
        $this->assertNull($info->persistentName);
        $this->assertTrue($info->useBinaryProto);
        $this->assertTrue($info->enableCompression);
        $this->assertTrue($info->tcpNoDelay);

        // pool
        $info = $memcached->parseDsn('memcached://:pool:/?server[]=/var/run/memcached.sock;20&server[]=127.0.0.1;10&server[]=[::1]:9001;5&server[]=localhost');

        $this->assertEquals(4, count($info->endPoints));

        $this->assertInstanceOf(UnixEndPoint::class, $info->endPoints[0][0]);
        $this->assertEquals('/var/run/memcached.sock', $info->endPoints[0][0]->path);
        $this->assertEquals(20, $info->endPoints[0][1]);

        $this->assertInstanceOf(IpEndPoint::class, $info->endPoints[1][0]);
        $this->assertEquals('127.0.0.1', (string)$info->endPoints[1][0]->address);
        $this->assertEquals(0, $info->endPoints[1][0]->port);
        $this->assertEquals(10, $info->endPoints[1][1]);

        $this->assertInstanceOf(IpEndPoint::class, $info->endPoints[2][0]);
        $this->assertEquals('::1', (string)$info->endPoints[2][0]->address);
        $this->assertEquals(9001, $info->endPoints[2][0]->port);
        $this->assertEquals(5, $info->endPoints[2][1]);

        $this->assertInstanceOf(DnsEndPoint::class, $info->endPoints[3][0]);
        $this->assertEquals('localhost', $info->endPoints[3][0]->host);
        $this->assertEquals(0, $info->endPoints[3][0]->port);
        $this->assertEquals(0, $info->endPoints[3][1]);

        $this->assertEquals('', $info->prefixKey);
        $this->assertFalse($info->persistent);
        $this->assertNull($info->persistentName);
        $this->assertTrue($info->useBinaryProto);
        $this->assertTrue($info->enableCompression);
        $this->assertTrue($info->tcpNoDelay);
    }

    public function testValkeyDSN(): void {
        $valkey = new ValkeyBackend;

        // unix socket
        $info = $valkey->parseDsn('valkey://:unix:/prefix/test/?socket=/var/run/valkey.sock');

        $this->assertInstanceOf(UnixEndPoint::class, $info->endPoint);
        $this->assertEquals('/var/run/valkey.sock', $info->serverHost);
        $this->assertEquals(0, $info->serverPort);
        $this->assertEquals('prefix:test:', $info->prefix);
        $this->assertFalse($info->persist);
        $this->assertEmpty($info->username);
        $this->assertEmpty($info->password);
        $this->assertEquals(0, $info->dbNumber);

        // ipv4
        $info = $valkey->parseDsn('keydb://password@127.0.0.1:6379?db=123');

        $this->assertInstanceOf(IpEndPoint::class, $info->endPoint);
        $this->assertEquals('127.0.0.1', $info->serverHost);
        $this->assertEquals(6379, $info->serverPort);
        $this->assertEmpty($info->prefix);
        $this->assertFalse($info->persist);
        $this->assertEmpty($info->username);
        $this->assertEquals('password', $info->password);
        $this->assertEquals(123, $info->dbNumber);

        // ipv6
        $info = $valkey->parseDsn('redis://username:password@[::1]/?persist&db=1');

        $this->assertInstanceOf(IpEndPoint::class, $info->endPoint);
        $this->assertEquals('::1', $info->serverHost);
        $this->assertEquals(0, $info->serverPort);
        $this->assertEmpty($info->prefix);
        $this->assertTrue($info->persist);
        $this->assertEquals('username', $info->username);
        $this->assertEquals('password', $info->password);
        $this->assertEquals(1, $info->dbNumber);

        // dns
        $info = $valkey->parseDsn('valkey://username:@misaka.nl/');

        $this->assertInstanceOf(DnsEndPoint::class, $info->endPoint);
        $this->assertEquals('misaka.nl', $info->serverHost);
        $this->assertEquals(0, $info->serverPort);
        $this->assertEmpty($info->prefix);
        $this->assertFalse($info->persist);
        $this->assertEquals('username', $info->username);
        $this->assertEquals('', $info->password);
        $this->assertEquals(0, $info->dbNumber);
    }
}

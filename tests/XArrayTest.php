<?php
// XArrayTest.php
// Created: 2021-04-26
// Updated: 2025-01-21

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\XArray;

#[CoversClass(XArray::class)]
final class XArrayTest extends TestCase {
    public function testCountEmpty(): void {
        $array = [];
        $this->assertEquals(0, XArray::count($array));
        $this->assertTrue(XArray::empty($array));

        $array[] = 'the';
        $array[] = 'the';
        $array[] = 'the';
        $array[] = 'the';

        $this->assertEquals(4, XArray::count($array));
        $this->assertFalse(XArray::empty($array));
    }

    public function testExtractIterator(): void {
        $array1 = [1, 2, 3, 4, 5];
        $array2 = ['a', 'b', 'c', 'd', 'e'];
        $array3 = [1 => 'a', 2 => 'b', 3 => 'c', 4 => 'd', 5 => 'e'];
        $array4 = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5];

        foreach(XArray::extractIterator($array1) as $key => $value) {
            $this->assertTrue(is_string($key) || is_int($key));
            $this->assertArrayHasKey($key, $array1);
            $this->assertEquals($value, $array1[$key]);
        }

        foreach(XArray::extractIterator($array2) as $key => $value) {
            $this->assertTrue(is_string($key) || is_int($key));
            $this->assertArrayHasKey($key, $array2);
            $this->assertEquals($value, $array2[$key]);
        }

        foreach(XArray::extractIterator($array3) as $key => $value) {
            $this->assertTrue(is_string($key) || is_int($key));
            $this->assertArrayHasKey($key, $array3);
            $this->assertEquals($value, $array3[$key]);
        }

        foreach(XArray::extractIterator($array4) as $key => $value) {
            $this->assertTrue(is_string($key) || is_int($key));
            $this->assertArrayHasKey($key, $array4);
            $this->assertEquals($value, $array4[$key]);
        }
    }

    public function testEquals(): void {
        $array1 = [1, 2, 3, 4, 5];
        $array2 = ['a', 'b', 'c', 'd', 'e'];
        $array3 = [1, 2, 3, 4, 5];

        $this->assertTrue(XArray::sequenceEquals($array1, $array3));
        $this->assertFalse(XArray::sequenceEquals($array2, $array3));
    }

    public function testContains(): void {
        $array = ['test' => 'meow'];

        $this->assertTrue(XArray::contains($array, 'meow'));
        $this->assertTrue(XArray::containsKey($array, 'test'));
        $this->assertFalse(XArray::containsKey($array, 'meow'));
        $this->assertFalse(XArray::contains($array, 'test'));
    }

    public function testFirstLastKey(): void {
        $array = ['test' => 'meow', 'windows' => 'xp', 'mewow' => 'vista'];

        $this->assertEquals('test', XArray::firstKey($array));
        $this->assertEquals('mewow', XArray::lastKey($array));
    }

    public function testReverse(): void {
        $array1 = [1, 2, 3, 4, 5];
        $array2 = XArray::reverse([5, 4, 3, 2, 1]);

        $this->assertTrue(XArray::sequenceEquals($array1, $array2));
    }

    public function testMerge(): void {
        $array1 = [0, 1, 2, 3, 4, 5, 6, 7];
        $array2 = [0, 1, 2, 3];
        $array3 = [4, 5, 6, 7];
        $array4 = XArray::merge($array2, $array3);

        $this->assertTrue(XArray::sequenceEquals($array1, $array4));
        $this->assertTrue(XArray::sequenceEquals($array4, $array1));
        $this->assertFalse(XArray::sequenceEquals($array2, $array1));
        $this->assertFalse(XArray::sequenceEquals($array3, $array4));
    }

    public function testIndexOf(): void {
        $array = ['a', 'd', 'b', 'c', 'd',  'c', 'e', 'poop' => 'meow'];

        $this->assertEquals(3, XArray::indexOf($array, 'c'));
        $this->assertEquals(1, XArray::indexOf($array, 'd'));
        $this->assertEquals('poop', XArray::indexOf($array, 'meow'));
        $this->assertFalse(XArray::indexOf($array, 'z'));
    }

    public function testUniqueAndReflow(): void {
        $array1 = ['a', 'd', 'b', 'c', 'd',  'c', 'e'];
        $array2 = ['a', 'd', 'b', 'c', 'e'];

        $this->assertFalse(XArray::sequenceEquals($array2, $array1));

        // keys should differ
        $this->assertFalse(XArray::sequenceEquals($array2, XArray::unique($array1)));
        $this->assertTrue(XArray::sequenceEquals($array2, XArray::reflow(XArray::unique($array1))));
    }

    public function testWhere(): void {
        $array1 = ['Windows', 'Flashwave', 'Misaka Mikoto', 'Misuzu', 'Index', 'Kasane Teto', 'Some garbage'];
        $array2 = ['Misaka Mikoto', 'Kasane Teto', 'Some garbage'];

        $this->assertTrue(XArray::sequenceEquals(XArray::where($array1, fn($value) => strpos($value, ' ') !== false), $array2));
    }

    public function testFirst(): void {
        $array = ['Windows', 'Flashwave', 'Misaka Mikoto', 'Misuzu', 'Index', 'Kasane Teto', 'Some garbage'];

        $this->assertEquals(XArray::first($array, fn($value) => strpos($value, 't') !== false), 'Misaka Mikoto');
    }

    public function testSelect(): void {
        $array1 = ['Windows', 'Flashwave', 'Misaka Mikoto', 'Misuzu', 'Index', 'Kasane Teto', 'Some garbage'];
        $array2 = ['WINDOWS', 'FLASHWAVE', 'MISAKA MIKOTO', 'MISUZU', 'INDEX', 'KASANE TETO', 'SOME GARBAGE'];

        $this->assertTrue(XArray::sequenceEquals(XArray::select($array1, fn(string $input) => strtoupper($input)), $array2));
    }

    public function testSlice(): void {
        $array1 = ['Windows', 'Flashwave', 'Misaka Mikoto', 'Misuzu', 'Index', 'Kasane Teto', 'Some garbage'];
        $array2 = ['Misaka Mikoto', 'Misuzu', 'Index', 'Kasane Teto'];

        $this->assertTrue(XArray::sequenceEquals(XArray::slice($array1, 2, 4), $array2));
    }
}

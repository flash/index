<?php
// BencodeTest.php
// Created: 2023-07-21
// Updated: 2024-12-02

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\Bencode\Bencode;

#[CoversClass(Bencode::class)]
final class BencodeTest extends TestCase {
    public const TORRENT = 'ZDg6YW5ub3VuY2U0NTpodHRwczovL3RyYWNrZXIuZmxhc2hpaS5uZXQvYW5ub3VuY2UucGhwL21lb3c3OmNvbW1lbnQyNjp0aGlzIGlzIHRoZSBjb21tZW50cyBmaWVsZDEwOmNyZWF0ZWQgYnkxODpxQml0dG9ycmVudCB2NC41LjQxMzpjcmVhdGlvbiBkYXRlaTE2ODk5NzM2NjRlNDppbmZvZDY6bGVuZ3RoaTM5NDg4ZTQ6bmFtZTEwOmF1dGlzbS5qcGcxMjpwaWVjZSBsZW5ndGhpMTYzODRlNjpwaWVjZXM2MDpJCQyONUjTseqtXs9g1xIC+yWtTGjuWTho+hSXmOUkSAiKnD4vs5E9QewQ1NrXvVeg2xCRHYD/HL9RNRc3OnByaXZhdGVpMWVlZQ';

    public function testDecode(): void {
        $decoded = Bencode::decode(base64_decode(self::TORRENT));

        $this->assertIsArray($decoded);
        $this->assertEquals(5, count($decoded));

        $this->assertArrayHasKey('announce', $decoded);
        $this->assertEquals('https://tracker.flashii.net/announce.php/meow', $decoded['announce']);

        $this->assertArrayHasKey('creation date', $decoded);
        $this->assertEquals(1689973664, $decoded['creation date']);

        $this->assertArrayHasKey('info', $decoded);

        $this->assertIsArray($decoded['info']);
        $this->assertArrayHasKey('private', $decoded['info']);
        $this->assertEquals(1, $decoded['info']['private']);

        $decoded = Bencode::decode(base64_decode(self::TORRENT), associative: false);

        $this->assertIsObject($decoded);
        $this->assertObjectHasProperty('announce', $decoded);
        $this->assertEquals('https://tracker.flashii.net/announce.php/meow', $decoded->announce); // @phpstan-ignore-line: checked above
        $this->assertObjectHasProperty('creation date', $decoded);
        $this->assertEquals(1689973664, $decoded->{'creation date'}); // @phpstan-ignore-line: checked above
        $this->assertObjectHasProperty('comment', $decoded);
        $this->assertEquals('this is the comments field', $decoded->comment); // @phpstan-ignore-line: checked above
        $this->assertObjectHasProperty('info', $decoded);
        $this->assertIsObject($decoded->info); // @phpstan-ignore-line: checked above
        $this->assertObjectHasProperty('private', $decoded->info);
        $this->assertEquals(1, $decoded->info->private); // @phpstan-ignore-line: checked above
    }

    public function testEncode(): void {
        $original = base64_decode(self::TORRENT);
        $decoded = Bencode::decode($original);
        $encoded = Bencode::encode($decoded);

        $this->assertEquals($original, $encoded);
    }
}

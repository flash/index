<?php
// ByteFormatTest.php
// Created: 2023-07-05
// Updated: 2024-07-31

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\ByteFormat;

#[CoversClass(ByteFormat::class)]
final class ByteFormatTest extends TestCase {
    public function testFormat(): void {
        $this->assertEquals(ByteFormat::formatDecimal(0), 'Zero Bytes');
        $this->assertEquals(ByteFormat::formatBinary(0), 'Zero Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(64), '64 Bytes');
        $this->assertEquals(ByteFormat::formatBinary(64), '64 Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(-64), '-64 Bytes');
        $this->assertEquals(ByteFormat::formatBinary(-64), '-64 Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(512), '512 Bytes');
        $this->assertEquals(ByteFormat::formatBinary(512), '512 Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(-512), '-512 Bytes');
        $this->assertEquals(ByteFormat::formatBinary(-512), '-512 Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(1000), '1.00 KB');
        $this->assertEquals(ByteFormat::formatBinary(1000), '1000 Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(-1000), '-1.00 KB');
        $this->assertEquals(ByteFormat::formatBinary(-1000), '-1000 Bytes');

        $this->assertEquals(ByteFormat::formatDecimal(1024), '1.02 KB');
        $this->assertEquals(ByteFormat::formatBinary(1024), '1.00 KiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1024), '-1.02 KB');
        $this->assertEquals(ByteFormat::formatBinary(-1024), '-1.00 KiB');

        $this->assertEquals(ByteFormat::formatDecimal(1000000), '1.00 MB');
        $this->assertEquals(ByteFormat::formatBinary(1000000), '976.6 KiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1000000), '-1.00 MB');
        $this->assertEquals(ByteFormat::formatBinary(-1000000), '-976.6 KiB');

        $this->assertEquals(ByteFormat::formatDecimal(1048576), '1.05 MB');
        $this->assertEquals(ByteFormat::formatBinary(1048576), '1.00 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1048576), '-1.05 MB');
        $this->assertEquals(ByteFormat::formatBinary(-1048576), '-1.00 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(25252525), '25.3 MB');
        $this->assertEquals(ByteFormat::formatBinary(25252525), '24.1 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(-25252525), '-25.3 MB');
        $this->assertEquals(ByteFormat::formatBinary(-25252525), '-24.1 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(26476544), '26.5 MB');
        $this->assertEquals(ByteFormat::formatBinary(26476544), '25.2 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(-26476544), '-26.5 MB');
        $this->assertEquals(ByteFormat::formatBinary(-26476544), '-25.2 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(1000000000), '1.00 GB');
        $this->assertEquals(ByteFormat::formatBinary(1000000000), '953.7 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1000000000), '-1.00 GB');
        $this->assertEquals(ByteFormat::formatBinary(-1000000000), '-953.7 MiB');

        $this->assertEquals(ByteFormat::formatDecimal(1073741824), '1.07 GB');
        $this->assertEquals(ByteFormat::formatBinary(1073741824), '1.00 GiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1073741824), '-1.07 GB');
        $this->assertEquals(ByteFormat::formatBinary(-1073741824), '-1.00 GiB');

        $this->assertEquals(ByteFormat::formatDecimal(1000000000000), '1.00 TB');
        $this->assertEquals(ByteFormat::formatBinary(1000000000000), '931.3 GiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1000000000000), '-1.00 TB');
        $this->assertEquals(ByteFormat::formatBinary(-1000000000000), '-931.3 GiB');

        $this->assertEquals(ByteFormat::formatDecimal(1099511627776), '1.10 TB');
        $this->assertEquals(ByteFormat::formatBinary(1099511627776), '1.00 TiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1099511627776), '-1.10 TB');
        $this->assertEquals(ByteFormat::formatBinary(-1099511627776), '-1.00 TiB');

        $this->assertEquals(ByteFormat::formatDecimal(1000000000000000), '1.00 PB');
        $this->assertEquals(ByteFormat::formatBinary(1000000000000000), '909.5 TiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1000000000000000), '-1.00 PB');
        $this->assertEquals(ByteFormat::formatBinary(-1000000000000000), '-909.5 TiB');

        $this->assertEquals(ByteFormat::formatDecimal(1125899906842624), '1.13 PB');
        $this->assertEquals(ByteFormat::formatBinary(1125899906842624), '1.00 PiB');

        $this->assertEquals(ByteFormat::formatDecimal(-1125899906842624), '-1.13 PB');
        $this->assertEquals(ByteFormat::formatBinary(-1125899906842624), '-1.00 PiB');
    }
}

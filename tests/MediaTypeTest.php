<?php
// MediaTypeTest.php
// Created: 2024-08-18
// Updated: 2025-01-18

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\{CoversClass,UsesClass};
use Index\{MediaType,XArray};

// This test needs to be made more comprehensive someday, this is currently only checking for an issue that I actually ran into.

#[CoversClass(MediaType::class)]
#[UsesClass(UsesClass::class)]
final class MediaTypeTest extends TestCase {
    public function testQualityParsing(): void {
        $types = [
            'text/html' => 1.0,
            'application/xhtml+xml' => 1.0,
            'application/xml;q=0.9' => 0.9,
            'image/webp' => 1.0,
            '*/*;q=0.8' => 0.8,
        ];

        foreach($types as $typeStr => $expectQuality) {
            $type = MediaType::parse($typeStr);
            $this->assertEquals($typeStr, (string)$type);
            $this->assertEquals($expectQuality, $type->quality);
        }
    }
}

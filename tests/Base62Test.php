<?php
// Base62Test.php
// Created: 2022-01-28
// Updated: 2024-12-22

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\XNumber;

#[CoversClass(XNumber::class)]
final class Base62Test extends TestCase {
    public const TESTS = [
        ['aaaaaa', 9311514030],
        ['Soap', 12962613],
        ['0', 0],
        ['1', 1],
        ['kPH', 80085],
        ['UC', 3510],
        ['p', 25],
        ['6zi', 25252],
        ['1ly7vk', 1234567890],
        ['15vjOLmUu1', 14739082838046309],
    ];

    public function testDecode(): void {
        $this->assertEquals(0, XNumber::fromBase62(''));
        foreach(self::TESTS as $test)
            $this->assertEquals($test[1], XNumber::fromBase62($test[0]));
    }

    public function testEncode(): void {
        foreach(self::TESTS as $test)
            $this->assertEquals($test[0], XNumber::toBase62($test[1]));
    }
}

<?php
// ColourTest.php
// Created: 2023-01-02
// Updated: 2025-01-18

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\Colour\{Colour,ColourHsl,ColourNamed,ColourNull,ColourRgb};

#[CoversClass(Colour::class)]
#[CoversClass(ColourHsl::class)]
#[CoversClass(ColourNamed::class)]
#[CoversClass(ColourNull::class)]
#[CoversClass(ColourRgb::class)]
final class ColourTest extends TestCase {
    public function testNone(): void {
        $none = Colour::none();
        $this->assertInstanceOf(ColourNull::class, $none);
        $this->assertTrue($none->inherits);
        $this->assertEquals('inherit', $none);
    }

    public function testLuminance(): void {
        for($i = 0; $i < 0xB; ++$i) {
            $num = max(0, min(255, ($i << 4) | $i));
            $colour = new ColourRgb($num, $num, $num, 1);
            $this->assertTrue($colour->dark);
            $this->assertFalse($colour->light);
        }

        for($i = 0xB; $i < 0x10; ++$i) {
            $num = max(0, min(255, ($i << 4) | $i));
            $colour = new ColourRgb($num, $num, $num, 1);
            $this->assertFalse($colour->dark);
            $this->assertTrue($colour->light);
        }
    }

    public function testNamed(): void {
        $names = [
            ['red', 0xFF, 0, 0, false],
            ['orange', 0xFF, 0xA5, 0, false],
            ['tan', 0xD2, 0xB4, 0x8C, false],
            ['rebeccapurple', 0x66, 0x33, 0x99, false],
            ['transparent', 0, 0, 0, true],
            ['xthiswillneverexist', 0, 0, 0, true],
        ];

        foreach($names as $name) {
            $colour = new ColourNamed($name[0]);
            $this->assertEquals($name[0], $colour->name);
            $this->assertEquals($name[1], $colour->red);
            $this->assertEquals($name[2], $colour->green);
            $this->assertEquals($name[3], $colour->blue);

            if($name[4]) {
                $this->assertTrue($colour->transparent);
                $this->assertEquals(0.0, $colour->alpha);
            } else {
                $this->assertFalse($colour->transparent);
                $this->assertEquals(1.0, $colour->alpha);
            }

            $this->assertFalse($colour->inherits);
            $this->assertInstanceOf(ColourNamed::class, $colour);
            $this->assertEquals($name[0], $name[0]);
        }
    }

    public function testRGB(): void {
        $colour = new ColourRgb(31, 120, 50, 1.0);
        $this->assertEquals(31, $colour->red);
        $this->assertEquals(120, $colour->green);
        $this->assertEquals(50, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0x1F7832, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF1F7832, Colour::toRawArgb($colour));
        $this->assertEquals(0x1F7832FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('#1f7832', $colour);

        $colour = ColourRgb::fromRawArgb(0xCCFF7A7F);
        $this->assertEquals(255, $colour->red);
        $this->assertEquals(122, $colour->green);
        $this->assertEquals(127, $colour->blue);
        $this->assertEquals(.8, $colour->alpha);
        $this->assertEquals(0xFF7A7F, Colour::toRawRgb($colour));
        $this->assertEquals(0xCCFF7A7F, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF7A7FCC, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('rgba(255, 122, 127, 0.8)', $colour);

        $colour = ColourRgb::fromRawRgb(0x4D3380);
        $this->assertEquals(77, $colour->red);
        $this->assertEquals(51, $colour->green);
        $this->assertEquals(128, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0x4D3380, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF4D3380, Colour::toRawArgb($colour));
        $this->assertEquals(0x4D3380FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('#4d3380', $colour);

        $colour = ColourRgb::fromRawRgba(0xFF7A7F33);
        $this->assertEquals(255, $colour->red);
        $this->assertEquals(122, $colour->green);
        $this->assertEquals(127, $colour->blue);
        $this->assertEquals(.2, $colour->alpha);
        $this->assertEquals(0xFF7A7F, Colour::toRawRgb($colour));
        $this->assertEquals(0x33FF7A7F, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF7A7F33, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('rgba(255, 122, 127, 0.2)', $colour);
    }

    public function testConvertToRGB(): void {
        $colourA = new ColourRgb(31, 120, 50, 1.0);
        $colourB = ColourRgb::convert($colourA);
        $this->assertSame($colourA, $colourB); // converting to the same type should passthru

        $colourA = Colour::none();
        $colourB = ColourRgb::convert($colourA); // converting none to RGB makes very little sense, but you can do it if you want...
        $this->assertEquals(0, $colourB->red);
        $this->assertEquals(0, $colourB->green);
        $this->assertEquals(0, $colourB->blue);
        $this->assertEquals(1.0, $colourB->alpha);
        $this->assertEquals(0, Colour::toRawRgb($colourB));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colourB));
        $this->assertEquals(0xFF, Colour::toRawRgba($colourB));
        $this->assertInstanceOf(ColourRgb::class, $colourB);
        $this->assertFalse($colourB->inherits); // it does lose the inherit flag, which is intentional
        $this->assertFalse($colourB->light);
        $this->assertTrue($colourB->dark);
        $this->assertEquals('#000000', $colourB);

        $colourA = new ColourNamed('steelblue');
        $colourB = ColourRgb::convert($colourA);
        $this->assertEquals(0x46, $colourB->red);
        $this->assertEquals(0x82, $colourB->green);
        $this->assertEquals(0xB4, $colourB->blue);
        $this->assertEquals(1.0, $colourB->alpha);
        $this->assertEquals(0x4682B4, Colour::toRawRgb($colourB));
        $this->assertEquals(0xFF4682B4, Colour::toRawArgb($colourB));
        $this->assertEquals(0x4682B4FF, Colour::toRawRgba($colourB));
        $this->assertInstanceOf(ColourRgb::class, $colourB);
        $this->assertFalse($colourB->inherits);
        $this->assertFalse($colourB->light);
        $this->assertTrue($colourB->dark);
        $this->assertEquals('#4682b4', $colourB);

        $colourA = new ColourHsl(50, .8, .4, 1.0);
        $colourB = ColourRgb::convert($colourA);
        $this->assertEquals(0xB8, $colourB->red);
        $this->assertEquals(0x9C, $colourB->green);
        $this->assertEquals(0x14, $colourB->blue);
        $this->assertEquals(1.0, $colourB->alpha);
        $this->assertEquals(0xB89C14, Colour::toRawRgb($colourB));
        $this->assertEquals(0xFFB89C14, Colour::toRawArgb($colourB));
        $this->assertEquals(0xB89C14FF, Colour::toRawRgba($colourB));
        $this->assertInstanceOf(ColourRgb::class, $colourB);
        $this->assertFalse($colourB->inherits);
        $this->assertFalse($colourB->light);
        $this->assertTrue($colourB->dark);
        $this->assertEquals('#b89c14', $colourB);
    }

    public function testHSL(): void {
        $colour = new ColourHsl(50, .8, .4, 1.0);
        $this->assertEquals(50, $colour->hue);
        $this->assertEquals(.8, $colour->saturation);
        $this->assertEquals(.4, $colour->lightness);
        $this->assertEquals(0xB8, $colour->red);
        $this->assertEquals(0x9C, $colour->green);
        $this->assertEquals(0x14, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0xB89C14, Colour::toRawRgb($colour));
        $this->assertEquals(0xFFB89C14, Colour::toRawArgb($colour));
        $this->assertEquals(0xB89C14FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('hsl(50deg, 80%, 40%)', $colour);

        $colour = new ColourHsl(150, .3, .6, 1.0);
        $this->assertEquals(150, $colour->hue);
        $this->assertEquals(.3, $colour->saturation);
        $this->assertEquals(.6, $colour->lightness);
        $this->assertEquals(0x7A, $colour->red);
        $this->assertEquals(0xB8, $colour->green);
        $this->assertEquals(0x99, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0x7AB899, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF7AB899, Colour::toRawArgb($colour));
        $this->assertEquals(0x7AB899FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('hsl(150deg, 30%, 60%)', $colour);

        $colour = new ColourHsl(108, .6, .45, .7);
        $this->assertEquals(108, $colour->hue);
        $this->assertEquals(.6, $colour->saturation);
        $this->assertEquals(.45, $colour->lightness);
        $this->assertEquals(0x49, $colour->red);
        $this->assertEquals(0xB8, $colour->green);
        $this->assertEquals(0x2E, $colour->blue);
        $this->assertEquals(0x49B82E, Colour::toRawRgb($colour));
        $this->assertEquals(0xB349B82E, Colour::toRawArgb($colour));
        $this->assertEquals(0x49B82EB3, Colour::toRawRgba($colour));
        $this->assertEquals(.7, $colour->alpha);
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('hsla(108deg, 60%, 45%, 0.7)', $colour);

        $colour = new ColourHsl(0, .8, .5, .25);
        $this->assertEquals(0, $colour->hue);
        $this->assertEquals(.8, $colour->saturation);
        $this->assertEquals(.5, $colour->lightness);
        $this->assertEquals(0xE5, $colour->red);
        $this->assertEquals(0x19, $colour->green);
        $this->assertEquals(0x19, $colour->blue);
        $this->assertEquals(.25, $colour->alpha);
        $this->assertEquals(0xE51919, Colour::toRawRgb($colour));
        $this->assertEquals(0x40E51919, Colour::toRawArgb($colour));
        $this->assertEquals(0xE5191940, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('hsla(0deg, 80%, 50%, 0.25)', $colour);
    }

    public function testConvertToHSL(): void {
        $colourA = new ColourHsl(50, .8, .4, 1.0);
        $colourB = ColourHsl::convert($colourA);
        $this->assertSame($colourA, $colourB); // converting to the same type should passthru

        $colourA = Colour::none();
        $colourB = ColourHsl::convert($colourA); // converting none to HSL makes very little sense, but you can do it if you want...
        $this->assertEquals(0, $colourB->hue);
        $this->assertEquals(0, $colourB->saturation);
        $this->assertEquals(0, $colourB->lightness);
        $this->assertEquals(0, $colourB->red);
        $this->assertEquals(0, $colourB->green);
        $this->assertEquals(0, $colourB->blue);
        $this->assertEquals(1.0, $colourB->alpha);
        $this->assertEquals(0, Colour::toRawRgb($colourB));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colourB));
        $this->assertEquals(0xFF, Colour::toRawRgba($colourB));
        $this->assertInstanceOf(ColourHsl::class, $colourB);
        $this->assertFalse($colourB->inherits); // it does lose the inherit flag, which is intentional
        $this->assertFalse($colourB->light);
        $this->assertTrue($colourB->dark);
        $this->assertEquals('hsl(0deg, 0%, 0%)', $colourB);

        $colourA = new ColourNamed('yellowgreen');
        $colourB = ColourHsl::convert($colourA);
        $this->assertEquals(79.742, $colourB->hue);
        $this->assertEquals(.608, $colourB->saturation);
        $this->assertEquals(.5, $colourB->lightness);
        $this->assertEquals(0x9A, $colourB->red);
        $this->assertEquals(0xCD, $colourB->green);
        $this->assertEquals(0x32, $colourB->blue);
        $this->assertEquals(1.0, $colourB->alpha);
        $this->assertEquals(0x9ACD32, Colour::toRawRgb($colourB));
        $this->assertEquals(0xFF9ACD32, Colour::toRawArgb($colourB));
        $this->assertEquals(0x9ACD32FF, Colour::toRawRgba($colourB));
        $this->assertInstanceOf(ColourHsl::class, $colourB);
        $this->assertFalse($colourB->inherits);
        $this->assertFalse($colourB->light);
        $this->assertTrue($colourB->dark);
        $this->assertEquals('hsl(79.74deg, 61%, 50%)', $colourB);

        $colourA = new ColourRgb(31, 120, 50, 1.0);
        $colourB = ColourHsl::convert($colourA);
        $this->assertEquals(132.809, $colourB->hue);
        $this->assertEquals(.589, $colourB->saturation);
        $this->assertEquals(.296, $colourB->lightness);
        $this->assertEquals(0x1F, $colourB->red);
        $this->assertEquals(0x78, $colourB->green);
        $this->assertEquals(0x32, $colourB->blue);
        $this->assertEquals(1.0, $colourB->alpha);
        $this->assertEquals(0x1F7832, Colour::toRawRgb($colourB));
        $this->assertEquals(0xFF1F7832, Colour::toRawArgb($colourB));
        $this->assertEquals(0x1F7832FF, Colour::toRawRgba($colourB));
        $this->assertInstanceOf(ColourHsl::class, $colourB);
        $this->assertFalse($colourB->inherits);
        $this->assertFalse($colourB->light);
        $this->assertTrue($colourB->dark);
        $this->assertEquals('hsl(132.81deg, 59%, 30%)', (string)$colourB);
    }

    public function testMisuzu(): void {
        $colour = Colour::fromMisuzu(0x40AABBCC);
        $this->assertInstanceOf(ColourNull::class, $colour);
        $this->assertTrue($colour->inherits);
        $this->assertEquals('inherit', $colour);
        $this->assertEquals(0x40000000, Colour::toMisuzu($colour));
        $this->assertEquals(0, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF, Colour::toRawRgba($colour));

        $colour = Colour::fromMisuzu(0xAABBCC);
        $this->assertEquals(0xAA, $colour->red);
        $this->assertEquals(0xBB, $colour->green);
        $this->assertEquals(0xCC, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0xAABBCC, Colour::toRawRgb($colour));
        $this->assertEquals(0xFFAABBCC, Colour::toRawArgb($colour));
        $this->assertEquals(0xAABBCCFF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('#aabbcc', $colour);
        $this->assertEquals(0xAABBCC, Colour::toMisuzu($colour));

        $colour = Colour::fromMisuzu(1693465);
        $this->assertEquals(0x19, $colour->red);
        $this->assertEquals(0xD7, $colour->green);
        $this->assertEquals(0x19, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0x19D719, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF19D719, Colour::toRawArgb($colour));
        $this->assertEquals(0x19D719FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('#19d719', $colour);
        $this->assertEquals(1693465, Colour::toMisuzu($colour));

        $colour = Colour::fromMisuzu(16711680);
        $this->assertEquals(0xFF, $colour->red);
        $this->assertEquals(0, $colour->green);
        $this->assertEquals(0, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0xFF0000, Colour::toRawRgb($colour));
        $this->assertEquals(0xFFFF0000, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF0000FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('#ff0000', $colour);
        $this->assertEquals(16711680, Colour::toMisuzu($colour));

        $colour = Colour::fromMisuzu(7558084);
        $this->assertEquals(0x73, $colour->red);
        $this->assertEquals(0x53, $colour->green);
        $this->assertEquals(0xC4, $colour->blue);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(0x7353C4, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF7353C4, Colour::toRawArgb($colour));
        $this->assertEquals(0x7353C4FF, Colour::toRawRgba($colour));
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->light);
        $this->assertTrue($colour->dark);
        $this->assertEquals('#7353c4', $colour);
        $this->assertEquals(7558084, Colour::toMisuzu($colour));
    }

    public function testParse(): void {
        $colour = Colour::parse(null);
        $this->assertTrue($colour->inherits);
        $this->assertSame(Colour::none(), $colour);
        $this->assertEquals('inherit', $colour);
        $this->assertEquals(0, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF, Colour::toRawRgba($colour));

        $colour = Colour::parse('   ');
        $this->assertTrue($colour->inherits);
        $this->assertSame(Colour::none(), $colour);
        $this->assertEquals('inherit', $colour);
        $this->assertEquals(0, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF, Colour::toRawRgba($colour));

        $colour = Colour::parse('inherit');
        $this->assertTrue($colour->inherits);
        $this->assertSame(Colour::none(), $colour);
        $this->assertEquals('inherit', $colour);
        $this->assertEquals(0, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF, Colour::toRawRgba($colour));

        $colour = Colour::parse('xthiswillalsoneverexist');
        $this->assertTrue($colour->inherits);
        $this->assertSame(Colour::none(), $colour);
        $this->assertEquals('inherit', $colour);
        $this->assertEquals(0, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF000000, Colour::toRawArgb($colour));
        $this->assertEquals(0xFF, Colour::toRawRgba($colour));


        $colour = Colour::parse('TRANSPARENT');
        $this->assertInstanceOf(ColourNamed::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertTrue($colour->transparent);
        $this->assertEquals('transparent', $colour->name);
        $this->assertEquals('transparent', $colour);
        $this->assertEquals(0, $colour->alpha);
        $this->assertEquals(0, Colour::toRawRgb($colour));
        $this->assertEquals(0, Colour::toRawArgb($colour));
        $this->assertEquals(0, Colour::toRawRgba($colour));

        $colour = Colour::parse('  MediumOrchid   ');
        $this->assertInstanceOf(ColourNamed::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertFalse($colour->transparent);
        $this->assertEquals('mediumorchid', $colour->name);
        $this->assertEquals('mediumorchid', $colour);
        $this->assertEquals(1, $colour->alpha);
        $this->assertEquals(0xBA, $colour->red);
        $this->assertEquals(0x55, $colour->green);
        $this->assertEquals(0xD3, $colour->blue);
        $this->assertEquals(0xBA55D3, Colour::toRawRgb($colour));
        $this->assertEquals(0xFFBA55D3, Colour::toRawArgb($colour));
        $this->assertEquals(0xBA55D3FF, Colour::toRawRgba($colour));


        $colour = Colour::parse('#123');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('#112233', $colour);
        $this->assertEquals(1, $colour->alpha);
        $this->assertEquals(0x11, $colour->red);
        $this->assertEquals(0x22, $colour->green);
        $this->assertEquals(0x33, $colour->blue);
        $this->assertEquals(0x112233, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF112233, Colour::toRawArgb($colour));
        $this->assertEquals(0x112233FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('#3456');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('rgba(51, 68, 85, 0.4)', $colour);
        $this->assertEquals(0.4, $colour->alpha);
        $this->assertEquals(0x33, $colour->red);
        $this->assertEquals(0x44, $colour->green);
        $this->assertEquals(0x55, $colour->blue);
        $this->assertEquals(0x334455, Colour::toRawRgb($colour));
        $this->assertEquals(0x66334455, Colour::toRawArgb($colour));
        $this->assertEquals(0x33445566, Colour::toRawRgba($colour));

        $colour = Colour::parse('#9475b2');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('#9475b2', $colour);
        $this->assertEquals(1, $colour->alpha);
        $this->assertEquals(0x94, $colour->red);
        $this->assertEquals(0x75, $colour->green);
        $this->assertEquals(0xB2, $colour->blue);
        $this->assertEquals(0x9475B2, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF9475B2, Colour::toRawArgb($colour));
        $this->assertEquals(0x9475B2FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('#8559A5b3');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('rgba(133, 89, 165, 0.702)', $colour);
        $this->assertEquals(0.702, round($colour->alpha, 3));
        $this->assertEquals(0x85, $colour->red);
        $this->assertEquals(0x59, $colour->green);
        $this->assertEquals(0xA5, $colour->blue);
        $this->assertEquals(0x8559A5, Colour::toRawRgb($colour));
        $this->assertEquals(0xB38559A5, Colour::toRawArgb($colour));
        $this->assertEquals(0x8559A5B3, Colour::toRawRgba($colour));


        $colour = Colour::parse('rgb( 17  , 34    ,  51)');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('#112233', $colour);
        $this->assertEquals(1, $colour->alpha);
        $this->assertEquals(0x11, $colour->red);
        $this->assertEquals(0x22, $colour->green);
        $this->assertEquals(0x33, $colour->blue);
        $this->assertEquals(0x112233, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF112233, Colour::toRawArgb($colour));
        $this->assertEquals(0x112233FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('rgba(51 ,        68,                    85 ,0.4   )');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('rgba(51, 68, 85, 0.4)', $colour);
        $this->assertEquals(0.4, $colour->alpha);
        $this->assertEquals(0x33, $colour->red);
        $this->assertEquals(0x44, $colour->green);
        $this->assertEquals(0x55, $colour->blue);
        $this->assertEquals(0x334455, Colour::toRawRgb($colour));
        $this->assertEquals(0x66334455, Colour::toRawArgb($colour));
        $this->assertEquals(0x33445566, Colour::toRawRgba($colour));

        $colour = Colour::parse('rgba( 148,117,178 )');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('#9475b2', $colour);
        $this->assertEquals(1, $colour->alpha);
        $this->assertEquals(0x94, $colour->red);
        $this->assertEquals(0x75, $colour->green);
        $this->assertEquals(0xB2, $colour->blue);
        $this->assertEquals(0x9475B2, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF9475B2, Colour::toRawArgb($colour));
        $this->assertEquals(0x9475B2FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('rgb(133 ,89,165,.700)');
        $this->assertInstanceOf(ColourRgb::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('rgba(133, 89, 165, 0.7)', $colour);
        $this->assertEquals(0.7, $colour->alpha);
        $this->assertEquals(0x85, $colour->red);
        $this->assertEquals(0x59, $colour->green);
        $this->assertEquals(0xA5, $colour->blue);
        $this->assertEquals(0x8559A5, Colour::toRawRgb($colour));
        $this->assertEquals(0xB38559A5, Colour::toRawArgb($colour));
        $this->assertEquals(0x8559A5B3, Colour::toRawRgba($colour));


        // these results aren't really correct
        // the difference in results is probably negligible for now
        // but should definitely be reviewed in the future

        $colour = Colour::parse('hsla( 210  , 50 , 13    )');
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('hsl(210deg, 50%, 13%)', $colour);
        $this->assertEquals(1, $colour->alpha);
        $this->assertEquals(210, $colour->hue);
        $this->assertEquals(.5, $colour->saturation);
        $this->assertEquals(.13, $colour->lightness);
        $this->assertEquals(17, $colour->red);
        $this->assertEquals(33, $colour->green);
        $this->assertEquals(50, $colour->blue);
        $this->assertEquals(0x112132, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF112132, Colour::toRawArgb($colour));
        $this->assertEquals(0x112132FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('hsl(210deg ,25%, 27% ,.6)');
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('hsla(210deg, 25%, 27%, 0.6)', $colour);
        $this->assertEquals(.6, $colour->alpha);
        $this->assertEquals(210, $colour->hue);
        $this->assertEquals(.25, $colour->saturation);
        $this->assertEquals(.27, $colour->lightness);
        $this->assertEquals(52, $colour->red);
        $this->assertEquals(69, $colour->green);
        $this->assertEquals(86, $colour->blue);
        $this->assertEquals(0x344556, Colour::toRawRgb($colour));
        $this->assertEquals(0x99344556, Colour::toRawArgb($colour));
        $this->assertEquals(0x34455699, Colour::toRawRgba($colour));

        $colour = Colour::parse('hsl(300grad,28%,58)');
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('hsl(270deg, 28%, 58%)', $colour);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(270, $colour->hue);
        $this->assertEquals(.28, $colour->saturation);
        $this->assertEquals(.58, $colour->lightness);
        $this->assertEquals(148, $colour->red);
        $this->assertEquals(118, $colour->green);
        $this->assertEquals(178, $colour->blue);
        $this->assertEquals(0x9476B2, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF9476B2, Colour::toRawArgb($colour));
        $this->assertEquals(0x9476B2FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('hsl(    4.799rad ,30           , 50%)');
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('hsl(275deg, 30%, 50%)', $colour);
        $this->assertEquals(1.0, $colour->alpha);
        $this->assertEquals(275, $colour->hue);
        $this->assertEquals(.3, $colour->saturation);
        $this->assertEquals(.5, $colour->lightness);
        $this->assertEquals(134, $colour->red);
        $this->assertEquals(89, $colour->green);
        $this->assertEquals(166, $colour->blue);
        $this->assertEquals(0x8659A6, Colour::toRawRgb($colour));
        $this->assertEquals(0xFF8659A6, Colour::toRawArgb($colour));
        $this->assertEquals(0x8659A6FF, Colour::toRawRgba($colour));

        $colour = Colour::parse('hsl( .775turn  , 13,60,             .54)');
        $this->assertInstanceOf(ColourHsl::class, $colour);
        $this->assertFalse($colour->inherits);
        $this->assertEquals('hsla(279deg, 13%, 60%, 0.54)', $colour);
        $this->assertEquals(0.54, $colour->alpha);
        $this->assertEquals(279, $colour->hue);
        $this->assertEquals(.13, $colour->saturation);
        $this->assertEquals(.6, $colour->lightness);
        $this->assertEquals(157, $colour->red);
        $this->assertEquals(140, $colour->green);
        $this->assertEquals(166, $colour->blue);
        $this->assertEquals(0x9D8CA6, Colour::toRawRgb($colour));
        $this->assertEquals(0x8A9D8CA6, Colour::toRawArgb($colour));
        $this->assertEquals(0x9D8CA68A, Colour::toRawRgba($colour));
    }

    public function testMix(): void {
        // none should always return none if involved...
        $mixed = Colour::mix(Colour::none(), Colour::parse('lemonchiffon'), 0.243);
        $this->assertTrue($mixed->inherits);
        $mixed = Colour::mix(Colour::parse('powderblue'), Colour::none(), 0.76);
        $this->assertTrue($mixed->inherits);

        // ...unless absolute 0 or 1
        $mixed = Colour::mix(Colour::none(), Colour::parse('lemonchiffon'), 1.0);
        $this->assertFalse($mixed->inherits);
        $mixed = Colour::mix(Colour::parse('powderblue'), Colour::none(), 0.0);
        $this->assertFalse($mixed->inherits);

        // probably a pretty naive test but its better than nothing
        $mixed = Colour::mix(new ColourRgb(0, 100, 50), new ColourRgb(100, 0, 50), 0.5);
        $this->assertEquals(50, $mixed->red);
        $this->assertEquals(50, $mixed->green);
        $this->assertEquals(50, $mixed->blue);

        // absolutes should return one of the args
        $colour1 = Colour::parse('royalblue');
        $colour2 = Colour::parse('tomato');

        $this->assertSame($colour1, Colour::mix($colour1, $colour2, 0.0));
        $this->assertSame($colour2, Colour::mix($colour1, $colour2, 1.0));

        $mixed = Colour::mix($colour1, $colour2, 0.5);
        $this->assertEquals(160, $mixed->red);
        $this->assertEquals(102, $mixed->green);
        $this->assertEquals(148, $mixed->blue);

        $mixed = Colour::mix($colour1, $colour2, 0.2);
        $this->assertEquals(103, $mixed->red);
        $this->assertEquals(104, $mixed->green);
        $this->assertEquals(194, $mixed->blue);

        $mixed = Colour::mix($colour1, $colour2, 0.8);
        $this->assertEquals(217, $mixed->red);
        $this->assertEquals(100, $mixed->green);
        $this->assertEquals(102, $mixed->blue);
    }
}

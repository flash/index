<?php
// IpAddressRangeTest.php
// Created: 2021-04-27
// Updated: 2024-10-02

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\{CoversClass,UsesClass};
use Index\Net\{IpAddress,IpAddressRange};

#[CoversClass(IpAddressRange::class)]
#[UsesClass(IpAddress::class)]
final class IpAddressRangeTest extends TestCase {
    public function testParsing(): void {
        $v4 = IpAddressRange::parse('127.0.0.1/23');
        $v6 = IpAddressRange::parse('::1/128');

        $this->expectException(InvalidArgumentException::class);
        $v0 = IpAddressRange::parse('12.4:424::1/128');
    }

    public function testToString(): void {
        $v4 = IpAddressRange::parse('53.64.123.86/18');
        $this->assertEquals('53.64.123.86/18', (string)$v4);

        $v6 = IpAddressRange::parse('abcd:1234::43:211a/86');
        $this->assertEquals('abcd:1234::43:211a/86', (string)$v6);
    }

    public function testMatch(): void {
        $v41 = IpAddress::parse('53.64.123.86');
        $v42 = IpAddress::parse('53.64.130.86');
        $v61 = IpAddress::parse('abcd:1234::43:211a');
        $v62 = IpAddress::parse('bbce:1535::73:212a');

        $r4 = IpAddressRange::parse('53.64.123.0/22');
        $r6 = IpAddressRange::parse('abcd:1234::43:211a/86');

        $this->assertTrue($r4->match($v41));
        $this->assertFalse($r4->match($v42));

        $this->assertTrue($r6->match($v61));
        $this->assertFalse($r6->match($v62));
    }

    public function testSerializable(): void {
        $cidrs = [
            '53.64.123.0/22',
            'abcd:1234::43:211a/86',
        ];

        foreach($cidrs as $cidr) {
            $obj = IpAddressRange::parse($cidr);
            $ser = serialize($obj);
            $unser = unserialize($ser);
            $this->assertTrue($obj->equals($unser));
        }
    }
}

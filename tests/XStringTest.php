<?php
// XStringTest.php
// Created: 2021-04-26
// Updated: 2024-07-31

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\XString;

#[CoversClass(XString::class)]
final class XStringTest extends TestCase {
    public function testEscape(): void {
        $dirty = '<img onerror="alert(\'xss\')">';
        $clean = '&lt;img onerror=&quot;alert(\'xss\')&quot;&gt;';

        $this->assertEquals($clean, XString::escape($dirty));
    }

    public function testEmpty(): void {
        $this->assertTrue(XString::nullOrEmpty(null));
        $this->assertTrue(XString::nullOrEmpty(''));
        $this->assertFalse(XString::nullOrEmpty('soap'));
        $this->assertTrue(XString::nullOrWhitespace(''));
        $this->assertTrue(XString::nullOrWhitespace('  '));
    }

    public function testCountUnique(): void {
        $this->assertEquals(10, XString::countUnique('iaabbccddjjefghefghi'));
        $this->assertEquals(11, XString::countUnique('jeff has three apples'));
    }
}

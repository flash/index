<?php
// WStringTest.php
// Created: 2021-04-26
// Updated: 2025-01-18

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\WString;

#[CoversClass(WString::class)]
final class WStringTest extends TestCase {
    public function testStartsWith(): void {
        $this->assertTrue(WString::startsWith('君は実にバカだな', '君は'));
    }

    public function testEndsWith(): void {
        $this->assertTrue(WString::endsWith('君は実にバカだな', 'バカだな'));
    }

    public function testReverse(): void {
        $this->assertEquals('なだカバに実は君', WString::reverse('君は実にバカだな'));
        $this->assertEquals("\x82\xc8\x82\xbe\x83\x4a\x83\x6f\x82\xc9\x8e\xc0\x82\xcd\x8c\x4e", WString::reverse("\x8c\x4e\x82\xcd\x8e\xc0\x82\xc9\x83\x6f\x83\x4a\x82\xbe\x82\xc8", encoding: 'Shift_JIS'));
    }

    public function testEmpty(): void {
        $this->assertTrue(WString::nullOrWhitespace(''));
        $this->assertTrue(WString::nullOrWhitespace('  '));
        $this->assertTrue(WString::nullOrWhitespace('　'));
        $this->assertTrue(WString::nullOrWhitespace('　'));
    }

    public function testCountUnique(): void {
        $this->assertEquals(10, WString::countUnique('iaabbccddjjefghefghi'));
        $this->assertEquals(11, WString::countUnique('jeff has three apples'));
        $this->assertEquals(5, WString::countUnique('みさかみこと'));
    }
}

<?php
// IndexTest.php
// Created: 2021-05-02
// Updated: 2024-08-01

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\{CoversClass,UsesClass};
use Index\Index;

#[CoversClass(Index::class)]
final class IndexTest extends TestCase {
    public function testIndexVersion(): void {
        $expectedVersion = file_get_contents(__DIR__ . '/../VERSION');
        $this->assertIsString($expectedVersion);

        $expectedVersion = trim($expectedVersion);
        $this->assertEquals($expectedVersion, Index::version());
    }
}

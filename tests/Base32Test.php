<?php
// Base32Test.php
// Created: 2021-04-28
// Updated: 2024-07-31

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\Base32;

#[CoversClass(Base32::class)]
final class Base32Test extends TestCase {
    public const TESTS = [
        'A' => '',
        'JVUXGYLLMEQE22LLN52G6' => 'Misaka Mikoto',
        'IFQUCYKBMFAWCQLBIFQUCYKBME' => 'AaAaAaAaAaAaAaAa',
        'JEQGC3JAM5XWS3THEB2G6IDHN4QHI3ZAORUGKIDTN52XAIDTORXXEZJAORXSAYTVPEQGG3DPORUGK4ZO' => 'I am going to go to the soup store to buy clothes.',
        'EFACGJBFLYTCUKBJ' => '!@#$%^&*()',
    ];

    public function testDecode(): void {
        foreach(self::TESTS as $value => $expected)
            $this->assertEquals($expected, Base32::decode($value));
    }

    public function testEncode(): void {
        foreach(self::TESTS as $expected => $value)
            $this->assertEquals($expected, Base32::encode($value));
    }
}

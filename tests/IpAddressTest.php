<?php
// IpAddressTest.php
// Created: 2021-04-26
// Updated: 2025-01-18

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\Net\{IpAddress,IpAddressVersion};

#[CoversClass(IpAddress::class)]
#[CoversClass(IpAddressVersion::class)]
final class IpAddressTest extends TestCase {
    public function testParsing(): void {
        $v4 = IpAddress::parse('127.0.0.1');
        $this->assertEquals('127.0.0.1', $v4->address);
        $v6 = IpAddress::parse('::1');
        $this->assertEquals('[::1]', $v6->address);

        $this->expectException(InvalidArgumentException::class);
        $v0 = IpAddress::parse('12.4:424');
    }

    public function testVersion(): void {
        $v4 = IpAddress::parse('123.53.65.34');
        $this->assertEquals(IpAddressVersion::V4, $v4->version);
        $this->assertEquals(4, $v4->version->value);
        $this->assertEquals(4, $v4->width);
        $this->assertTrue($v4->v4);
        $this->assertFalse($v4->v6);

        $v6 = IpAddress::parse('23cd::43c4:2342:acde');
        $this->assertEquals(IpAddressVersion::V6, $v6->version);
        $this->assertEquals(6, $v6->version->value);
        $this->assertEquals(16, $v6->width);
        $this->assertTrue($v6->v6);
        $this->assertFalse($v6->v4);
    }

    public function testToString(): void {
        $v4 = IpAddress::parse('53.64.123.86');
        $this->assertEquals('53.64.123.86', (string)$v4);

        $v6 = IpAddress::parse('abcd:1234::43:211a');
        $this->assertEquals('abcd:1234::43:211a', (string)$v6);
    }

    public function testEquals(): void {
        $v41 = IpAddress::parse('53.64.123.86');
        $v42 = IpAddress::parse('123.53.65.34');
        $v61 = IpAddress::parse('abcd:1234::43:211a');
        $v62 = IpAddress::parse('23cd::43c4:2342:acde');

        $this->assertTrue($v41->equals($v41));
        $this->assertFalse($v42->equals($v41));
        $this->assertFalse($v61->equals($v42));
        $this->assertTrue($v62->equals($v62));
    }

    public function testJsonSerialize(): void {
        $addrs = [
            '53.64.123.86',
            '123.53.65.34',
            'abcd:1234::43:211a',
            '23cd::43c4:2342:acde',
        ];

        foreach($addrs as $addr)
            $this->assertEquals(json_encode($addr), json_encode(IpAddress::parse($addr)));
    }

    public function testSerializable(): void {
        $addrs = [
            '53.64.123.86',
            '123.53.65.34',
            'abcd:1234::43:211a',
            '23cd::43c4:2342:acde',
        ];

        foreach($addrs as $addr) {
            $obj = IpAddress::parse($addr);
            $ser = serialize($obj);
            $unser = unserialize($ser);
            $this->assertTrue($obj->equals($unser));
        }
    }
}

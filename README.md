# Index

Index is a common library for my PHP projects.

It provides a number of components that I would otherwise copy between projects (and thus become out-of-sync) as well as a number of quality of life things on top of standard PHP stdlib functionality.


## Requirements and Dependencies

Index currently targets **PHP 8.3** with the `mbstring` extension installed.

### `Index\Cache\Memcached`

Requires either the `memcached` or `memcache` extension, if both are installed the `memcached` based implementation will be used.
For full functionality you will want the `memcached` implementation.
The way each implementation stores data is NOT guaranteed to be compatible, please do not mix them.

### `Index\Cache\Valkey`

Requires the `redis` extension.
Valkey is the name of the Linux Foundation's fork of Redis.

### `Index\Db\MariaDb`

Requires the `mysqli` extension.
`mysqlnd` is recommended as the underlying driver, but `libmysql` should work without a hitch.
This driver also works for MySQL as the dependencies would suggest, but you should consider using MariaDB instead of possible.

### `Index\Db\Sqlite`

Requires the `sqlite3` extension.


## Versioning

Index versioning will mostly follow the [Semantic Versioning specification v2.0.0](https://semver.org/spec/v2.0.0.html), counting dropped support for a minor PHP version (e.g. 7.1 -> 7.2 or 7.4 -> 8.0) as a reason to increment the major version.
Previous major versions may be supported for a time with backports depending on what projects of mine still target older versions of PHP.

The version is stored in the root of the repository in a file called `VERSION` and can be read out within Index using `Index\Index::version()`.


## Contribution

By submitting code for inclusion in the main Index source tree you agree to transfer ownership of the code to the project owner.
The contributor will still be attributed for the contributed code, unless they ask for this attribution to be removed.
This is to avoid intellectual property rights traps and drama that could lead to blackmail situations.
If you do not agree with these terms, you are free to fork off.


## Licencing

Index is available under the BSD 3-Clause Clear License, a full version of which is enclosed in the LICENCE file.

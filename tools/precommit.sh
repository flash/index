#!/usr/bin/env bash

pushd .
cd $(dirname "$0")

./update-headers
./update-version

popd

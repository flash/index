<?php
// HttpHeader.php
// Created: 2022-02-14
// Updated: 2025-02-27

namespace Index\Http;

use Stringable;

/**
 * Represents a generic HTTP header.
 */
class HttpHeader implements Stringable {
    /**
     * Lines of the header.
     *
     * @var string[]
     */
    public private(set) array $lines;

    /**
     * @param string $name Name of the header.
     * @param string ...$lines Lines of the header.
     */
    public function __construct(
        public private(set) string $name,
        string ...$lines
    ) {
        $this->lines = $lines;
    }

    /**
     * First line of the header.
     *
     * @var string
     */
    public string $firstLine {
        get => $this->lines[0];
    }

    public function __toString(): string {
        return implode(', ', $this->lines);
    }
}

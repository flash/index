<?php
// HtmlHttpErrorHandler.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http;

/**
 * Represents a basic HTML error message handler for building HTTP response messages.
 */
class HtmlHttpErrorHandler implements HttpErrorHandler {
    private const TEMPLATE = <<<HTML
    <!doctype html>
    <html>
        <head>
            <meta charset=":charset">
            <title>:code :message</title>
        </head>
        <body>
            <center><h1>:code :message</h1><center>
            <hr>
            <center>Index</center>
        </body>
    </html>
    HTML;

    public function handle(HttpResponseBuilder $response, HttpRequest $request, int $code, string $message): void {
        $response->setTypeHtml();

        $charSet = mb_preferred_mime_name(mb_internal_encoding());
        if($charSet === false)
            $charSet = 'UTF-8';

        $response->content = strtr(self::TEMPLATE, [
            ':charset' => strtolower($charSet),
            ':code' => sprintf('%03d', $code),
            ':message' => $message,
        ]);
    }
}

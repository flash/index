<?php
// HttpMessage.php
// Created: 2022-02-08
// Updated: 2025-01-18

namespace Index\Http;

use RuntimeException;

/**
 * Represents a base HTTP message.
 */
abstract class HttpMessage {
    /**
     * @param string $version HTTP message version.
     * @param HttpHeaders $headers HTTP message headers.
     * @param ?HttpContent $content Body contents.
     */
    public function __construct(
        public private(set) string $version,
        public private(set) HttpHeaders $headers,
        public private(set) ?HttpContent $content
    ) {}

    /**
     * Checks if a header is present.
     *
     * @param string $name Name of the header.
     * @return bool true if the header is present.
     */
    public function hasHeader(string $name): bool {
        return $this->headers->hasHeader($name);
    }

    /**
     * Retrieves a header by name.
     *
     * @param string $name Name of the header.
     * @throws RuntimeException If no header with $name exists.
     * @return HttpHeader Instance of the requested header.
     */
    public function getHeader(string $name): HttpHeader {
        return $this->headers->getHeader($name);
    }

    /**
     * Gets the contents of a header as a string.
     *
     * @param string $name Name of the header.
     * @return string Contents of the header.
     */
    public function getHeaderLine(string $name): string {
        return $this->headers->getHeaderLine($name);
    }

    /**
     * Gets lines of a header.
     *
     * @param string $name Name of the header.
     * @return string[] Header lines.
     */
    public function getHeaderLines(string $name): array {
        return $this->headers->getHeaderLines($name);
    }

    /**
     * Gets the first line of a header.
     *
     * @param string $name Name of the header.
     * @return string First line of the header.
     */
    public function getHeaderFirstLine(string $name): string {
        return $this->headers->getHeaderFirstLine($name);
    }
}

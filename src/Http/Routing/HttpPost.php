<?php
// HttpPost.php
// Created: 2024-03-28
// Updated: 2024-08-01

namespace Index\Http\Routing;

use Attribute;

/**
 * Provides an attribute for marking methods in a class as a POST route.
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class HttpPost extends HttpRoute {
    /**
     * @param string $path Path this route represents.
     */
    public function __construct(string $path) {
        parent::__construct('POST', $path);
    }
}

<?php
// HandlerAttribute.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http\Routing;

use ReflectionAttribute;
use ReflectionObject;

/**
 * Provides base for attributes that mark methods in a class as handlers.
 */
abstract class HandlerAttribute {
    /**
     * @param string $path Target path.
     */
    public function __construct(
        public private(set) string $path
    ) {}

    /**
     * Reads attributes from methods in a RouteHandler instance and registers them to a given Router instance.
     *
     * @param Router $router Router instance.
     * @param RouteHandler $handler Handler instance.
     */
    public static function register(Router $router, RouteHandler $handler): void {
        $objectInfo = new ReflectionObject($handler);
        $methodInfos = $objectInfo->getMethods();

        foreach($methodInfos as $methodInfo) {
            $attrInfos = $methodInfo->getAttributes(HandlerAttribute::class, ReflectionAttribute::IS_INSTANCEOF);

            foreach($attrInfos as $attrInfo) {
                $handlerInfo = $attrInfo->newInstance();
                $closure = $methodInfo->getClosure($methodInfo->isStatic() ? null : $handler);

                if($handlerInfo instanceof HttpRoute)
                    $router->add($handlerInfo->method, $handlerInfo->path, $closure);
                else
                    $router->use($handlerInfo->path, $closure);
            }
        }
    }
}

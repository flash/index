<?php
// RouterCommon.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http\Routing;

/**
 * Contains implementations for HTTP request method handler registration.
 */
trait RouterCommon {
    public function get(string $path, callable $handler): void {
        $this->add('GET', $path, $handler);
    }

    public function post(string $path, callable $handler): void {
        $this->add('POST', $path, $handler);
    }

    public function delete(string $path, callable $handler): void {
        $this->add('DELETE', $path, $handler);
    }

    public function patch(string $path, callable $handler): void {
        $this->add('PATCH', $path, $handler);
    }

    public function put(string $path, callable $handler): void {
        $this->add('PUT', $path, $handler);
    }

    public function options(string $path, callable $handler): void {
        $this->add('OPTIONS', $path, $handler);
    }

    public function register(RouteHandler $handler): void {
        $handler->registerRoutes($this);
    }
}

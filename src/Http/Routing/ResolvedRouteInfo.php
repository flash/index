<?php
// ResolvedRouteInfo.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http\Routing;

/**
 * Represents a resolved route.
 */
class ResolvedRouteInfo {
    /**
     * @param array{callable, mixed[]}[] $middlewares Middlewares that should be run prior to the route handler.
     * @param string[] $supportedMethods HTTP methods that this route accepts.
     * @param (callable(): mixed)|null $handler Route handler.
     * @param mixed[] $args Argument list to pass to the middleware and route handlers.
     */
    public function __construct(
        private array $middlewares,
        public private(set) array $supportedMethods,
        private mixed $handler,
        private array $args,
    ) {}

    /**
     * Run middleware handlers.
     *
     * @param mixed[] $args Additional arguments to pass to the middleware handlers.
     * @return mixed Return value from the first middleware to return anything non-null, otherwise null.
     */
    public function runMiddleware(array $args): mixed {
        foreach($this->middlewares as $middleware) {
            $result = $middleware[0](...array_merge($args, $middleware[1]));
            if($result !== null)
                return $result;
        }

        return null;
    }

    /**
     * Whether this route has a handler.
     *
     * @return bool true if it does.
     */
    public function hasHandler(): bool {
        return is_callable($this->handler);
    }

    /**
     * Dispatches this route.
     *
     * @param mixed[] $args Additional arguments to pass to the route handler.
     * @return mixed Return value of the route handler.
     */
    public function dispatch(array $args): mixed {
        if(!is_callable($this->handler))
            return null;

        return ($this->handler)(...array_merge($args, $this->args));
    }
}

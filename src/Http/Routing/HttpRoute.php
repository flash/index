<?php
// HttpRoute.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http\Routing;

use Attribute;

/**
 * Provides an attribute for marking methods in a class as a route.
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class HttpRoute extends HandlerAttribute {
    /**
     * @param string $method
     * @param string $path
     */
    public function __construct(
        public private(set) string $method,
        string $path
    ) {
        parent::__construct($path);
    }
}

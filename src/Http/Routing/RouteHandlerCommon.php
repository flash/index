<?php
// RouteHandlerCommon.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http\Routing;

/**
 * Provides an implementation of RouteHandler::registerRoutes that uses the attributes.
 * For more advanced use, everything can be use'd separately and HandlerAttribute::register called manually.
 */
trait RouteHandlerCommon {
    public function registerRoutes(Router $router): void {
        HandlerAttribute::register($router, $this);
    }
}

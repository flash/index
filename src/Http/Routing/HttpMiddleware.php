<?php
// HttpMiddleware.php
// Created: 2024-03-28
// Updated: 2024-03-28

namespace Index\Http\Routing;

use Attribute;

/**
 * Provides an attribute for marking methods in a class as middleware.
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class HttpMiddleware extends HandlerAttribute {}

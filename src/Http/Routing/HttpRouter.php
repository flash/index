<?php
// HttpRouter.php
// Created: 2024-03-28
// Updated: 2025-02-27

namespace Index\Http\Routing;

use stdClass;
use InvalidArgumentException;
use Index\Bencode\BencodeHttpContentHandler;
use Index\Http\{
    HtmlHttpErrorHandler,HttpContentHandler,HttpErrorHandler,HttpResponse,
    HttpResponseBuilder,HttpRequest,PlainHttpErrorHandler,StringHttpContent
};
use Index\Json\JsonHttpContentHandler;

class HttpRouter implements Router {
    use RouterCommon;

    /** @var array{handler: callable, match?: string, prefix?: string}[] */
    private array $middlewares = [];

    /** @var array<string, array<string, callable>> */
    private array $staticRoutes = [];

    /** @var array<string, array<string, callable>> */
    private array $dynamicRoutes = [];

    /** @var HttpContentHandler[] */
    private array $contentHandlers = [];

    private string $charSetValue;

    /**
     * @param string $charSet Default character set to specify when none is present.
     * @param HttpErrorHandler|string $errorHandler Error handling to use for error responses with an empty body. 'html' for the default HTML implementation, 'plain' for the plaintext implementation.
     * @param bool $registerDefaultContentHandlers true to register default content handlers for JSON, Bencode, etc.
     */
    public function __construct(
        string $charSet = '',
        HttpErrorHandler|string $errorHandler = 'html',
        bool $registerDefaultContentHandlers = true,
    ) {
        $this->charSetValue = $charSet;
        $this->errorHandler = $errorHandler;

        if($registerDefaultContentHandlers)
            $this->registerDefaultContentHandlers();
    }

    /**
     * Retrieves the normalised name of the preferred character set.
     *
     * @return string Normalised character set name.
     */
    public string $charSet {
        get {
            if($this->charSetValue === '') {
                $charSet = mb_preferred_mime_name(mb_internal_encoding());
                if($charSet === false)
                    $charSet = 'UTF-8';

                return strtolower($charSet);
            }

            return $this->charSetValue;
        }
    }

    /**
     * Error handler instance.
     *
     * @var HttpErrorHandler
     */
    public HttpErrorHandler $errorHandler {
        get => $this->errorHandler;
        set(HttpErrorHandler|string $handler) {
            if($handler instanceof HttpErrorHandler)
                $this->errorHandler = $handler;
            elseif($handler === 'html')
                $this->setHtmlErrorHandler();
            else // plain
                $this->setPlainErrorHandler();
        }
    }

    /**
     * Set the error handler to the basic HTML one.
     */
    public function setHtmlErrorHandler(): void {
        $this->errorHandler = new HtmlHttpErrorHandler;
    }

    /**
     * Set the error handler to the plain text one.
     */
    public function setPlainErrorHandler(): void {
        $this->errorHandler = new PlainHttpErrorHandler;
    }

    /**
     * Register a message body content handler.
     *
     * @param HttpContentHandler $contentHandler Content handler to register.
     */
    public function registerContentHandler(HttpContentHandler $contentHandler): void {
        if(!in_array($contentHandler, $this->contentHandlers))
            $this->contentHandlers[] = $contentHandler;
    }

    /**
     * Register the default content handlers.
     */
    public function registerDefaultContentHandlers(): void {
        $this->registerContentHandler(new JsonHttpContentHandler);
        $this->registerContentHandler(new BencodeHttpContentHandler);
    }

    /**
     * Retrieve a scoped router to a given path prefix.
     *
     * @param string $prefix Prefix to apply to paths within the returned router.
     * @return Router Scopes router proxy.
     */
    public function scopeTo(string $prefix): Router {
        return new ScopedRouter($this, $prefix);
    }

    private static function preparePath(string $path, bool $prefixMatch): string|false {
        // this sucks lol
        if(!str_contains($path, '(') || !str_contains($path, ')'))
            return false;

        // make trailing slash optional
        if(!$prefixMatch && str_ends_with($path, '/'))
            $path .= '?';

        return sprintf('#^%s%s#su', $path, $prefixMatch ? '' : '$');
    }

    /**
     * Registers a middleware handler.
     *
     * @param string $path Path prefix or regex to apply this middleware on.
     * @param callable $handler Middleware handler.
     */
    public function use(string $path, callable $handler): void {
        $mwInfo = [];
        $mwInfo['handler'] = $handler;

        $prepared = self::preparePath($path, true);
        if($prepared === false) {
            if(str_ends_with($path, '/'))
                $path = substr($path, 0, -1);

            $mwInfo['prefix'] = $path;
        } else
            $mwInfo['match'] = $prepared;

        $this->middlewares[] = $mwInfo;
    }

    /**
     * Registers a route handler for a given method and path.
     *
     * @param string $method Method to use this handler for.
     * @param string $path Path or regex to use this handler with.
     * @param callable $handler Handler to use for this method/path combination.
     * @throws InvalidArgumentException If $method is empty.
     * @throws InvalidArgumentException If $method starts or ends with spaces.
     */
    public function add(string $method, string $path, callable $handler): void {
        if($method === '')
            throw new InvalidArgumentException('$method may not be empty');

        $method = strtoupper($method);
        if(trim($method) !== $method)
            throw new InvalidArgumentException('$method may start or end with whitespace');

        $prepared = self::preparePath($path, false);
        if($prepared === false) {
            if(str_ends_with($path, '/'))
                $path = substr($path, 0, -1);

            if(array_key_exists($path, $this->staticRoutes))
                $this->staticRoutes[$path][$method] = $handler;
            else
                $this->staticRoutes[$path] = [$method => $handler];
        } else {
            if(array_key_exists($prepared, $this->dynamicRoutes))
                $this->dynamicRoutes[$prepared][$method] = $handler;
            else
                $this->dynamicRoutes[$prepared] = [$method => $handler];
        }
    }

    /**
     * Resolves middlewares and a route handler for a given method and path.
     *
     * @param string $method Method to resolve for.
     * @param string $path Path to resolve for.
     * @return ResolvedRouteInfo Resolved route information.
     */
    public function resolve(string $method, string $path): ResolvedRouteInfo {
        if(str_ends_with($path, '/'))
            $path = substr($path, 0, -1);

        $middlewares = [];

        foreach($this->middlewares as $mwInfo) {
            if(array_key_exists('match', $mwInfo)) {
                if(preg_match($mwInfo['match'], $path, $args) !== 1)
                    continue;

                array_shift($args);
            } elseif(array_key_exists('prefix', $mwInfo)) {
                if($mwInfo['prefix'] !== '' && !str_starts_with($path, $mwInfo['prefix']))
                    continue;

                $args = [];
            } else continue;

            $middlewares[] = [$mwInfo['handler'], $args];
        }

        $methods = [];

        if(array_key_exists($path, $this->staticRoutes)) {
            foreach($this->staticRoutes[$path] as $sMethodName => $sMethodHandler)
                $methods[$sMethodName] = [$sMethodHandler, []];
        } else {
            foreach($this->dynamicRoutes as $rPattern => $rMethods)
                if(preg_match($rPattern, $path, $args) === 1)
                    foreach($rMethods as $rMethodName => $rMethodHandler)
                        if(!array_key_exists($rMethodName, $methods))
                            $methods[$rMethodName] = [$rMethodHandler, array_slice($args, 1)];
        }

        $method = strtoupper($method);
        if(array_key_exists($method, $methods)) {
            [$handler, $args] = $methods[$method];
        } elseif($method === 'HEAD' && array_key_exists('GET', $methods)) {
            [$handler, $args] = $methods['GET'];
        } else {
            $handler = null;
            $args = [];
        }

        return new ResolvedRouteInfo($middlewares, array_keys($methods), $handler, $args);
    }

    /**
     * Dispatches a route based on a given HTTP request message with additional prefix arguments and output to stdout.
     *
     * @param ?HttpRequest $request HTTP request message to handle, null to use the current request.
     * @param mixed[] $args Additional arguments to prepend to the argument list sent to the middleware and route handlers.
     */
    public function dispatch(?HttpRequest $request = null, array $args = []): void {
        $request ??= HttpRequest::fromRequest();
        $response = new HttpResponseBuilder;
        $args = array_merge([$response, $request], $args);

        $routeInfo = $this->resolve($request->method, $request->path);

        // always run middleware regardless of 404 or 405
        $result = $routeInfo->runMiddleware($args);
        if($result === null) {
            if(!$routeInfo->hasHandler()) {
                if(empty($routeInfo->supportedMethods)) {
                    $result = 404;
                } else {
                    $result = 405;
                    $response->setHeader('Allow', implode(', ', $routeInfo->supportedMethods));
                }
            } else
                $result = $routeInfo->dispatch($args);
        }

        if(is_int($result)) {
            if($result >= 100 && $result < 600)
                $this->writeErrorPage($response, $request, $result);
            else
                $response->content = new StringHttpContent((string)$result);
        } elseif(empty($response->content)) {
            foreach($this->contentHandlers as $contentHandler)
                if($contentHandler->match($result)) {
                    $contentHandler->handle($response, $result);
                    break;
                }

            if(empty($response->content) && is_scalar($result)) {
                $result = (string)$result;
                $response->content = new StringHttpContent($result);

                if(!$response->hasContentType()) {
                    if(strtolower(substr($result, 0, 14)) === '<!doctype html')
                        $response->setTypeHtml($this->charSet);
                    else {
                        $charset = mb_detect_encoding($result);
                        if($charset !== false)
                            $charset = mb_preferred_mime_name($charset);
                        $charset = $charset === false ? 'utf-8' : strtolower($charset);

                        if(strtolower(substr($result, 0, 5)) === '<?xml')
                            $response->setTypeXml($charset);
                        else
                            $response->setTypePlain($charset);
                    }
                }
            }
        }

        self::output($response->toResponse(), $request->method !== 'HEAD');
    }

    /**
     * Writes an error page to a given HTTP response builder.
     *
     * @param HttpResponseBuilder $response HTTP response builder to apply the error page to.
     * @param HttpRequest $request HTTP request that triggered this error.
     * @param int $statusCode HTTP status code for this error page.
     */
    public function writeErrorPage(HttpResponseBuilder $response, HttpRequest $request, int $statusCode): void {
        $this->errorHandler->handle(
            $response,
            $request,
            $response->statusCode = $statusCode,
            $response->statusText
        );
    }

    /**
     * Outputs a HTTP response message to stdout.
     *
     * @param HttpResponse $response HTTP response message to output.
     * @param bool $includeBody true to include the response message body, false to omit it for HEAD requests.
     */
    public static function output(HttpResponse $response, bool $includeBody): void {
        header(sprintf(
            'HTTP/%s %03d %s',
            $response->version,
            $response->statusCode,
            $response->statusText
        ));

        foreach($response->headers->all as $header)
            foreach($header->lines as $line)
                header(sprintf('%s: %s', $header->name, (string)$line));

        if($includeBody && !empty($response->content)) {
            if(is_resource($response->content))
                echo stream_get_contents($response->content);
            else
                echo (string)$response->content;
        }
    }
}

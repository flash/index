<?php
// RouteHandler.php
// Created: 2024-03-28
// Updated: 2024-10-02

namespace Index\Http\Routing;

/**
 * Provides the interface for Router::register().
 */
interface RouteHandler {
    /**
     * Registers routes on a given Router instance.
     *
     * @param Router $router Target router.
     */
    public function registerRoutes(Router $router): void;
}

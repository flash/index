<?php
// Router.php
// Created: 2024-03-28
// Updated: 2024-10-03

namespace Index\Http\Routing;

use InvalidArgumentException;
use RuntimeException;
use Index\Http\HttpRequest;

/**
 * Provides an interface for defining HTTP routers.
 */
interface Router {
    /**
     * Creates a scoped version of this router.
     *
     * @param string $prefix Prefix path to prepend to all registered routes.
     * @return Router Scoped router.
     */
    public function scopeTo(string $prefix): Router;

    /**
     * Apply middleware functions to a path.
     *
     * @param string $path Path to apply the middleware to.
     * @param callable $handler Middleware function.
     */
    public function use(string $path, callable $handler): void;

    /**
     * Adds a new route.
     *
     * @param string $method Request method.
     * @param string $path Request path.
     * @param callable $handler Request handler.
     * @throws InvalidArgumentException if $method is not a valid method name
     */
    public function add(string $method, string $path, callable $handler): void;

    /**
     * Resolves a route
     *
     * @param string $method Request method.
     * @param string $path Request path.
     * @return ResolvedRouteInfo Response route.
     */
    public function resolve(string $method, string $path): ResolvedRouteInfo;

    /**
     * Adds a new GET route.
     *
     * @param string $path Request path.
     * @param callable $handler Request handler.
     */
    public function get(string $path, callable $handler): void;

    /**
     * Adds a new POST route.
     *
     * @param string $path Request path.
     * @param callable $handler Request handler.
     */
    public function post(string $path, callable $handler): void;

    /**
     * Adds a new DELETE route.
     *
     * @param string $path Request path.
     * @param callable $handler Request handler.
     */
    public function delete(string $path, callable $handler): void;

    /**
     * Adds a new PATCH route.
     *
     * @param string $path Request path.
     * @param callable $handler Request handler.
     */
    public function patch(string $path, callable $handler): void;

    /**
     * Adds a new PUT route.
     *
     * @param string $path Request path.
     * @param callable $handler Request handler.
     */
    public function put(string $path, callable $handler): void;

    /**
     * Adds a new OPTIONS route.
     *
     * @param string $path Request path.
     * @param callable $handler Request handler.
     */
    public function options(string $path, callable $handler): void;

    /**
     * Registers routes in an RouteHandler implementation.
     *
     * @param RouteHandler $handler Routes handler.
     */
    public function register(RouteHandler $handler): void;
}

<?php
// ScopedRouter.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http\Routing;

/**
 * Provides a scoped router interface, automatically adds a prefix to any routes added.
 */
class ScopedRouter implements Router {
    use RouterCommon;

    /**
     * @param Router $router Underlying router.
     * @param string $prefix Base path to use as a prefix.
     */
    public function __construct(
        private Router $router,
        private string $prefix
    ) {
        if($router instanceof ScopedRouter)
            $this->router = $router->router;

        // TODO: cleanup prefix
    }

    public function scopeTo(string $prefix): Router {
        return $this->router->scopeTo($this->prefix . $prefix);
    }

    public function add(string $method, string $path, callable $handler): void {
        $this->router->add($method, $this->prefix . $path, $handler);
    }

    public function use(string $path, callable $handler): void {
        $this->router->use($this->prefix . $path, $handler);
    }

    public function resolve(string $method, string $path): ResolvedRouteInfo {
        return $this->router->resolve($method, $this->prefix . $path);
    }
}

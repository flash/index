<?php
// HttpHeadersBuilder.php
// Created: 2022-02-08
// Updated: 2024-08-03

namespace Index\Http;

use InvalidArgumentException;

/**
 * Represents a HTTP message header builder.
 */
class HttpHeadersBuilder {
    /** @var array<string, string[]> */
    private array $headers = [];

    /**
     * Adds a header to the HTTP message.
     * If a header with the same name is already present, it will be appended with a new line.
     *
     * @param string $name Name of the header to add.
     * @param string $value Value to apply for this header.
     */
    public function addHeader(string $name, string $value): void {
        $nameLower = strtolower($name);
        if(!isset($this->headers[$nameLower]))
            $this->headers[$nameLower] = [$name];
        $this->headers[$nameLower][] = $value;
    }

    /**
     * Sets a header to the HTTP message.
     * If a header with the same name is already present, it will be overwritten.
     *
     * @param string $name Name of the header to set.
     * @param string $value Value to apply for this header.
     */
    public function setHeader(string $name, string $value): void {
        $this->headers[strtolower($name)] = [$name, $value];
    }

    /**
     * Removes a header from the HTTP message.
     *
     * @param string $name Name of the header to remove.
     */
    public function removeHeader(string $name): void {
        unset($this->headers[strtolower($name)]);
    }

    /**
     * Checks if a header is already present.
     *
     * @param string $name Name of the header.
     * @return bool true if it is present.
     */
    public function hasHeader(string $name): bool {
        return isset($this->headers[strtolower($name)]);
    }

    /**
     * Create HttpHeaders instance from this builder.
     *
     * @return HttpHeaders Instance containing HTTP headers.
     */
    public function toHeaders(): HttpHeaders {
        $headers = [];

        foreach($this->headers as $index => $lines)
            $headers[] = new HttpHeader(array_shift($lines) ?? '', ...$lines);

        return new HttpHeaders($headers);
    }
}

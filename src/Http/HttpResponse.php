<?php
// HttpResponse.php
// Created: 2022-02-08
// Updated: 2025-01-18

namespace Index\Http;

/**
 * Represents a HTTP response message.
 */
class HttpResponse extends HttpMessage {
    /**
     * @param string $version HTTP message version.
     * @param int $statusCode HTTP response status code.
     * @param string $statusText HTTP response status text.
     * @param HttpHeaders $headers HTTP message headers.
     * @param ?HttpContent $content Body contents.
     */
    public function __construct(
        string $version,
        public private(set) int $statusCode,
        public private(set) string $statusText,
        HttpHeaders $headers,
        ?HttpContent $content
    ) {
        parent::__construct($version, $headers, $content);
    }
}

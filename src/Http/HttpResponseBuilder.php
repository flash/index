<?php
// HttpResponseBuilder.php
// Created: 2022-02-08
// Updated: 2025-02-27

namespace Index\Http;

use DateTimeInterface;
use Index\{UrlEncoding,MediaType,XDateTime};
use Index\Performance\Timings;

/**
 * Represents a HTTP response message builder.
 */
class HttpResponseBuilder extends HttpMessageBuilder {
    private ?string $statusTextValue = null;

    /** @var string[] */
    private array $vary = [];

    /**
     * HTTP status code for the target HTTP response message.
     *
     * @var int
     */
    public int $statusCode = 200;

    /**
     * Status text for this response.
     *
     * @var string
     */
    public string $statusText {
        get => $this->statusTextValue ?? self::STATUS[$this->statusCode] ?? 'Unknown Status';
        set(string $value) {
            $this->statusTextValue = $value === '' ? null : $value;
        }
    }

    /**
     * Adds a cookie to this response.
     *
     * @param string $name Name of the cookie.
     * @param string $value Value of the cookie.
     * @param DateTimeInterface|int|null $expires Point at which the cookie should expire.
     * @param string $path Path to which to apply the cookie.
     * @param string $domain Domain name to which to apply the cookie.
     * @param bool $secure true to only make the client include this cookie in a secure context.
     * @param bool $httpOnly true to make the client hide this cookie to Javascript code.
     * @param bool $sameSiteStrict true to set the SameSite attribute to Strict, false to set it to Lax.
     */
    public function addCookie(
        string $name,
        string $value,
        DateTimeInterface|int|null $expires = null,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = false,
        bool $sameSiteStrict = false
    ): void {
        $cookie = rawurlencode($name) . '=' . rawurlencode($value)
            . '; SameSite=' . ($sameSiteStrict ? 'Strict' : 'Lax');

        if($expires !== null)
            $cookie .= '; Expires=' . XDateTime::toCookieString($expires);

        if(!empty($domain))
            $cookie .= '; Domain=' . $domain;

        if(!empty($path))
            $cookie .= '; Path=' . $path;

        if($secure)
            $cookie .= '; Secure';

        if($httpOnly)
            $cookie .= '; HttpOnly';

        $this->addHeader('Set-Cookie', $cookie);
    }

    /**
     * Make the client remove a cookie.
     *
     * @param string $name Name of the cookie.
     * @param string $path Path to which the cookie was applied to cookie.
     * @param string $domain Domain name to which the cookie was applied to cookie.
     * @param bool $secure true to only make the client include this cookie in a secure context.
     * @param bool $httpOnly true to make the client hide this cookie to Javascript code.
     * @param bool $sameSiteStrict true to set the SameSite attribute to Strict, false to set it to Lax.
     */
    public function removeCookie(
        string $name,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = false,
        bool $sameSiteStrict = false
    ): void {
        $this->addCookie($name, '', -9001, $path, $domain, $secure, $httpOnly, $sameSiteStrict);
    }

    /**
     * Specifies a redirect header on the response message.
     *
     * @param string $to Target to redirect to.
     * @param bool $permanent true for status code 301, false for status code 302. Makes the client cache the redirect.
     */
    public function redirect(string $to, bool $permanent = false): void {
        $this->statusCode = $permanent ? 301 : 302;
        $this->setHeader('Location', $to);
    }

    /**
     * Sets a Vary header.
     *
     * @param string|string[] $headers Header or headers that will vary.
     */
    public function addVary(string|array $headers): void {
        if(!is_array($headers))
            $headers = [$headers];

        foreach($headers as $header) {
            $header = $header;
            if(!in_array($header, $this->vary))
                $this->vary[] = $header;
        }

        $this->setHeader('Vary', implode(', ', $this->vary));
    }

    /**
     * Sets an X-Powered-By header.
     *
     * @param string $poweredBy Thing that your website is powered by.
     */
    public function setPoweredBy(string $poweredBy): void {
        $this->setHeader('X-Powered-By', $poweredBy);
    }

    /**
     * Sets an ETag header.
     *
     * @param string $eTag Unique identifier for the current state of the content.
     * @param bool $weak Whether to use weak matching.
     */
    public function setEntityTag(string $eTag, bool $weak = false): void {
        $eTag = '"' . $eTag . '"';

        if($weak)
            $eTag = 'W/' . $eTag;

        $this->setHeader('ETag', $eTag);
    }

    /**
     * Sets a Server-Timing header.
     *
     * @param Timings $timings Timings to supply to the devtools.
     */
    public function setServerTiming(Timings $timings): void {
        $laps = $timings->laps;
        $timings = [];

        foreach($laps as $lap) {
            $timing = $lap->name;
            if(!empty($lap->comment))
                $timing .= ';desc="' . strtr($lap->comment, ['"' => '\\"']) . '"';

            $timing .= ';dur=' . round($lap->durationTime, 5);
            $timings[] = $timing;
        }

        $this->setHeader('Server-Timing', implode(', ', $timings));
    }

    /**
     * Whether a Content-Type header is present.
     *
     * @return bool true if a Content-Type header is present.
     */
    public function hasContentType(): bool {
        return $this->hasHeader('Content-Type');
    }

    /**
     * Sets a Content-Type header.
     *
     * @param MediaType|string $mediaType Media type to set as the content type of the response body.
     */
    public function setContentType(MediaType|string $mediaType): void {
        $this->setHeader('Content-Type', (string)$mediaType);
    }

    /**
     * Sets the Content-Type to 'application/octet-stream' for raw content.
     */
    public function setTypeStream(): void {
        $this->setContentType('application/octet-stream');
    }

    /**
     * Sets the Content-Type to 'text/plain' with a provided character set for plain text content.
     *
     * @param string $charset Character set.
     */
    public function setTypePlain(string $charset = 'us-ascii'): void {
        $this->setContentType('text/plain; charset=' . $charset);
    }

    /**
     * Sets the Content-Type to 'text/html' with a provided character set for HTML content.
     *
     * @param string $charset Character set.
     */
    public function setTypeHtml(string $charset = 'utf-8'): void {
        $this->setContentType('text/html; charset=' . $charset);
    }

    /**
     * Sets the Content-Type to 'application/json' with a provided character set for JSON content.
     *
     * @param string $charset Character set.
     */
    public function setTypeJson(string $charset = 'utf-8'): void {
        $this->setContentType('application/json; charset=' . $charset);
    }

    /**
     * Sets the Content-Type to 'application/xml' with a provided character set for XML content.
     *
     * @param string $charset Character set.
     */
    public function setTypeXml(string $charset = 'utf-8'): void {
        $this->setContentType('application/xml; charset=' . $charset);
    }

    /**
     * Sets the Content-Type to 'text/css' with a provided character set for CSS content.
     *
     * @param string $charset Character set.
     */
    public function setTypeCss(string $charset = 'utf-8'): void {
        $this->setContentType('text/css; charset=' . $charset);
    }

    /**
     * Sets the Content-Type to 'application/javascript' with a provided character set for Javascript content.
     *
     * @param string $charset Character set.
     */
    public function setTypeJs(string $charset = 'utf-8'): void {
        $this->setContentType('application/javascript; charset=' . $charset);
    }

    /**
     * Specifies an Apache Web Server X-Sendfile header.
     *
     * @param string $absolutePath Absolute path to the content to serve.
     */
    public function sendFile(string $absolutePath): void {
        $this->setHeader('X-Sendfile', $absolutePath);
    }

    /**
     * Specifies an NGINX X-Accel-Redirect header.
     *
     * @param string $uri Relative URI to the content to serve.
     */
    public function accelRedirect(string $uri): void {
        $this->setHeader('X-Accel-Redirect', $uri);
    }

    /**
     * Specifies a Content-Disposition header to supply a filename for the content.
     *
     * @param string $fileName Name of the file.
     * @param bool $attachment true if the browser should prompt the user to download the body.
     */
    public function setFileName(string $fileName, bool $attachment = false): void {
        $this->setHeader(
            'Content-Disposition',
            sprintf(
                '%s; filename="%s"',
                ($attachment ? 'attachment' : 'inline'),
                $fileName
            )
        );
    }

    /**
     * Specifies a Clear-Site-Data header.
     *
     * @param string ...$directives Directives to specify.
     */
    public function clearSiteData(string ...$directives): void {
        $this->setHeader(
            'Clear-Site-Data',
            implode(', ', array_map(fn($directive) => sprintf('"%s"', $directive), $directives))
        );
    }

    /**
     * Specifies a Cache-Control header.
     *
     * @param string ...$directives Directives to specify.
     */
    public function setCacheControl(string ...$directives): void {
        $this->setHeader('Cache-Control', implode(', ', $directives));
    }

    /**
     * Creates an instance of HttpResponse from this builder.
     *
     * @return HttpResponse An instance representing this builder.
     */
    public function toResponse(): HttpResponse {
        return new HttpResponse(
            $this->version,
            $this->statusCode,
            $this->statusText,
            $this->headers->toHeaders(),
            // this is crunchy, this entire pipeline needs to be redone anyway
            $this->content instanceof HttpContent ? $this->content : null
        );
    }

    private const STATUS = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        103 => 'Early Hints',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Required',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        425 => 'Too Early',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
    ];
}

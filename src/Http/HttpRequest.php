<?php
// HttpRequest.php
// Created: 2022-02-08
// Updated: 2025-01-18

namespace Index\Http;

use InvalidArgumentException;
use RuntimeException;
use Index\MediaType;
use Index\Json\JsonHttpContent;

/**
 * Represents a HTTP request message.
 */
class HttpRequest extends HttpMessage {
    /**
     * @param string $remoteAddress Origin remote address.
     * @param bool $secure true if HTTPS.
     * @param string $countryCode Origin ISO 3166 country code.
     * @param string $version HTTP message version.
     * @param string $method HTTP request method.
     * @param string $path HTTP request path.
     * @param array<string, mixed> $params HTTP request query parameters.
     * @param array<string, string> $cookies HTTP request cookies.
     * @param HttpHeaders $headers HTTP message headers.
     * @param ?HttpContent $content Body contents.
     */
    public function __construct(
        public private(set) string $remoteAddress,
        public private(set) bool $secure,
        public private(set) string $countryCode,
        string $version,
        public private(set) string $method,
        public private(set) string $path,
        public private(set) array $params,
        public private(set) array $cookies,
        HttpHeaders $headers,
        ?HttpContent $content
    ) {
        parent::__construct($version, $headers, $content);
    }

    /**
     * Retrieves all HTTP request query fields as a query string.
     *
     * @param bool $spacesAsPlus true if spaces should be represented with a +, false if %20.
     * @return string Query string representation of query fields.
     */
    public function getParamString(bool $spacesAsPlus = false): string {
        return http_build_query($this->params, '', '&', $spacesAsPlus ? PHP_QUERY_RFC1738 : PHP_QUERY_RFC3986);
    }

    /**
     * Retrieves an HTTP request query field, or null if it is not present.
     *
     * @param string $name Name of the request query field.
     * @param int $filter A PHP filter extension filter constant.
     * @param mixed[]|int $options Options for the PHP filter.
     * @return ?mixed Value of the query field, null if not present.
     */
    public function getParam(string $name, int $filter = FILTER_DEFAULT, array|int $options = 0): mixed {
        if(!isset($this->params[$name]))
            return null;
        return filter_var($this->params[$name], $filter, $options);
    }

    /**
     * Retrieves a form field with filtering and enforcing a scalar value.
     *
     * @param string $name Name of the form field.
     * @param int $filter A PHP filter extension filter constant.
     * @param array<string, mixed>|int $options Options for the PHP filter.
     * @return ?scalar Value of the form field, null if not present.
     */
    public function getParamScalar(string $name, int $filter = FILTER_DEFAULT, array|int $options = 0): bool|float|int|string|null {
        $value = $this->getParam($name, $filter, $options);
        return is_scalar($value) ? $value : null;
    }

    /**
     * Checks if a query field is present.
     *
     * @param string $name Name of the query field.
     * @return bool true if the field is present, false if not.
     */
    public function hasParam(string $name): bool {
        return isset($this->params[$name]);
    }

    /**
     * Retrieves an HTTP request cookie, or null if it is not present.
     *
     * @param string $name Name of the request cookie.
     * @param int $filter A PHP filter extension filter constant.
     * @param array<string, mixed>|int $options Options for the PHP filter.
     * @return mixed Value of the cookie, null if not present.
     */
    public function getCookie(string $name, int $filter = FILTER_DEFAULT, array|int $options = 0): mixed {
        if(!isset($this->cookies[$name]))
            return null;
        return filter_var($this->cookies[$name], $filter, $options);
    }

    /**
     * Checks if a cookie is present.
     *
     * @param string $name Name of the cookie.
     * @return bool true if the cookie is present, false if not.
     */
    public function hasCookie(string $name): bool {
        return isset($this->cookies[$name]);
    }

    /**
     * Creates an HttpRequest instance from the current request.
     *
     * @return HttpRequest An instance representing the current request.
     */
    public static function fromRequest(): HttpRequest {
        $build = new HttpRequestBuilder;
        $build->remoteAddress = (string)filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        $build->secure = filter_has_var(INPUT_SERVER, 'HTTPS');
        $build->version = (string)filter_input(INPUT_SERVER, 'SERVER_PROTOCOL');
        $build->method = (string)filter_input(INPUT_SERVER, 'REQUEST_METHOD');

        if(filter_has_var(INPUT_SERVER, 'COUNTRY_CODE'))
            $build->countryCode = (string)filter_input(INPUT_SERVER, 'COUNTRY_CODE');

        // this currently doesn't "properly" support the scenario where a full url is specified in the http request
        $path = (string)filter_input(INPUT_SERVER, 'REQUEST_URI');
        $pathQueryOffset = strpos($path, '?');
        if($pathQueryOffset !== false)
            $path = substr($path, 0, $pathQueryOffset);
        else {
            $pathHashOffset = strpos($path, '#');
            if($pathHashOffset !== false)
                $path = substr($path, 0, $pathHashOffset);
        }

        if(!str_starts_with($path, '/'))
            $path = '/' . $path;

        $build->path = $path;

        /** @var array<string, mixed> $getVars */
        $getVars = $_GET;
        $build->params = $getVars;

        /** @var array<string, string> $cookieVars */
        $cookieVars = $_COOKIE;
        $build->cookies = $cookieVars;

        $contentType = null;
        $contentLength = 0;

        $headers = self::getRawRequestHeaders();
        foreach($headers as $name => $value) {
            if($name === 'content-type')
                try {
                    $contentType = MediaType::parse($value);
                } catch(InvalidArgumentException $ex) {
                    $contentType = null;
                }
            elseif($name === 'content-length')
                $contentLength = (int)$value;

            $build->setHeader($name, $value);
        }

        if($contentType !== null && ($contentType->equals('application/json') || $contentType->equals('application/x-json')))
            $build->content = JsonHttpContent::fromRequest();
        elseif($contentType !== null && ($contentType->equals('application/x-www-form-urlencoded') || $contentType->equals('multipart/form-data')))
            $build->content = FormHttpContent::fromRequest();
        elseif($contentLength > 0)
            $build->content = StringHttpContent::fromRequest();

        return $build->toRequest();
    }

    /** @return array<string, string> */
    private static function getRawRequestHeaders(): array {
        if(function_exists('getallheaders')) {
            $raw = getallheaders();
            $headers = [];
            foreach($raw as $name => $value)
                if(is_string($name) && is_string($value))
                    $headers[strtolower($name)] = $value;

            return $headers;
        }

        $headers = [];

        foreach($_SERVER as $key => $value) {
            if(!is_string($key) || !is_scalar($value))
                continue;

            if(substr($key, 0, 5) === 'HTTP_') {
                $key = str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($key, 5))));
                $headers[$key] = (string)$value;
            } elseif($key === 'CONTENT_TYPE') {
                $headers['content-type'] = (string)$value;
            } elseif($key === 'CONTENT_LENGTH') {
                $headers['content-length'] = (string)$value;
            }
        }

        if(!isset($headers['authorization'])) {
            if(filter_has_var(INPUT_SERVER, 'REDIRECT_HTTP_AUTHORIZATION')) {
                $headers['authorization'] = (string)filter_input(INPUT_SERVER, 'REDIRECT_HTTP_AUTHORIZATION');
            } elseif(filter_has_var(INPUT_SERVER, 'PHP_AUTH_USER')) {
                $headers['authorization'] = sprintf('Basic %s', base64_encode(sprintf(
                    '%s:%s',
                    (string)filter_input(INPUT_SERVER, 'PHP_AUTH_USER'),
                    (string)filter_input(INPUT_SERVER, 'PHP_AUTH_PW')
                )));
            } elseif(filter_has_var(INPUT_SERVER, 'PHP_AUTH_DIGEST')) {
                $headers['authorization'] = (string)filter_input(INPUT_SERVER, 'PHP_AUTH_DIGEST');
            }
        }

        return $headers;
    }
}

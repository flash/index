<?php
// HttpContent.php
// Created: 2022-02-08
// Updated: 2024-10-02

namespace Index\Http;

use Stringable;

/**
 * Represents the body content for a HTTP message.
 */
interface HttpContent extends Stringable {}

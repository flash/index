<?php
// HttpErrorHandler.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http;

/**
 * Represents an error message handler for building HTTP response messages.
 */
interface HttpErrorHandler {
    /**
     * Applies an error message template to the provided HTTP response builder.
     *
     * @param HttpResponseBuilder $response HTTP Response builder to apply this error to.
     * @param HttpRequest $request HTTP Request this error is a response to.
     * @param int $code HTTP status code to apply.
     * @param string $message HTTP status message to apply.
     */
    public function handle(HttpResponseBuilder $response, HttpRequest $request, int $code, string $message): void;
}

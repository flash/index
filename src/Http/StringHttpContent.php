<?php
// StringHttpContent.php
// Created: 2022-02-10
// Updated: 2025-01-18

namespace Index\Http;

use Stringable;

/**
 * Represents string body content for a HTTP message.
 */
class StringHttpContent implements HttpContent {
    /**
     * @param string $string String that represents this message body.
     */
    public function __construct(
        public private(set) string $string
    ) {}

    public function __toString(): string {
        return $this->string;
    }

    /**
     * Creates an instance an existing object.
     *
     * @param Stringable|string $string Object to cast to a string.
     * @return StringHttpContent Instance representing the provided object.
     */
    public static function fromObject(Stringable|string $string): StringHttpContent {
        return new StringHttpContent((string)$string);
    }

    /**
     * Creates an instance from a file.
     *
     * @param string $path Path to the file.
     * @return StringHttpContent Instance representing the provided path.
     */
    public static function fromFile(string $path): StringHttpContent {
        $string = file_get_contents($path);
        if($string === false)
            $string = '';

        return new StringHttpContent($string);
    }

    /**
     * Creates an instance from the raw request body.
     *
     * @return StringHttpContent Instance representing the request body.
     */
    public static function fromRequest(): StringHttpContent {
        return self::fromFile('php://input');
    }
}

<?php
// FormHttpContent.php
// Created: 2022-02-10
// Updated: 2025-01-18

namespace Index\Http;

use RuntimeException;

/**
 * Represents form body content for a HTTP message.
 */
class FormHttpContent implements HttpContent {
    /**
     * @param array<string, mixed> $params Form fields.
     * @param array<string, HttpUploadedFile|array<string, mixed>> $files Uploaded files.
     */
    public function __construct(
        public private(set) array $params,
        public private(set) array $files
    ) {}

    /**
     * Retrieves a form field with filtering.
     *
     * @param string $name Name of the form field.
     * @param int $filter A PHP filter extension filter constant.
     * @param array<string, mixed>|int $options Options for the PHP filter.
     * @return mixed Value of the form field, null if not present.
     */
    public function getParam(string $name, int $filter = FILTER_DEFAULT, array|int $options = 0): mixed {
        if(!isset($this->params[$name]))
            return null;
        return filter_var($this->params[$name] ?? null, $filter, $options);
    }

    /**
     * Retrieves a form field with filtering and enforcing a scalar value.
     *
     * @param string $name Name of the form field.
     * @param int $filter A PHP filter extension filter constant.
     * @param array<string, mixed>|int $options Options for the PHP filter.
     * @return ?scalar Value of the form field, null if not present.
     */
    public function getParamScalar(string $name, int $filter = FILTER_DEFAULT, array|int $options = 0): bool|float|int|string|null {
        $value = $this->getParam($name, $filter, $options);
        return is_scalar($value) ? $value : null;
    }

    /**
     * Checks if a form field is present.
     *
     * @param string $name Name of the form field.
     * @return bool true if the field is present, false if not.
     */
    public function hasParam(string $name): bool {
        return isset($this->params[$name]);
    }

    /**
     * Gets all form fields as a query string.
     *
     * @param bool $spacesAsPlus true if spaces should be represented with a +, false if %20.
     * @return string Query string representation of form fields.
     */
    public function getParamString(bool $spacesAsPlus = false): string {
        return http_build_query($this->params, '', '&', $spacesAsPlus ? PHP_QUERY_RFC1738 : PHP_QUERY_RFC3986);
    }

    /**
     * Checks if a file upload is present.
     *
     * @param string $name Name of the form field.
     * @return bool true if the upload is present, false if not.
     */
    public function hasUploadedFile(string $name): bool {
        return isset($this->files[$name]);
    }

    /**
     * Retrieves a file upload.
     *
     * @param string $name Name of the form field.
     * @throws RuntimeException If no uploaded file with form field $name is present.
     * @return HttpUploadedFile Uploaded file info.
     */
    public function getUploadedFile(string $name): HttpUploadedFile {
        if(!isset($this->files[$name]))
            throw new RuntimeException('No file with name $name present.');
        if(is_array($this->files[$name]))
            throw new RuntimeException('Array based accessors are unsupported currently.');

        return $this->files[$name];
    }

    /**
     * Creates an instance from an array of form fields and uploaded files.
     *
     * @param array<string, mixed> $post Form fields.
     * @param array<string, mixed> $files Uploaded files.
     * @return FormHttpContent Instance representing the request body.
     */
    public static function fromRaw(array $post, array $files): FormHttpContent {
        return new FormHttpContent(
            $post,
            HttpUploadedFile::createFromPhpFiles($files)
        );
    }

    /**
     * Creates an instance from the $_POST and $_FILES superglobals.
     *
     * @return FormHttpContent Instance representing the request body.
     */
    public static function fromRequest(): FormHttpContent {
        /** @var array<string, mixed> $postVars */
        $postVars = $_POST;

        /** @var array<string, mixed> $filesVars */
        $filesVars = $_FILES;

        return self::fromRaw($postVars, $filesVars);
    }

    public function __toString(): string {
        return $this->getParamString();
    }
}

<?php
// PlainHttpErrorHandler.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http;

/**
 * Represents a plain text error message handler for building HTTP response messages.
 */
class PlainHttpErrorHandler implements HttpErrorHandler {
    public function handle(HttpResponseBuilder $response, HttpRequest $request, int $code, string $message): void {
        $response->setTypePlain();
        $response->content = sprintf('HTTP %03d %s', $code, $message);
    }
}

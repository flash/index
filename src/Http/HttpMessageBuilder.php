<?php
// HttpMessageBuilder.php
// Created: 2022-02-08
// Updated: 2025-01-18

namespace Index\Http;

use InvalidArgumentException;
use RuntimeException;
use Stringable;

/**
 * Represents a base HTTP message builder.
 */
class HttpMessageBuilder {
    /**
     * HTTP version of this message.
     *
     * @var string
     */
    public string $version = '1.1';

    /**
     * HTTP header builder instance.
     *
     * @var HttpHeadersBuilder
     */
    public private(set) HttpHeadersBuilder $headers;

    public function __construct() {
        $this->headers = new HttpHeadersBuilder;
    }

    /**
     * Adds a header to the HTTP message.
     * If a header with the same name is already present, it will be appended with a new line.
     *
     * @param string $name Name of the header to add.
     * @param string $value Value to apply for this header.
     */
    public function addHeader(string $name, string $value): void {
        $this->headers->addHeader($name, $value);
    }

    /**
     * Sets a header to the HTTP message.
     * If a header with the same name is already present, it will be overwritten.
     *
     * @param string $name Name of the header to set.
     * @param string $value Value to apply for this header.
     */
    public function setHeader(string $name, string $value): void {
        $this->headers->setHeader($name, $value);
    }

    /**
     * Removes a header from the HTTP message.
     *
     * @param string $name Name of the header to remove.
     */
    public function removeHeader(string $name): void {
        $this->headers->removeHeader($name);
    }

    /**
     * Checks if a header is already present.
     *
     * @param string $name Name of the header.
     * @return bool true if it is present.
     */
    public function hasHeader(string $name): bool {
        return $this->headers->hasHeader($name);
    }

    /**
     * HTTP message body.
     *
     * @var HttpContent|Stringable|string|int|float|resource|null
     */
    public mixed $content = null {
        get => $this->content;
        set(mixed $content) {
            if($content instanceof HttpContent || $content === null) {
                $this->content = $content;
                return;
            }

            if(is_scalar($content) || $content instanceof Stringable) {
                $this->content = new StringHttpContent((string)$content);
                return;
            }

            if(is_resource($content)) {
                $content = stream_get_contents($content);
                if($content === false)
                    throw new RuntimeException('was unable to read the stream resource in $content');

                $this->content = new StringHttpContent($content);
                return;
            }

            throw new InvalidArgumentException('$content not a supported type');
        }
    }
}

<?php
// HttpHeaders.php
// Created: 2022-02-08
// Updated: 2025-02-27

namespace Index\Http;

use RuntimeException;

/**
 * Represents a collection of HTTP headers.
 */
class HttpHeaders {
    /**
     * @param HttpHeader[] $headers HTTP header instances.
     */
    public function __construct(
        private array $headers
    ) {
        $real = [];

        foreach($headers as $header)
            if($header instanceof HttpHeader)
                $real[strtolower($header->name)] = $header;

        $this->headers = $real;
    }

    /**
     * Checks if a header is present.
     *
     * @param string $name Name of the header.
     * @return bool true if the header is present.
     */
    public function hasHeader(string $name): bool {
        return isset($this->headers[strtolower($name)]);
    }

    /**
     * All headers.
     *
     * @var HttpHeader[]
     */
    public array $all {
        get => array_values($this->headers);
    }

    /**
     * Retrieves a header by name.
     *
     * @param string $name Name of the header.
     * @throws RuntimeException If no header with $name exists.
     * @return HttpHeader Instance of the requested header.
     */
    public function getHeader(string $name): HttpHeader {
        $name = strtolower($name);
        if(!isset($this->headers[$name]))
            throw new RuntimeException('No header with that name is present.');

        return $this->headers[$name];
    }

    /**
     * Gets the contents of a header as a string.
     *
     * @param string $name Name of the header.
     * @return string Contents of the header.
     */
    public function getHeaderLine(string $name): string {
        if(!$this->hasHeader($name))
            return '';

        return (string)$this->getHeader($name);
    }

    /**
     * Gets lines of a header.
     *
     * @param string $name Name of the header.
     * @return string[] Header lines.
     */
    public function getHeaderLines(string $name): array {
        if(!$this->hasHeader($name))
            return [];

        return $this->getHeader($name)->lines;
    }

    /**
     * Gets the first line of a header.
     *
     * @param string $name Name of the header.
     * @return string First line of the header.
     */
    public function getHeaderFirstLine(string $name): string {
        if(!$this->hasHeader($name))
            return '';

        return $this->getHeader($name)->firstLine;
    }
}

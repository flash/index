<?php
// HttpRequestBuilder.php
// Created: 2022-02-08
// Updated: 2025-01-18

namespace Index\Http;

use InvalidArgumentException;

/**
 * Represents a HTTP request message builder.
 */
class HttpRequestBuilder extends HttpMessageBuilder {
    /**
     * Origin remote address.
     *
     * @var string
     */
    public string $remoteAddress = '::' {
        get => $this->remoteAddress;
        set(string $value) {
            if(filter_var($value, FILTER_VALIDATE_IP) === false)
                throw new InvalidArgumentException('$remoteAddress must be a valid remote address');

            $this->remoteAddress = $value;
        }
    }

    /**
     * Whether the request was made over HTTPS or not.
     *
     * @var bool
     */
    public bool $secure = false;

    /**
     * Origin ISO 3166 country code.
     *
     * @var string
     */
    public string $countryCode = 'XX' {
        get => $this->countryCode;
        set(string $value) {
            if(strlen($value) !== 2)
                throw new InvalidArgumentException('$countryCode must be two characters');
            if(!ctype_alnum($value))
                throw new InvalidArgumentException('$countryCode must be alphanumeric characters');

            $this->countryCode = strtoupper($value);
        }
    }

    /**
     * HTTP request method.
     *
     * @var string
     */
    public string $method = 'GET';

    /**
     * HTTP request path.
     *
     * @var string
     */
    public string $path = '/';

    /**
     * HTTP request query params.
     *
     * @var array<string, mixed>
     */
    public array $params = [];

    /**
     * Sets a HTTP request query param.
     *
     * @param string $name Name of the query field.
     * @param mixed $value Value of the query field.
     */
    public function setParam(string $name, mixed $value): void {
        $this->params[$name] = $value;
    }

    /**
     * Removes a HTTP request query param.
     *
     * @param string $name Name of the query field.
     */
    public function removeParam(string $name): void {
        unset($this->params[$name]);
    }

    /**
     * HTTP request cookies.
     *
     * @var array<string, string>
     */
    public array $cookies = [];

    /**
     * Sets a HTTP request cookie.
     *
     * @param string $name Name of the cookie.
     * @param string $value Value of the cookie.
     */
    public function setCookie(string $name, string $value): void {
        $this->cookies[$name] = $value;
    }

    /**
     * Removes a HTTP request cookie.
     *
     * @param string $name Name of the cookie.
     */
    public function removeCookie(string $name): void {
        unset($this->cookies[$name]);
    }

    /**
     * Creates a HttpRequest instance from this builder.
     *
     * @return HttpRequest An instance representing this builder.
     */
    public function toRequest(): HttpRequest {
        return new HttpRequest(
            $this->remoteAddress, $this->secure, $this->countryCode,
            $this->version, $this->method, $this->path,
            $this->params, $this->cookies,
            $this->headers->toHeaders(),
            // this is crunchy, this entire pipeline needs to be redone anyway
            $this->content instanceof HttpContent ? $this->content : null
        );
    }
}

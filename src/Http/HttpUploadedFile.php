<?php
// HttpUploadedFile.php
// Created: 2022-02-10
// Updated: 2025-02-27

namespace Index\Http;

use InvalidArgumentException;
use RuntimeException;
use Index\MediaType;

/**
 * Represents an uploaded file in a multipart/form-data request.
 */
class HttpUploadedFile {
    /**
     * @param int $errorCode PHP file upload error code.
     * @param int $size Size, as provided by the client.
     * @param string $localFileName Filename generated by PHP on the server.
     * @param string $suggestedFileName Filename included by the client.
     * @param MediaType|string $suggestedMediaType Mediatype included by the client.
     */
    public function __construct(
        public private(set) int $errorCode,
        public private(set) int $size,
        public private(set) string $localFileName,
        public private(set) string $suggestedFileName,
        MediaType|string $suggestedMediaType
    ) {
        $this->suggestedMediaType = $suggestedMediaType instanceof MediaType
            ? $suggestedMediaType
            : MediaType::parse($suggestedMediaType);
    }

    /**
     * Media type of the uploaded file.
     *
     * @var ?MediaType
     */
    public ?MediaType $localMediaType {
        get => MediaType::fromPath($this->localFileName);
    }

    /**
     * Suggested media type for the uploaded file.
     *
     * @var MediaType
     */
    public private(set) MediaType $suggestedMediaType;

    /**
     * Whether the file has been moved to its final destination.
     *
     * @var bool
     */
    public private(set) bool $hasMoved = false;

    /**
     * Moves the uploaded file to its final destination.
     *
     * @param string $path Path to move the file to.
     * @throws RuntimeException If the file has already been moved.
     * @throws RuntimeException If an upload error occurred.
     * @throws InvalidArgumentException If the provided $path is not valid.
     * @throws RuntimeException If the file failed to move.
     */
    public function moveTo(string $path): void {
        if($this->hasMoved)
            throw new RuntimeException('This uploaded file has already been moved.');
        if($this->errorCode !== UPLOAD_ERR_OK)
            throw new RuntimeException('Can\'t move file because of an upload error.');

        if(empty($path))
            throw new InvalidArgumentException('$path is not a valid path.');

        $this->hasMoved = PHP_SAPI === 'CLI'
            ? rename($this->localFileName, $path)
            : move_uploaded_file($this->localFileName, $path);

        if(!$this->hasMoved)
            throw new RuntimeException('Failed to move file to ' . $path);

        $this->localFileName = $path;
    }

    /**
     * Creates a HttpUploadedFile instance from an entry in the $_FILES superglobal.
     *
     * @param array<string, int|string> $file File info array.
     * @return HttpUploadedFile Uploaded file info.
     */
    public static function createFromPhpFilesEntry(array $file): self {
        return new HttpUploadedFile(
            (int)($file['error'] ?? UPLOAD_ERR_NO_FILE),
            (int)($file['size'] ?? -1),
            (string)($file['tmp_name'] ?? ''),
            (string)($file['name'] ?? ''),
            (string)($file['type'] ?? '')
        );
    }

    /**
     * Creates a collection of HttpUploadedFile instances from the $_FILES superglobal.
     *
     * @param array<string, mixed> $files Value of a $_FILES superglobal.
     * @return array<string, HttpUploadedFile|array<string, mixed>> Uploaded files.
     */
    public static function createFromPhpFiles(array $files): array {
        if(empty($files))
            return [];

        return self::createObjectInstances(self::normalizePhpFiles($files));
    }

    /**
     * @param array<string, mixed> $files
     * @return array<string, mixed>
     */
    private static function traversePhpFiles(array $files, string $keyName): array {
        $arr = [];

        foreach($files as $key => $val) {
            $key = "_{$key}";

            if(is_array($val)) {
                /** @var array<string, mixed> $val */
                $arr[$key] = self::traversePhpFiles($val, $keyName);
            } else {
                $arr[$key][$keyName] = $val;
            }
        }

        return $arr;
    }

    /**
     * @param array<string, mixed> $files
     * @return array<string, mixed>
     */
    private static function normalizePhpFiles(array $files): array {
        $out = [];

        foreach($files as $key => $arr) {
            if(!is_array($arr) || !isset($arr['error']))
                continue;

            $key = '_' . $key;

            if(is_int($arr['error'])) {
                $out[$key] = $arr;
                continue;
            }

            if(is_array($arr['error'])) {
                $keys = array_keys($arr);

                foreach($keys as $keyName) {
                    $source = $arr[$keyName];
                    if(!is_array($source))
                        continue;

                    /** @var array<string, mixed> $mergeWith */
                    $mergeWith = $out[$key] ?? [];

                    /** @var array<string, mixed> $source */
                    $out[$key] = array_merge_recursive($mergeWith, self::traversePhpFiles($source, (string)$keyName));
                }
                continue;
            }
        }

        return $out;
    }

    /**
     * @param array<string, mixed> $files
     * @return array<string, HttpUploadedFile|array<string, mixed>>
     */
    private static function createObjectInstances(array $files): array {
        $coll = [];

        foreach($files as $key => $val) {
            if(!is_array($val))
                continue;

            $key = substr($key, 1);

            if(isset($val['error']))
                /** @var array<string, int|string> $val */
                $coll[$key] = self::createFromPhpFilesEntry($val);
            else
                /** @var array<string, mixed> $val */
                $coll[$key] = self::createObjectInstances($val);
        }

        return $coll;
    }
}

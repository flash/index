<?php
// HttpContentHandler.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Http;

/**
 * Represents a content handler for building HTTP response messages.
 */
interface HttpContentHandler {
    /**
     * Determines whether this handler is suitable for the body content.
     *
     * @param mixed $content Content to be judged.
     * @return bool true if suitable, false if not.
     */
    public function match(mixed $content): bool;

    /**
     * Handles body content.
     *
     * @param HttpResponseBuilder $response Response to apply this body to.
     * @param mixed $content Body to apply to the response message.
     */
    public function handle(HttpResponseBuilder $response, mixed $content): void;
}

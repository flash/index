<?php
// BencodeSerializable.php
// Created: 2022-01-13
// Updated: 2025-01-18

namespace Index\Bencode;

/**
 * Provides an interface for serialising objects for bencoding in a controlled manner.
 */
interface BencodeSerializable {
    /**
     * Gets the data that should represent this object in a Bencode structure.
     *
     * @return mixed Representative data.
     */
    public function bencodeSerialize(): mixed;
}

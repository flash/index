<?php
// BencodeSerializableCommon.php
// Created: 2024-09-29
// Updated: 2025-01-18

namespace Index\Bencode;

use ReflectionMethod;
use ReflectionObject;
use ReflectionProperty;
use RuntimeException;

/**
 * Implements support for the BencodeProperty attribute.
 */
trait BencodeSerializableCommon {
    /**
     * Constructs Bencode object based on BencodeProperty attributes.
     *
     * @return array<string, mixed>
     */
    public function bencodeSerialize(): mixed {
        $objectInfo = new ReflectionObject($this);
        $props = [];

        $propInfos = $objectInfo->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach($propInfos as $propInfo) {
            $attrInfos = $propInfo->getAttributes(BencodeProperty::class);
            if(count($attrInfos) < 1)
                continue;
            if(count($attrInfos) > 1)
                throw new RuntimeException('Properties may only carry a single instance of the BencodeProperty attribute.');

            $info = $attrInfos[0]->newInstance();

            $name = $info->name ?? $propInfo->getName();
            if(array_key_exists($name, $props))
                throw new RuntimeException('A property with that name has already been defined.');

            $value = $propInfo->getValue($propInfo->isStatic() ? null : $this);
            if($info->shouldBeOmitted($value))
                continue;

            if(is_float($value) || (is_int($value) && $info->numbersAsString))
                $value = (string)$value;
            elseif(is_bool($value))
                $value = $value ? 1 : 0;

            $props[$name] = [
                'name' => $name,
                'value' => $value,
                'order' => $info->order,
            ];
        }

        $methodInfos = $objectInfo->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach($methodInfos as $methodInfo) {
            $attrInfos = $methodInfo->getAttributes(BencodeProperty::class);
            if(count($attrInfos) < 1)
                continue;
            if(count($attrInfos) > 1)
                throw new RuntimeException('Methods may only carry a single instance of the BencodeProperty attribute.');

            if($methodInfo->getNumberOfRequiredParameters() > 0)
                throw new RuntimeException('Methods marked with the BencodeProperty attribute must not have any required arguments.');

            $info = $attrInfos[0]->newInstance();

            $name = $info->name ?? $methodInfo->getName();
            if(array_key_exists($name, $props))
                throw new RuntimeException('A property with that name has already been defined.');

            $value = $methodInfo->invoke($methodInfo->isStatic() ? null : $this);
            if($info->shouldBeOmitted($value))
                continue;

            if(is_float($value) || (is_int($value) && $info->numbersAsString))
                $value = (string)$value;
            elseif(is_bool($value))
                $value = $value ? 1 : 0;

            $props[$name] = [
                'name' => $name,
                'value' => $value,
                'order' => $info->order,
            ];
        }

        usort($props, fn($a, $b) => $a['order'] <=> $b['order']);

        return array_column($props, 'value', 'name');
    }
}

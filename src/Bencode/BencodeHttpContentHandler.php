<?php
// BencodeHttpContentHandler.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Bencode;

use Index\Http\{HttpContentHandler,HttpResponseBuilder};

/**
 * Represents a Bencode content handler for building HTTP response messages.
 */
class BencodeHttpContentHandler implements HttpContentHandler {
    public function match(mixed $content): bool {
        return $content instanceof BencodeSerializable;
    }

    public function handle(HttpResponseBuilder $response, mixed $content): void {
        if(!$response->hasContentType())
            $response->setTypePlain();

        $response->content = new BencodeHttpContent($content);
    }
}

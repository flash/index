<?php
// BencodeHttpContent.php
// Created: 2022-02-10
// Updated: 2025-01-18

namespace Index\Bencode;

use RuntimeException;
use Index\Http\HttpContent;

/**
 * Represents Bencoded body content for a HTTP message.
 */
class BencodeHttpContent implements BencodeSerializable, HttpContent {
    /**
     * @param mixed $content Content to be bencoded.
     */
    public function __construct(
        public private(set) mixed $content
    ) {}

    public function bencodeSerialize(): mixed {
        return $this->content;
    }

    /**
     * Encodes the content.
     *
     * @return string Bencoded string.
     */
    public function encode(): string {
        return Bencode::encode($this->content);
    }

    public function __toString(): string {
        return $this->encode();
    }

    /**
     * Creates an instance from encoded content.
     *
     * @param mixed $encoded Bencoded content.
     * @return BencodeHttpContent Instance representing the provided content.
     */
    public static function fromEncoded(mixed $encoded): BencodeHttpContent {
        return new BencodeHttpContent(Bencode::decode($encoded));
    }

    /**
     * Creates an instance from an encoded file.
     *
     * @param string $path Path to the bencoded file.
     * @return BencodeHttpContent Instance representing the provided path.
     */
    public static function fromFile(string $path): BencodeHttpContent {
        $handle = fopen($path, 'rb');
        if(!is_resource($handle))
            throw new RuntimeException('$path could not be opened');

        try {
            return self::fromEncoded($handle);
        } finally {
            fclose($handle);
        }
    }

    /**
     * Creates an instance from the raw request body.
     *
     * @return BencodeHttpContent Instance representing the request body.
     */
    public static function fromRequest(): BencodeHttpContent {
        return self::fromFile('php://input');
    }
}

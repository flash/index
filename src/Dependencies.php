<?php
// Dependencies.php
// Created: 2025-01-18
// Updated: 2025-01-22

namespace Index;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionFunctionAbstract;
use ReflectionNamedType;
use ReflectionParameter;
use RuntimeException;

class Dependencies {
    /** @var array<class-string, object[]> */
    private array $objects = [];

    /** @var class-string[] */
    private array $resolving = [];

    /**
     * @param class-string $name
     */
    private function registerObject(string $name, object $object): void {
        if(array_key_exists($name, $this->objects))
            $this->objects[$name][] = $object;
        else
            $this->objects[$name] = [$object];
    }

    /**
     * @param array<string|int, mixed> $args
     * @return array<string|int, mixed>
     */
    private function resolveArgs(ReflectionFunctionAbstract $constructor, array $args): array {
        if($constructor->getNumberOfRequiredParameters() > 0 && (empty($args) || !array_is_list($args))) {
            $params = XArray::where(
                $constructor->getParameters(),
                fn(ReflectionParameter $param) => !$param->isOptional() && $param->hasType() && !array_key_exists($param->getName(), $args)
            );
            foreach($params as $paramInfo) {
                $typeInfo = $paramInfo->getType();
                $allowsNull = $typeInfo === null || $typeInfo->allowsNull();
                $value = null;

                // maybe support intersection and union someday
                if($typeInfo instanceof ReflectionNamedType && !$typeInfo->isBuiltin()) {
                    $className = $typeInfo->getName();
                    if(!class_exists($className) && !interface_exists($className))
                        throw new RuntimeException(sprintf('class in type does not exist: %s', $className));

                    $value = $this->resolve($className);
                    if($value === null && !$allowsNull)
                        throw new RuntimeException(sprintf('was not able to resolve %s', $className));
                }

                if($value === null && !$allowsNull)
                    throw new RuntimeException('required parameter has unsupported type definition');
                $args[$paramInfo->getName()] = $value;
            }
        }

        return $args;
    }

    /**
     * Constructs an object using dependencies.
     *
     * @template T of object
     * @param class-string<T> $class Class string.
     * @param mixed ...$args Arguments to be passed to the constructor.
     * @return T
     */
    public function construct(string $class, mixed ...$args): object {
        $objInfo = new ReflectionClass($class);
        if(!$objInfo->isInstantiable())
            throw new InvalidArgumentException('$class must be instantiable');

        $constructor = $objInfo->getMethod('__construct');
        if(!$constructor->isPublic())
            throw new RuntimeException(sprintf('constructor of %s is not public', $objInfo->getName()));
        if(!$constructor->isConstructor())
            throw new RuntimeException(sprintf('__construct on %s is not not a constructor somehow', $objInfo->getName()));

        return $objInfo->newInstanceArgs($this->resolveArgs($constructor, $args));
    }

    /**
     * Creates an object using dependencies and delays construction.
     *
     * @template T of object
     * @param class-string<T> $class Class string.
     * @param mixed ...$args Arguments to be passed to the constructor.
     * @return T
     */
    public function constructLazy(string $class, mixed ...$args): object {
        $objInfo = new ReflectionClass($class);
        if(!$objInfo->isInstantiable())
            throw new InvalidArgumentException('$class must be instantiable');

        return $objInfo->newLazyGhost(function(object $object) use ($objInfo, $args) {
            try {
                $constructor = $objInfo->getMethod('__construct');
            } catch(ReflectionException $ex) {
                return;
            }

            if(!$constructor->isPublic())
                throw new RuntimeException(sprintf('constructor of %s is not public', $objInfo->getName()));
            if(!$constructor->isConstructor())
                throw new RuntimeException(sprintf('__construct on %s is not not a constructor somehow', $objInfo->getName()));

            $constructor->invokeArgs($object, $this->resolveArgs($constructor, $args));
        });
    }

    /**
     * Registers a dependency class. Use a class string and $args to create lazy object.
     *
     * @template T of object
     * @param class-string<T>|T $objectOrClass Class string or object instance.
     * @param mixed ...$args Arguments to be passed to the constructor. Dependency resolving will only be done if $args is empty or associative.
     * @throws InvalidArgumentException If $objectOrClass is not an expected value, e.g. not instantiable.
     * @throws InvalidArgumentException If $args is not empty but $objectOrClass is an instance and not a class string.
     * @throws RuntimeException If constructor is defined but not public, or some other critical issue with it.
     */
    public function register(string|object $objectOrClass, mixed ...$args): void {
        $objInfo = new ReflectionClass($objectOrClass);
        if(!$objInfo->isInstantiable())
            throw new InvalidArgumentException('$objectOrClass must be instantiable');

        if(is_string($objectOrClass)) {
            $object = $objInfo->newLazyGhost(function(object $object) use ($objInfo, $args) {
                try {
                    $constructor = $objInfo->getMethod('__construct');
                } catch(ReflectionException $ex) {
                    return;
                }

                if(!$constructor->isPublic())
                    throw new RuntimeException(sprintf('constructor of %s is not public', $objInfo->getName()));
                if(!$constructor->isConstructor())
                    throw new RuntimeException(sprintf('__construct on %s is not not a constructor somehow', $objInfo->getName()));

                $constructor->invokeArgs($object, $this->resolveArgs($constructor, $args));
            });
        } else {
            if(!empty($args))
                throw new InvalidArgumentException('$args must be empty if $object is an instance already');

            $object = $objectOrClass;
        }

        $parent = $objInfo;
        while($parent !== false) {
            $this->registerObject($objInfo->getName(), $object);
            $parent = $parent->getParentClass();
        }

        $names = $objInfo->getInterfaceNames();
        foreach($names as $name)
            $this->registerObject($name, $object);
    }

    /**
     * Attempts to resolve an object from registered types.
     *
     * @template T of object
     * @param class-string<T> $class Class or interface name of an object to resolve.
     * @param (callable(T): bool)|null $predicate Filter objects more specifically if not null.
     * @throws RuntimeException If a circular dependency has been caught.
     * @return ?T Instance of an object if a match was found, null if not.
     */
    public function resolve(string $class, ?callable $predicate = null): ?object {
        if(in_array($class, $this->resolving))
            throw new RuntimeException(sprintf('circular dependency caught, %s was requested while it itself was being resolved', $class));

        try {
            array_push($this->resolving, $class);
            if(!array_key_exists($class, $this->objects))
                return null;

            $object = XArray::first($this->objects[$class], $predicate); // @phpstan-ignore-line: IDC
            if($object === null || !is_a($object, $class))
                return null;

            return $object;
        } finally {
            array_pop($this->resolving);
        }
    }

    /**
     * Attempts to resolve an object from registered types and throws if its not found.
     *
     * @template T of object
     * @param class-string<T> $class Class or interface name of an object to resolve.
     * @param (callable(T): bool)|null $predicate Filter objects more specifically if not null.
     * @throws RuntimeException If a circular dependency has been caught.
     * @throws RuntimeException If the dependency could not be resolved.
     * @return T Instance of an object if a match was found.
     */
    public function resolveThrow(string $class, ?callable $predicate = null): object {
        $instance = $this->resolve($class, $predicate);
        if($instance === null)
            throw new RuntimeException(sprintf('was not able to resolve %s', $class));

        return $instance;
    }

    /**
     * Retrieves all registered objects that implement a given type.
     *
     * @template T of object
     * @param class-string<T> $class Class or interface name of objects to resolve.
     * @param (callable(T): bool)|null $predicate Filter objects more specifically if not null.
     * @return T[] All registered instances of a given type.
     */
    public function all(string $class, ?callable $predicate = null): array {
        if(!array_key_exists($class, $this->objects))
            return [];

        if($predicate === null)
            return $this->objects[$class]; // @phpstan-ignore-line: trust me

        return XArray::where($this->objects[$class], $predicate); // @phpstan-ignore-line: this is fine
    }
}

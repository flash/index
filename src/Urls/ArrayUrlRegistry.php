<?php
// ArrayUrlRegistry.php
// Created: 2024-10-03
// Updated: 2024-10-04

namespace Index\Urls;

use InvalidArgumentException;
use Stringable;

/**
 * Provides an array backed URL registry implementation.
 */
class ArrayUrlRegistry implements UrlRegistry {
    /** @var array<string, array{path: string, query: array<string, string>, fragment: string}> */
    private array $urls = [];

    public function register(UrlSource|string $nameOrSource, string $path = '', array $query = [], string $fragment = ''): void {
        if($nameOrSource instanceof UrlSource) {
            $nameOrSource->registerUrls($this);
            return;
        }

        if($nameOrSource === '')
            throw new InvalidArgumentException('$nameOrSource may not be empty');
        if(array_key_exists($nameOrSource, $this->urls))
            throw new InvalidArgumentException('A URL format with $nameOrSource has already been registered');
        if($path === '')
            throw new InvalidArgumentException('$path may not be empty');
        if(!str_starts_with($path, '/'))
            throw new InvalidArgumentException('$path must begin with /');

        $this->urls[$nameOrSource] = [
            'path' => $path,
            'query' => $query,
            'fragment' => $fragment,
        ];
    }

    public function format(string $name, array $vars = [], bool $spacesAsPlus = false): string {
        if(!array_key_exists($name, $this->urls))
            return '';

        $format = $this->urls[$name];
        $string = self::replaceVariables($format['path'], $vars);

        if(!empty($format['query'])) {
            $query = [];
            foreach($format['query'] as $name => $value) {
                $value = self::replaceVariables($value, $vars);
                if($value !== '' && !($name === 'page' && (int)$value <= 1))
                    $query[$name] = $value;
            }

            if(!empty($query))
                $string .= '?' . http_build_query($query, '', '&', $spacesAsPlus ? PHP_QUERY_RFC1738 : PHP_QUERY_RFC3986);
        }

        if(!empty($format['fragment'])) {
            $fragment = self::replaceVariables($format['fragment'], $vars);
            if($fragment !== '')
                $string .= '#' . $fragment;
        }

        return $string;
    }

    /**
     * Formats a component template.
     *
     * @param string $str Template string.
     * @param array<string, mixed> $vars Replacement variables.
     * @return string Formatted string.
     */
    private static function replaceVariables(string $str, array $vars): string {
        // i: have no idea what i'm doing
        // i: still don't i literally copy pasted this from Misuzu lmao
        $out = '';
        $varName = '';
        $inVarName = false;

        $length = strlen($str);
        for($i = 0; $i < $length; ++$i) {
            $char = $str[$i];

            if($inVarName) {
                if($char === '>') {
                    $inVarName = false;
                    if(array_key_exists($varName, $vars)) {
                        $varValue = $vars[$varName];
                        if(is_array($varValue))
                            $varValue = empty($varValue) ? '' : implode(',', $varValue);
                        elseif(is_int($varValue))
                            $varValue = ($varName === 'page' ? $varValue < 2 : $varValue === 0) ? '' : (string)$varValue;
                        elseif(is_scalar($varValue) || $varValue instanceof Stringable)
                            $varValue = (string)$varValue;
                        else
                            $varValue = '';
                    } else
                        $varValue = '';

                    $out .= $varValue;
                    $varName = '';
                    continue;
                }

                if($char === '<') {
                    $out .= '<' . $varName;
                    $varName = '';
                    continue;
                }

                $varName .= $char;
            } else
                $inVarName = $char === '<';

            if(!$inVarName)
                $out .= $char;
        }

        if($varName !== '')
            $out .= $varName;

        return $out;
    }

    public function scopeTo(string $namePrefix = '', string $pathPrefix = ''): UrlRegistry {
        return new ScopedUrlRegistry($this, $namePrefix, $pathPrefix);
    }
}

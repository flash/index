<?php
// ScopedUrlRegistry.php
// Created: 2024-10-03
// Updated: 2024-10-04

namespace Index\Urls;

use InvalidArgumentException;

/**
 * Provides a scoped URL registry implementation.
 */
class ScopedUrlRegistry implements UrlRegistry {
    /**
     * @param UrlRegistry $registry Base URL registry, preferably not a scoped one to avoid overhead.
     * @param string $namePrefix String to prefix names in the scope with.
     * @param string $pathPrefix String to prefix paths in the scope with, must start with / if not empty.
     * @throws InvalidArgumentException If $pathPrefix is non-empty but doesn't start with a /.
     */
    public function __construct(
        private UrlRegistry $registry,
        private string $namePrefix,
        private string $pathPrefix
    ) {
        if($pathPrefix !== '' && !str_starts_with($pathPrefix, '/'))
            throw new InvalidArgumentException('$pathPrefix must start with a / if it is non-empty');
    }

    public function register(UrlSource|string $nameOrSource, string $path = '', array $query = [], string $fragment = ''): void {
        if($nameOrSource instanceof UrlSource) {
            $nameOrSource->registerUrls($this);
            return;
        }

        if($nameOrSource === '')
            throw new InvalidArgumentException('$nameOrSource may not be empty');
        if($path === '')
            throw new InvalidArgumentException('$path may not be empty');
        if(!str_starts_with($path, '/'))
            throw new InvalidArgumentException('$path must begin with /');

        $this->registry->register(
            $this->namePrefix . $nameOrSource,
            $this->pathPrefix . $path,
            $query,
            $fragment
        );
    }

    public function format(string $name, array $vars = [], bool $spacesAsPlus = false): string {
        return $this->registry->format(
            $this->namePrefix . $name,
            $vars,
            $spacesAsPlus
        );
    }

    public function scopeTo(string $namePrefix = '', string $pathPrefix = ''): UrlRegistry {
        return $this->registry->scopeTo(
            $this->namePrefix . $namePrefix,
            $this->pathPrefix . $pathPrefix
        );
    }
}

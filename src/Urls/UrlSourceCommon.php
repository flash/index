<?php
// UrlSourceCommon.php
// Created: 2024-10-04
// Updated: 2025-01-18

namespace Index\Urls;

/**
 * Provides an implementation of UrlSource::registerUrls that uses the attributes.
 * For more advanced use, everything can be use'd separately and UrlFormat::register called manually.
 */
trait UrlSourceCommon {
    public function registerUrls(UrlRegistry $registry): void {
        UrlFormat::register($registry, $this);
    }
}

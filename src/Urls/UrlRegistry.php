<?php
// UrlRegistry.php
// Created: 2024-10-03
// Updated: 2025-01-18

namespace Index\Urls;

use InvalidArgumentException;

/**
 * Provides an interface for defining a URL registry.
 */
interface UrlRegistry {
    /**
     * Registers a URL format.
     *
     * TODO: explain formatting system
     *
     * @param UrlSource|string $nameOrSource Name of the format or a URL source object.
     * @param string $path Path portion of the URL. Required if $nameOrSource is a name string.
     * @param array<string, string> $query Query string variables of the URL.
     * @param string $fragment Fragment portion of the URL.
     * @throws InvalidArgumentException If $name has already been registered.
     * @throws InvalidArgumentException If $path if empty or does not start with a /.
     */
    public function register(UrlSource|string $nameOrSource, string $path = '', array $query = [], string $fragment = ''): void;

    /**
     * Format a URL.
     *
     * @param string $name Name of the URL to format.
     * @param array<string, mixed> $vars Values to replace the variables in the path, query and fragment with.
     * @param bool $spacesAsPlus Whether to represent spaces as a + instead of %20.
     * @return string Formatted URL.
     */
    public function format(string $name, array $vars = [], bool $spacesAsPlus = false): string;

    /**
     * Creates a scoped URL registry.
     *
     * @param string $namePrefix String to prefix names in the scope with.
     * @param string $pathPrefix String to prefix paths in the scope with, must start with / if not empty.
     * @throws InvalidArgumentException If $pathPrefix is non-empty but doesn't start with a /.
     * @return UrlRegistry A scoped URL registry.
     */
    public function scopeTo(string $namePrefix = '', string $pathPrefix = ''): UrlRegistry;
}

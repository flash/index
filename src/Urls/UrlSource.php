<?php
// UrlSource.php
// Created: 2024-10-03
// Updated: 2025-01-18

namespace Index\Urls;

interface UrlSource {
    public function registerUrls(UrlRegistry $registry): void;
}

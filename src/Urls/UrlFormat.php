<?php
// UrlFormat.php
// Created: 2024-10-03
// Updated: 2025-01-18

namespace Index\Urls;

use Attribute;
use ReflectionAttribute;
use ReflectionObject;

/**
 * An attribute for denoting URL formatting for a route handler.
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class UrlFormat {
    /**
     * @param string $name Name of the URL format.
     * @param string $path Path component template of the URL.
     * @param array<string, string> $query Query string component template for the URL.
     * @param string $fragment Fragment component template for the URL.
     */
    public function __construct(
        public private(set) string $name,
        public private(set) string $path,
        public private(set) array $query = [],
        public private(set) string $fragment = ''
    ) {}

    /**
     * Reads attributes from methods in a UrlSource instance and registers them to a given UrlRegistry instance.
     *
     * @param UrlRegistry $registry
     * @param UrlSource $source
     */
    public static function register(UrlRegistry $registry, UrlSource $source): void {
        $objectInfo = new ReflectionObject($source);
        $methodInfos = $objectInfo->getMethods();

        foreach($methodInfos as $methodInfo) {
            $attrInfos = $methodInfo->getAttributes(UrlFormat::class, ReflectionAttribute::IS_INSTANCEOF);
            foreach($attrInfos as $attrInfo) {
                $formatInfo = $attrInfo->newInstance();
                $registry->register(
                    $formatInfo->name,
                    $formatInfo->path,
                    $formatInfo->query,
                    $formatInfo->fragment,
                );
            }
        }
    }
}

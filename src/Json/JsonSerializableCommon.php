<?php
// JsonSerializableCommon.php
// Created: 2024-09-29
// Updated: 2025-01-18

namespace Index\Json;

use ReflectionMethod;
use ReflectionObject;
use ReflectionProperty;
use RuntimeException;

/**
 * Implements support for the JsonProperty attribute.
 */
trait JsonSerializableCommon {
    /**
     * Constructs JSON object based on JsonProperty attributes.
     *
     * @return array<string, mixed>
     */
    public function jsonSerialize(): mixed {
        $objectInfo = new ReflectionObject($this);
        $props = [];

        $propInfos = $objectInfo->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach($propInfos as $propInfo) {
            $attrInfos = $propInfo->getAttributes(JsonProperty::class);
            if(count($attrInfos) < 1)
                continue;
            if(count($attrInfos) > 1)
                throw new RuntimeException('Properties may only carry a single instance of the JsonProperty attribute.');

            $info = $attrInfos[0]->newInstance();

            $name = $info->name ?? $propInfo->getName();
            if(array_key_exists($name, $props))
                throw new RuntimeException('A property with that name has already been defined.');

            $value = $propInfo->getValue($propInfo->isStatic() ? null : $this);
            if($info->shouldBeOmitted($value))
                continue;

            if((is_int($value) || is_float($value)) && $info->numbersAsString)
                $value = (string)$value;

            $props[$name] = [
                'name' => $name,
                'value' => $value,
                'order' => $info->order,
            ];
        }

        $methodInfos = $objectInfo->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach($methodInfos as $methodInfo) {
            $attrInfos = $methodInfo->getAttributes(JsonProperty::class);
            if(count($attrInfos) < 1)
                continue;
            if(count($attrInfos) > 1)
                throw new RuntimeException('Methods may only carry a single instance of the JsonProperty attribute.');

            if($methodInfo->getNumberOfRequiredParameters() > 0)
                throw new RuntimeException('Methods marked with the JsonProperty attribute must not have any required arguments.');

            $info = $attrInfos[0]->newInstance();

            $name = $info->name ?? $methodInfo->getName();
            if(array_key_exists($name, $props))
                throw new RuntimeException('A property with that name has already been defined.');

            $value = $methodInfo->invoke($methodInfo->isStatic() ? null : $this);
            if($info->shouldBeOmitted($value))
                continue;

            if((is_int($value) || is_float($value)) && $info->numbersAsString)
                $value = (string)$value;

            $props[$name] = [
                'name' => $name,
                'value' => $value,
                'order' => $info->order,
            ];
        }

        usort($props, fn($a, $b) => $a['order'] <=> $b['order']);

        return array_column($props, 'value', 'name');
    }
}

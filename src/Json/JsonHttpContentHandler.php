<?php
// JsonHttpContentHandler.php
// Created: 2024-03-28
// Updated: 2025-01-18

namespace Index\Json;

use stdClass;
use JsonSerializable;
use Index\Http\{HttpContentHandler,HttpResponseBuilder};

/**
 * Represents a JSON content handler for building HTTP response messages.
 */
class JsonHttpContentHandler implements HttpContentHandler {
    public function match(mixed $content): bool {
        return is_array($content) || $content instanceof JsonSerializable || $content instanceof stdClass;
    }

    public function handle(HttpResponseBuilder $response, mixed $content): void {
        if(!$response->hasContentType())
            $response->setTypeJson();

        $response->content = new JsonHttpContent($content);
    }
}

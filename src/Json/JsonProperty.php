<?php
// JsonProperty.php
// Created: 2024-09-29
// Updated: 2025-01-18

namespace Index\Json;

use Attribute;
/**
 * Defines a class method or property as a JSON object property.
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_PROPERTY)]
final class JsonProperty {
    /**
     * @param string $name Name of the property in the JSON object.
     * @param int $order Ordering of the property in the JSON object.
     * @param bool $omitIfNull Whether the value should be omitted if its null.
     * @param mixed|mixed[] $omitIfValue Which values should be omitted.
     * @param bool $numbersAsString Whether numbers should be cast to strings.
     */
    public function __construct(
        public private(set) ?string $name = null,
        public private(set) int $order = 0,
        public private(set) bool $omitIfNull = true,
        mixed $omitIfValue = [],
        public private(set) bool $numbersAsString = false
    ) {
        $this->omitIfValue = is_array($omitIfValue) ? $omitIfValue : [$omitIfValue];
    }

    /**
     * Which values should be omitted.
     *
     * @var mixed[]
     */
    public array $omitIfValue;

    /**
     * Checks whether a given value should be omitted from the JSON object.
     *
     * @param mixed $value Value to check.
     * @return bool
     */
    public function shouldBeOmitted(mixed $value): bool {
        return ($this->omitIfNull && $value === null)
            || in_array($value, $this->omitIfValue);
    }
}

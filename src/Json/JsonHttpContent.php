<?php
// JsonHttpContent.php
// Created: 2022-02-10
// Updated: 2025-01-18

namespace Index\Json;

use JsonSerializable;
use RuntimeException;
use Index\Http\HttpContent;

/**
 * Represents JSON body content for a HTTP message.
 */
class JsonHttpContent implements HttpContent, JsonSerializable {
    /**
     * @param mixed $content Content to be JSON encoded.
     */
    public function __construct(
        public private(set) mixed $content
    ) {}

    public function jsonSerialize(): mixed {
        return $this->content;
    }

    /**
     * Encodes the content.
     *
     * @return string JSON encoded string.
     */
    public function encode(): string {
        $encoded = json_encode($this->content);
        if($encoded === false)
            return '';

        return $encoded;
    }

    public function __toString(): string {
        return $this->encode();
    }

    /**
     * Creates an instance from encoded content.
     *
     * @param string $encoded JSON encoded content.
     * @return JsonHttpContent Instance representing the provided content.
     */
    public static function fromEncoded(string $encoded): JsonHttpContent {
        return new JsonHttpContent(json_decode($encoded));
    }

    /**
     * Creates an instance from an encoded file.
     *
     * @param string $path Path to the JSON encoded file.
     * @return JsonHttpContent Instance representing the provided path.
     */
    public static function fromFile(string $path): JsonHttpContent {
        $contents = file_get_contents($path);
        if($contents === false)
            throw new RuntimeException('was unable to read file at $path');

        return self::fromEncoded($contents);
    }

    /**
     * Creates an instance from the raw request body.
     *
     * @return JsonHttpContent Instance representing the request body.
     */
    public static function fromRequest(): JsonHttpContent {
        return self::fromFile('php://input');
    }
}

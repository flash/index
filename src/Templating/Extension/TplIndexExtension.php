<?php
// TplIndexExtension.php
// Created: 2024-08-04
// Updated: 2024-10-04

namespace Index\Templating\Extension;

use Index\{ByteFormat,Index};
use Twig\{Environment,TwigFilter,TwigFunction};
use Twig\Extension\AbstractExtension;

/**
 * Provides version functions and additional functionality implemented in Index.
 */
class TplIndexExtension extends AbstractExtension {
    public function getFilters() {
        return [
            new TwigFilter('format_filesize', ByteFormat::format(...)),
        ];
    }

    public function getFunctions() {
        return [
            new TwigFunction('ndx_version', Index::version(...)),
            new TwigFunction('twig_version', fn() => Environment::VERSION),
        ];
    }
}

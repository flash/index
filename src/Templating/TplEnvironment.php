<?php
// TplEnvironment.php
// Created: 2023-08-24
// Updated: 2025-02-27

namespace Index\Templating;

use InvalidArgumentException;
use UnexpectedValueException;
use Index\Templating\Cache\TplFilesystemCache;
use Index\Templating\Extension\TplIndexExtension;
use Index\Templating\Loader\TplFilesystemLoader;
use Twig\{Environment,TwigFilter,TwigFunction,TwigTest};
use Twig\Cache\CacheInterface;
use Twig\Extension\ExtensionInterface;
use Twig\Extra\Html\HtmlExtension;
use Twig\Loader\LoaderInterface;

/**
 * Provides a wrapper of Twig\Environment.
 */
class TplEnvironment {
    /**
     * @param LoaderInterface|string $loader A template loader instance or a path.
     * @param CacheInterface|array<string>|string|null $cache A caching driver.
     * @param string $charset Character for templates.
     * @param bool $debug Debug mode.
     */
    public function __construct(
        LoaderInterface|string $loader,
        CacheInterface|array|string|null $cache = null,
        string $charset = 'utf-8',
        bool $debug = false
    ) {
        if(is_string($loader))
            $loader = new TplFilesystemLoader($loader);

        if(is_array($cache)) {
            if(empty($cache))
                throw new InvalidArgumentException('if $cache is an array, it may not be empty');
            $cache = TplFilesystemCache::create(array_shift($cache), array_shift($cache));
        } elseif($cache === null) $cache = false;

        $this->environment = new Environment($loader, [
            'debug' => $debug,
            'cache' => $cache,
            'charset' => $charset,
            'strict_variables' => true, // there's no reason to disable this ever
        ]);

        $this->environment->addExtension(new HtmlExtension);
        $this->environment->addExtension(new TplIndexExtension);
    }

    /**
     * Reference to the underlying Twig Environment.
     * Things that aren't exposed through Index generally have a reason
     *  for being "obfuscated" but go wild if you really want to.
     *
     * @var Environment
     */
    private Environment $environment;

    /**
     * Returns if debug mode is enabled.
     *
     * @return bool
     */
    public bool $debug {
        get => $this->environment->isDebug();
    }

    /**
     * Registers an extension.
     *
     * @param ExtensionInterface $extension
     */
    public function addExtension(ExtensionInterface $extension): void {
        $this->environment->addExtension($extension);
    }

    /**
     * Registers a filter.
     *
     * @param string $name Name of the filter.
     * @param callable $body Body of the filter.
     * @param array<string, mixed> $options Options, review the TwigFilter file for the options.
     */
    public function addFilter(string $name, callable $body, array $options = []): void {
        $this->environment->addFilter(new TwigFilter($name, $body, $options));
    }

    /**
     * Registers a function.
     *
     * @param string $name Name of the function.
     * @param callable $body Body of the function.
     * @param array<string, mixed> $options Options, review the TwigFunction file for the options.
     */
    public function addFunction(string $name, callable $body, array $options = []): void {
        $this->environment->addFunction(new TwigFunction($name, $body, $options));
    }

    /**
     * Registers a twig.
     *
     * @param string $name Name of the twig.
     * @param callable $body Body of the twig.
     * @param array<string, mixed> $options Options, review the TwigTest file for the options.
     */
    public function addTest(string $name, callable $body, array $options = []): void {
        $this->environment->addTest(new TwigTest($name, $body, $options));
    }

    /**
     * Adds a global variable available in any TplContext instance.
     *
     * @param string $name Name of the variable.
     * @param mixed $value Content of the variable.
     */
    public function addGlobal(string $name, mixed $value): void {
        $this->environment->addGlobal($name, $value);
    }

    /**
     * Loads a template and creates a TplContext instance.
     *
     * @param string $name Name or path of the template.
     * @param array<string, mixed> $vars Context local variables to add right away.
     * @return TplContext
     */
    public function load(string $name, array $vars = []): TplContext {
        return new TplContext($this->environment->load($name), $vars);
    }

    /**
     * Direct proxy to Environment's render method.
     *
     * @param string $name Name or path of the template.
     * @param array<string, mixed> $vars Local variables to render the template with.
     * @return string
     */
    public function render(string $name, array $vars = []): string {
        return $this->environment->render($name, $vars);
    }
}

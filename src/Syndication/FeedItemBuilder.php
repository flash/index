<?php
// FeedItemBuilder.php
// Created: 2024-10-02
// Updated: 2025-01-18

namespace Index\Syndication;

use DateTimeInterface;
use DOMDocument;
use DOMElement;
use RuntimeException;
use Index\{XDateTime,XString};

/**
 * Provides an RSS feed item builder with Atom extensions.
 */
class FeedItemBuilder {
    /**
     * Title of this feed item.
     *
     * @var string
     */
    public string $title = 'Feed Item';

    /**
     * Description of the feed item, may contain HTML.
     *
     * @var string
     */
    public string $description = '';

    /**
     * Creation time for this feed item.
     *
     * @var DateTimeInterface|int|null
     */
    public DateTimeInterface|int|null $createdAt = null;

    /**
     * Last updated time for this feed item.
     *
     * @var DateTimeInterface|int|null
     */
    public DateTimeInterface|int|null $updatedAt = null;

    /**
     * URL to the content this feed item represents.
     *
     * @var string
     */
    public string $contentUrl = '';

    /**
     * URL to where comments can be made on the content this feed item represents.
     *
     * @var string
     */
    public string $commentsUrl = '';

    /**
     * Name of the author of this feed item.
     *
     * @var string
     */
    public string $authorName = '';

    /**
     * URL for the author of this feed item. Has no effect if no author name is provided.
     *
     * @var string
     */
    public string $authorUrl = '';

    /**
     * Creates an XML document based on this item builder.
     *
     * @param DOMDocument $document Document to which this element is to be appended.
     * @return DOMElement Element created from this builder.
     */
    public function createElement(DOMDocument $document): DOMElement {
        $item = $document->createElement('item');
        if($item === false)
            throw new RuntimeException('creation of item element failed');

        $elements = [
            'title' => $this->title,
            'pubDate' => $this->createdAt === null ? '' : XDateTime::toRfc822String($this->createdAt),
            'atom:updated' => $this->updatedAt === null ? '' : XDateTime::toIso8601String($this->updatedAt),
            'link' => $this->contentUrl,
            'guid' => $this->contentUrl,
            'comments' => $this->commentsUrl,
        ];
        foreach($elements as $elemName => $elemValue)
            if(!XString::nullOrWhitespace($elemValue)) {
                $element = $document->createElement($elemName, $elemValue);
                if($element === false)
                    throw new RuntimeException(sprintf('creation of %s child item element failed', $elemName));

                $item->appendChild($element);
            }

        if(!XString::nullOrWhitespace($this->description)) {
            $description = $document->createElement('description');
            if($description === false)
                throw new RuntimeException('creation of description child item element failed');

            $cdata = $document->createCDATASection($this->description);
            if($cdata === false)
                throw new RuntimeException('creation of description cdata item section failed');

            $description->appendChild($cdata);
            $item->appendChild($description);
        }

        if(!XString::nullOrWhitespace($this->authorName)) {
            $author = $document->createElement('atom:author');
            if($author === false)
                throw new RuntimeException('creation of atom:author child item element failed');

            $item->appendChild($author);

            $authorName = $document->createElement('atom:name', $this->authorName);
            if($authorName === false)
                throw new RuntimeException('creation of atom:name child item element failed');

            $author->appendChild($authorName);

            if(!XString::nullOrWhitespace($this->authorUrl)) {
                $authorUrl = $document->createElement('atom:uri', $this->authorUrl);
                if($authorUrl === false)
                    throw new RuntimeException('creation of atom:uri child item element failed');

                $author->appendChild($authorUrl);
            }
        }

        return $item;
    }
}

<?php
// FeedBuilder.php
// Created: 2024-10-02
// Updated: 2025-01-18

namespace Index\Syndication;

use DateTimeInterface;
use DOMDocument;
use Stringable;
use RuntimeException;
use Index\{XDateTime,XString};

/**
 * Provides an RSS feed builder with Atom extensions.
 */
class FeedBuilder implements Stringable {
    /** @var FeedItemBuilder[] */
    private array $items = [];

    /**
     * Title of the feed.
     *
     * @var string
     */
    public string $title = 'Feed';

    /**
     * Description of the feed, may contain HTML.
     *
     * @var string
     */
    public string $description = '';

    /**
     * Last updated time for this feed.
     *
     * @var DateTimeInterface|int|null
     */
    public DateTimeInterface|int|null $updatedAt = null;

    /**
     * URL to the content this feed represents.
     *
     * @var string
     */
    public string $contentUrl = '';

    /**
     * URL to this feed's XML file.
     *
     * @var string
     */
    public string $feedUrl = '';

    /**
     * Creates a feed item using a builder.
     *
     * @param callable(FeedItemBuilder): void $handler Item builder closure.
     */
    public function createEntry(callable $handler): void {
        $builder = new FeedItemBuilder;
        $handler($builder);
        $this->items[] = $builder;
    }

    /**
     * Creates a DOMDocument from this builder.
     *
     * @throws RuntimeException If the creation of the XML document failed.
     * @return DOMDocument XML document representing this feed.
     */
    public function toXmlDocument(): DOMDocument {
        $document = new DOMDocument('1.0', 'UTF-8');
        $document->preserveWhiteSpace = false;
        $document->formatOutput = true;

        $rss = $document->createElement('rss');
        if($rss === false)
            throw new RuntimeException('failed to create root rss element');

        $rss->setAttribute('version', '2.0');
        $rss->setAttribute('xmlns:atom', 'http://www.w3.org/2005/Atom');
        $document->appendChild($rss);

        $channel = $document->createElement('channel');
        if($channel === false)
            throw new RuntimeException('creation of channel element failed');

        $elements = [
            'title' => $this->title,
            'pubDate' => $this->updatedAt === null ? '' : XDateTime::toRfc822String($this->updatedAt),
            'link' => $this->contentUrl,
        ];
        foreach($elements as $elemName => $elemValue)
            if(!XString::nullOrWhitespace($elemValue)) {
                $element = $document->createElement($elemName, $elemValue);
                if($element === false)
                    throw new RuntimeException(sprintf('creation of %s child element failed', $elemName));

                $channel->appendChild($element);
            }

        if(!XString::nullOrWhitespace($this->description)) {
            $description = $document->createElement('description');
            if($description === false)
                throw new RuntimeException('creation of description child element failed');

            $cdata = $document->createCDATASection($this->description);
            if($cdata === false)
                throw new RuntimeException('creation of description cdata section failed');

            $description->appendChild($cdata);
            $channel->appendChild($description);
        }

        if(!XString::nullOrWhitespace($this->feedUrl)) {
            $element = $document->createElement('atom:link');
            if($element === false)
                throw new RuntimeException('creation of atom:link child element failed');

            $element->setAttribute('href', $this->feedUrl);
            $element->setAttribute('ref', 'self');
            $element->setAttribute('type', 'application/rss+xml');
            $channel->appendChild($element);
        }

        foreach($this->items as $item)
            $channel->appendChild($item->createElement($document));

        $rss->appendChild($channel);

        return $document;
    }

    /**
     * Converts the XML Document to a printable string.
     *
     * @return string Printable XML document.
     */
    public function toXmlString(): string {
        $string = $this->toXmlDocument()->saveXML();
        if($string === false)
            $string = '';

        return $string;
    }

    public function __toString(): string {
        return $this->toXmlString();
    }
}

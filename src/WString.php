<?php
// WString.php
// Created: 2021-06-22
// Updated: 2025-01-18

namespace Index;

use Stringable;

/**
 * Provides various helper methods for multibyte strings.
 */
final class WString {
    /**
     * Checks if a multibyte string starts with a given substring.
     *
     * @param Stringable|string $haystack String to search in.
     * @param Stringable|string $needle Sustring to search for in the haystack.
     * @param ?string $encoding String character encoding. null for mb_internal_encoding value.
     * @return bool true if haystack begins with needle, false otherwise.
     */
    public static function startsWith(Stringable|string $haystack, Stringable|string $needle, ?string $encoding = null): bool {
        return mb_strpos((string)$haystack, (string)$needle, encoding: $encoding) === 0;
    }

    /**
     * Checks if a multibyte string ends with a given substring.
     *
     * @param Stringable|string $haystack String to search in.
     * @param Stringable|string $needle Sustring to search for in the haystack.
     * @param ?string $encoding String character encoding. null for mb_internal_encoding value.
     * @return bool true if haystack ends with needle, false otherwise.
     */
    public static function endsWith(Stringable|string $haystack, Stringable|string $needle, ?string $encoding = null): bool {
        $haystack = (string)$haystack;
        $haystackLength = mb_strlen($haystack, $encoding);

        $needle = (string)$needle;
        $needleLength = mb_strlen($needle, $encoding);

        return mb_substr($haystack, -$needleLength, encoding: $encoding) === $needle;
    }

    /**
     * Reverses a multibyte string.
     *
     * @param Stringable|string $string String to reverse.
     * @param ?string $encoding String character encoding. null for mb_internal_encoding value.
     * @return string Reversed string.
     */
    public static function reverse(Stringable|string $string, ?string $encoding = null): string {
        return implode(array_reverse(mb_str_split((string)$string, encoding: $encoding)));
    }

    /**
     * Counts unique characters in a string.
     *
     * @param Stringable|string $string String to count unique characters of.
     * @param ?string $encoding String character encoding. null for mb_internal_encoding value.
     * @return int Unique character count.
     */
    public static function countUnique(Stringable|string $string, ?string $encoding = null): int {
        $string = mb_str_split((string)$string, encoding: $encoding);
        $chars = [];

        foreach($string as $char)
            if(!in_array($char, $chars, true))
                $chars[] = $char;

        return count($chars);
    }

    /**
     * Check if a multibyte string is null or whitespace.
     *
     * @param Stringable|string|null $string String to check for whitespace.
     * @param ?string $encoding String character encoding. null for mb_internal_encoding value.
     * @return bool true if the string is whitespace, false if not.
     */
    public static function nullOrWhitespace(Stringable|string|null $string, ?string $encoding = null): bool {
        return $string === null || mb_trim((string)$string, encoding: $encoding) === '';
    }
}

<?php
// MediaType.php
// Created: 2022-02-10
// Updated: 2025-02-27

namespace Index;

use InvalidArgumentException;
use RuntimeException;
use Stringable;

/**
 * Implements a structure for representing and comparing media/mime types.
 */
class MediaType implements Stringable, Comparable, Equatable {
    /**
     * @param string $category Media type category. Expected values for this are 'application', 'audio', 'image', 'message', 'multipart', 'text', 'video', 'font', 'example', 'model', 'haptics' or '*' for wildcard.
     * @param string $kind Media type kind.
     * @param string $suffix Media type suffix.
     * @param array<string, mixed> $params Media type parameters.
     */
    public function __construct(
        public private(set) string $category,
        public private(set) string $kind,
        public private(set) string $suffix,
        public private(set) array $params
    ) {}

    /**
     * Checks if a media type parameter is present.
     *
     * @param string $name Media type parameter name.
     * @return bool true if the parameter is present.
     */
    public function hasParam(string $name): bool {
        return isset($this->params[$name]);
    }

    /**
     * Retrieves a media type parameter, or null if it is not present.
     *
     * @param string $name Media type parameter name.
     * @param int $filter A PHP filter extension filter constant.
     * @param array<string, mixed>|int $options Options for the PHP filter.
     * @return mixed Value of the parameter, null if not present.
     */
    public function getParam(string $name, int $filter = FILTER_DEFAULT, array|int $options = 0): mixed {
        if(!isset($this->params[$name]))
            return null;
        return filter_var($this->params[$name], $filter, $options);
    }

    /**
     * Charset parameter from the media type parameters.
     *
     * @var string
     */
    public string $charset {
        get {
            $charset = $this->getParam('charset', FILTER_DEFAULT, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
            return is_string($charset) ? $charset : 'utf-8';
        }
    }

    /**
     * q parameter from the media type parameters.
     *
     * @var float
     */
    public float $quality {
        get {
            $quality = $this->getParam('q', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            if(is_string($quality) || is_int($quality))
                $quality = (float)$quality;
            if(!is_float($quality))
                return 1.0;

            return max(min(round($quality, 2), 1.0), 0.0);
        }
    }

    /**
     * Compares the category string with another one. Can be used for sorting.
     *
     * @param string $category Category string to compare with.
     * @return int 0 if they are a match, something else if not.
     */
    public function compareCategory(string $category): int {
        return ($this->category === '*' || $category === '*')
            ? 0 : strcmp($this->category, $category);
    }

    /**
     * Matches the category string with another one.
     *
     * @param string $category Category string to match with.
     * @return bool true if they are a match.
     */
    public function matchCategory(string $category): bool {
        return $this->category === '*' || $category === '*' || $this->category === $category;
    }

    /**
     * Compares the kind string with another one. Can be used for sorting.
     *
     * @param string $kind Kind string to compare with.
     * @return int 0 if they are a match, something else if not.
     */
    public function compareKind(string $kind): int {
        return ($this->kind === '*' || $kind === '*')
            ? 0 : strcmp($this->kind, $kind);
    }

    /**
     * Matches the kind string with another one.
     *
     * @param string $kind Kind string to match with.
     * @return bool true if they are a match.
     */
    public function matchKind(string $kind): bool {
        return $this->kind === '*' || $kind === '*' || $this->kind === $kind;
    }

    /**
     * Compares the suffix string with another one. Can be used for sorting.
     *
     * @param string $suffix Suffix string to compare with.
     * @return int 0 if they are a match, something else if not.
     */
    public function compareSuffix(string $suffix): int {
        return strcmp($this->suffix, $suffix);
    }

    /**
     * Matches the suffix string with another one.
     *
     * @param string $suffix Suffix string to match with.
     * @return bool true if they are a match.
     */
    public function matchSuffix(string $suffix): bool {
        return $this->suffix === $suffix;
    }

    public function compare(mixed $other): int {
        if(!($other instanceof MediaType)) {
            if(!is_scalar($other))
                return -1;

            try {
                $other = self::parse((string)$other);
            } catch(InvalidArgumentException $ex) {
                return -1;
            }
        }

        if(($match = self::compareCategory($other->category)) !== 0)
            return $match;
        if(($match = self::compareKind($other->kind)) !== 0)
            return $match;

        return 0;
    }

    public function equals(mixed $other): bool {
        if(!($other instanceof MediaType)) {
            if(!is_scalar($other))
                return false;

            try {
                $other = self::parse((string)$other);
            } catch(InvalidArgumentException $ex) {
                return false;
            }
        }

        if(!$this->matchCategory($other->category))
            return false;
        if(!$this->matchKind($other->kind))
            return false;

        return true;
    }

    public function __toString(): string {
        $string = $this->category . '/' . $this->kind;

        if(!empty($this->suffix))
            $string .= '+' . $this->suffix;

        if(!empty($this->params))
            foreach($this->params as $key => $value) {
                $string .= ';';
                if(is_string($key))
                    $string .= $key . '=';
                if(is_scalar($value))
                    $string .= (string)$value;
            }

        return $string;
    }

    /**
     * Parses a media type string into a MediaType object.
     *
     * @param string $string Media type string.
     * @return MediaType Object representing the given string.
     */
    public static function parse(string $string): MediaType {
        $parts = explode(';', $string);
        $string = array_shift($parts);
        $stringParts = explode('/', $string, 2);

        $category = $stringParts[0];

        $kindSplit = explode('+', $stringParts[1] ?? '', 2);
        $kind = $kindSplit[0];
        $suffix = $kindSplit[1] ?? '';

        $params = [];

        foreach($parts as $part) {
            $paramSplit = explode('=', trim($part), 2);
            $params[trim($paramSplit[0])] = trim($paramSplit[1] ?? '', " \n\r\t\v\0\"");
        }

        return new MediaType($category, $kind, $suffix, $params);
    }

    /**
     * Reads the media type of a given file path.
     *
     * Uses mime_content_type().
     *
     * @param string $path File path.
     * @throws RuntimeException If $path does not exist.
     * @return MediaType Media type of file.
     */
    public static function fromPath(string $path): MediaType {
        $string = mime_content_type($path);
        if($string === false)
            throw new RuntimeException('File specified in $path does not exist.');

        return self::parse($string);
    }
}

<?php
// Comparable.php
// Created: 2021-04-30
// Updated: 2025-01-18

namespace Index;

/**
 * Provides an interface for comparison between objects. Allows for order/sorting instances.
 */
interface Comparable {
    /**
     * Compares the current instance with another and returns an integer that indicates whether
     *  the current object comes before or after or the same position and the other object.
     *
     * @return int A value that indicates the relative order of the objects being compared.
     *             Less than zero for before, zero for same position, greater than zero for after.
     */
    public function compare(mixed $other): int;
}

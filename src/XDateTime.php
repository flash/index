<?php
// XDateTime.php
// Created: 2024-07-31
// Updated: 2025-01-18

namespace Index;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;

/**
 * Provides a set of DateTime related utilities.
 */
final class XDateTime {
    private const string COMPARE_FORMAT = 'YmdHisu';

    /**
     * Returns the current date and time.
     *
     * @return DateTimeInterface Current date and time.
     */
    public static function now(): DateTimeInterface {
        return new DateTimeImmutable('now');
    }

    private static function toCompareFormat(DateTimeInterface|string|int $dt): string {
        if($dt instanceof DateTimeInterface)
            return $dt->format(self::COMPARE_FORMAT);

        if(is_string($dt))
            $dt = strtotime($dt);
        if($dt === false)
            $dt = null;

        return gmdate(self::COMPARE_FORMAT, $dt);
    }

    /**
     * Compares two timestamps.
     *
     * @param DateTimeInterface|string|int $dt1 Base timestamp.
     * @param DateTimeInterface|string|int $dt2 Timestamp to compare against.
     * @return int 0 if they are equal, less than 0 if $dt1 is less than $dt2, greater than 0 if $dt1 is greater than $dt2.
     */
    public static function compare(
        DateTimeInterface|string|int $dt1,
        DateTimeInterface|string|int $dt2
    ): int {
        return strcmp(
            self::toCompareFormat($dt1),
            self::toCompareFormat($dt2)
        );
    }

    /**
     * Compares two timezones for sorting.
     *
     * @param DateTimeZone|string $tz1 Base time zone.
     * @param DateTimeZone|string $tz2 Time zone to compare against.
     * @return int 0 if they are equal, less than 0 if $tz1 is less than $tz2, greater than 0 if $tz1 is greater than $tz2.
     */
    public static function compareTimeZone(
        DateTimeZone|string $tz1,
        DateTimeZone|string $tz2
    ): int {
        if(is_string($tz1))
            $tz1 = new DateTimeZone($tz1);
        if(is_string($tz2))
            $tz2 = new DateTimeZone($tz2);

        $now = self::now();
        $diff = $tz1->getOffset($now) <=> $tz2->getOffset($now);
        if($diff) return $diff;

        return strcmp(
            $tz1->getName(),
            $tz2->getName()
        );
    }

    /**
     * Formats a time zone as a string for a list.
     *
     * @param DateTimeZone $dtz Time zone to format.
     * @return string Formatted time zone string.
     */
    public static function timeZoneListName(DateTimeZone $dtz): string {
        $offset = $dtz->getOffset(self::now());
        return sprintf(
            '(UTC%s%s) %s',
            $offset < 0 ? '-' : '+',
            date('H:i', abs($offset)),
            $dtz->getName()
        );
    }

    /**
     * Generates a list of time zones.
     *
     * @param bool $sort true if the list should be sorted.
     * @param int $group Group of time zones to fetch.
     * @param ?string $countryCode Country code of country of which to fetch time zones.
     * @return DateTimeZone[] Collection of DateTimeZone instances.
     */
    public static function listTimeZones(bool $sort = false, int $group = DateTimeZone::ALL, string|null $countryCode = null): array {
        $list = DateTimeZone::listIdentifiers($group, $countryCode);
        $list = XArray::select($list, fn($id) => new DateTimeZone($id));

        if($sort)
            $list = XArray::sort($list, self::compareTimeZone(...));

        return $list;
    }

    /**
     * Formats a date and time as a string.
     *
     * @param DateTimeInterface|string|int|null $dt Date time to format, null for now.
     * @param string $format DateTimeInterface::format date formatting string.
     * @return string Provided date, formatted as requested.
     */
    public static function format(DateTimeInterface|string|int|null $dt, string $format): string {
        if($dt === null) {
            $dt = time();
        } else {
            if($dt instanceof DateTimeInterface)
                return $dt->format($format);

            if(is_string($dt))
                $dt = strtotime($dt);
        }

        if($dt === false)
            $dt = null;

        return gmdate($format, $dt);
    }

    /**
     * Formats a date and time as an ISO-8601 string.
     *
     * @param DateTimeInterface|string|int|null $dt Date time to format, null for now.
     * @return string ISO-8601 date time string.
     */
    public static function toIso8601String(DateTimeInterface|string|int|null $dt): string {
        return self::format($dt, DateTimeInterface::ATOM);
    }

    /**
     * Formats a date and time as a Cookie date/time string.
     *
     * @param DateTimeInterface|string|int|null $dt Date time to format, null for now.
     * @return string Cookie date time string.
     */
    public static function toCookieString(DateTimeInterface|string|int|null $dt): string {
        return self::format($dt, DateTimeInterface::COOKIE);
    }

    /**
     * Formats a date and time as an RFC 822 date/time string.
     *
     * @param DateTimeInterface|string|int|null $dt Date time to format, null for now.
     * @return string RFC 822 date time string.
     */
    public static function toRfc822String(DateTimeInterface|string|int|null $dt): string {
        return self::format($dt, DateTimeInterface::RFC822);
    }
}

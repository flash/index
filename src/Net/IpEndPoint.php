<?php
// IpEndPoint.php
// Created: 2021-04-30
// Updated: 2025-01-18

namespace Index\Net;

use InvalidArgumentException;
use Index\XString;

/**
 * Represents an IP address end point.
 */
class IpEndPoint extends EndPoint {
    /**
     * @param IpAddress $address IP address.
     * @param int $port Network port, 0 to leave default.
     * @throws InvalidArgumentException If $port is less than 0 or greater than 65535.
     */
    public function __construct(
        public private(set) IpAddress $address,
        public private(set) int $port
    ) {
        if($port < 0 || $port > 0xFFFF)
            throw new InvalidArgumentException('$port is not a valid port number.');
    }

    public function __serialize(): array {
        return [$this->address, $this->port];
    }

    /** @param array{IpAddress, int} $serialized */
    public function __unserialize(array $serialized): void {
        $this->address = $serialized[0];
        $this->port = $serialized[1];
    }

    public function equals(mixed $other): bool {
        return $other instanceof IpEndPoint
            && $this->port === $other->port
            && $this->address->equals($other->address);
    }

    /**
     * Attempts to parse a string into an IP end point.
     *
     * @param string $string String to parse.
     * @throws InvalidArgumentException If $string does not contain a valid IP end point string.
     * @return IpEndPoint Representation of the given string.
     */
    public static function parse(string $string): IpEndPoint {
        if(str_starts_with($string, '[')) { // IPv6
            $closing = strpos($string, ']');
            if($closing === false)
                throw new InvalidArgumentException('$string is not a valid IP end point.');
            $ip = substr($string, 1, $closing - 1);
            $port = (int)substr($string, $closing + 2);
        } else { // IPv4
            $parts = explode(':', $string, 2);
            if(count($parts) < 2)
                throw new InvalidArgumentException('$string is not a valid IP end point.');
            $ip = $parts[0];
            $port = (int)$parts[1];
        }

        return new IpEndPoint(IpAddress::parse($ip), $port);
    }

    public function __toString(): string {
        if($this->port < 1)
            return $this->address->address;
        return $this->address->address . ':' . $this->port;
    }
}

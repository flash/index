<?php
// IpAddress.php
// Created: 2021-04-26
// Updated: 2025-02-27

namespace Index\Net;

use InvalidArgumentException;
use JsonSerializable;
use Stringable;
use Index\Equatable;

/**
 * Represents an IP address.
 */
final class IpAddress implements JsonSerializable, Stringable, Equatable {
    /**
     * @param string $raw Raw IP address data.
     */
    public function __construct(
        public private(set) string $raw
    ) {
        $this->width = strlen($raw);
    }

    /**
     * Gets the width of the raw address byte data.
     *
     * 4 bytes for IPv4
     * 16 bytes for IPv6
     *
     * @var int
     */
    public private(set) int $width;

    /**
     * Safe string representation of IP address.
     *
     * IPv6 addresses will be wrapped in square brackets.
     * Use getCleanAddress if this is undesirable.
     *
     * @var string
     */
    public string $address {
        get => sprintf($this->v6 ? '[%s]' : '%s', $this->clean);
    }

    /**
     * String representation of IP address.
     *
     * @var string
     */
    public string $clean {
        get {
            $string = inet_ntop($this->raw);
            if($string === false)
                return '';

            return $string;
        }
    }

    /**
     * IP version this address is for.
     *
     * @var IpAddressVersion
     */
    public IpAddressVersion $version {
        get {
            if($this->v4)
                return IpAddressVersion::V4;
            if($this->v6)
                return IpAddressVersion::V6;
            return IpAddressVersion::Unknown;
        }
    }

    /**
     * Whether this is an IPv4 address.
     *
     * @var bool
     */
    public bool $v4 {
        get => $this->width === 4;
    }

    /**
     * Whether this is an IPv6 address.
     *
     * @var bool
     */
    public bool $v6 {
        get => $this->width === 16;
    }

    public function __toString(): string {
        return $this->clean;
    }

    public function equals(mixed $other): bool {
        return $other instanceof IpAddress && $this->raw === $other->raw;
    }

    #[\Override]
    public function jsonSerialize(): mixed {
        return $this->clean;
    }

    public function __serialize(): array {
        return [$this->raw];
    }

    /** @param array{string} $serialized */
    public function __unserialize(array $serialized): void {
        $this->raw = $serialized[0];
        $this->width = strlen($this->raw);
    }

    /**
     * Attempts to parse a string into an IP address.
     *
     * @param string $string String to parse.
     * @throws InvalidArgumentException If $string does not contain a valid IP address string.
     * @return IpAddress Representation of the given string.
     */
    public static function parse(string $string): self {
        $parsed = inet_pton($string);
        if($parsed === false)
            throw new InvalidArgumentException('$string is not a valid IP address.');

        return new static($parsed);
    }
}

<?php
// UnixEndPoint.php
// Created: 2021-04-30
// Updated: 2025-01-18

namespace Index\Net;

/**
 * Represents a UNIX socket path end point.
 */
class UnixEndPoint extends EndPoint {
    /**
     * @param string $path Path to the socket file.
     */
    public function __construct(
        public private(set) string $path
    ) {}

    public function __serialize(): array {
        return [$this->path];
    }

    /** @param array{string} $serialized */
    public function __unserialize(array $serialized): void {
        $this->path = $serialized[0];
    }

    public function equals(mixed $other): bool {
        return $other instanceof UnixEndPoint
            && $this->path === $other->path;
    }

    /**
     * Attempts to parse a string into an UNIX end point.
     *
     * @param string $string String to parse.
     * @return UnixEndPoint Representation of the given string.
     */
    public static function parse(string $string): UnixEndPoint {
        if(str_starts_with($string, 'unix:')) {
            $uri = parse_url($string);
            $string = $uri['path'] ?? '';
        }

        return new UnixEndPoint($string);
    }

    public function __toString(): string {
        return $this->path;
    }
}

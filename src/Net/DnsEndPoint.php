<?php
// DnsEndPoint.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Net;

use InvalidArgumentException;

/**
 * A DNS end point.
 */
class DnsEndPoint extends EndPoint {
    /**
     * @param string $host DNS host name.
     * @param int $port Network port, 0 to leave default.
     * @throws InvalidArgumentException If $port is less than 0 or greater than 65535.
     */
    public function __construct(
        public private(set) string $host,
        public private(set) int $port
    ) {
        if($port < 0 || $port > 0xFFFF)
            throw new InvalidArgumentException('$port is not a valid port number.');
    }

    public function __serialize(): array {
        return [$this->host, $this->port];
    }

    /** @param array{string,int} $serialized */
    public function __unserialize(array $serialized): void {
        $this->host = $serialized[0];
        $this->port = $serialized[1];
    }

    public function equals(mixed $other): bool {
        return $other instanceof DnsEndPoint
            && $this->port === $other->port
            && $this->host === $other->host;
    }

    /**
     * Attempts to parse a string into a DNS end point.
     *
     * @param string $string String to parse.
     * @throws InvalidArgumentException If $string does not contain a valid DNS end point string.
     * @return DnsEndPoint Representation of the given string.
     */
    public static function parse(string $string): DnsEndPoint {
        $parts = explode(':', $string);
        if(count($parts) < 2)
            throw new InvalidArgumentException('$string is not a valid host and port combo.');
        return new DnsEndPoint($parts[0], (int)($parts[1] ?? '0'));
    }

    public function __toString(): string {
        if($this->port < 1)
            return $this->host;
        return $this->host . ':' . $this->port;
    }
}

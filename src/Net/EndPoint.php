<?php
// EndPoint.php
// Created: 2021-04-30
// Updated: 2024-10-02

namespace Index\Net;

use InvalidArgumentException;
use JsonSerializable;
use Stringable;
use Index\Equatable;

/**
 * Represents a generic network end point.
 */
abstract class EndPoint implements JsonSerializable, Stringable, Equatable {
    abstract public function equals(mixed $other): bool;
    abstract public function __toString(): string;

    public function jsonSerialize(): mixed {
        return (string)$this;
    }

    /**
     * Attempts to parse a string into a known end point object.
     *
     * @param string $string String to parse.
     * @throws InvalidArgumentException If $string does not contain a valid end point string.
     * @return EndPoint Representation of the given string.
     */
    public static function parse(string $string): EndPoint {
        if($string === '')
            throw new InvalidArgumentException('$string is not a valid endpoint string.');

        $firstChar = $string[0];

        if($firstChar === '/' || str_starts_with($string, 'unix:'))
            $endPoint = UnixEndPoint::parse($string);
        elseif($firstChar === '[') { // IPv6
            if(str_contains($string, ']:'))
                $endPoint = IpEndPoint::parse($string);
            else
                $endPoint = new IpEndPoint(IpAddress::parse(trim($string, '[]')), 0);
        } elseif(is_numeric($firstChar)) { // IPv4
            if(str_contains($string, ':'))
                $endPoint = IpEndPoint::parse($string);
            else
                $endPoint = new IpEndPoint(IpAddress::parse($string), 0);
        } else { // DNS
            if(str_contains($string, ':'))
                $endPoint = DnsEndPoint::parse($string);
            else
                $endPoint = new DnsEndPoint($string, 0);
        }

        return $endPoint;
    }
}

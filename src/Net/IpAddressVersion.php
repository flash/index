<?php
// IpAddressVersion.php
// Created: 2025-01-18
// Updated: 2025-01-18

namespace Index\Net;

/**
 * IP address versions.
 */
enum IpAddressVersion: int {
    /**
     * Unknown.
     */
    case Unknown = 0;

    /**
     * IPv4 address.
     */
    case V4 = 4;

    /**
     * IPv6 address.
     */
    case V6 = 6;
}

<?php
// IpAddressRange.php
// Created: 2021-04-26
// Updated: 2025-02-27

namespace Index\Net;

use InvalidArgumentException;
use JsonSerializable;
use Stringable;
use Index\Equatable;

/**
 * Represents a CIDR range of IP addresses.
 */
final class IpAddressRange implements JsonSerializable, Stringable, Equatable {
    /**
     * @param IpAddress $base Base IP address.
     * @param int $mask CIDR mask.
     */
    public function __construct(
        public private(set) IpAddress $base,
        public private(set) int $mask
    ) {}

    /**
     * Full CIDR representation of this range.
     *
     * @var string
     */
    public string $cidr {
        get => ((string)$this->base) . '/' . $this->mask;
    }

    public function __toString(): string {
        return $this->cidr;
    }

    public function equals(mixed $other): bool {
        return $other instanceof IpAddressRange
            && $this->mask === $other->mask
            && $this->base->equals($other->base);
    }

    /**
     * Checks if a given IP address matches with this range.
     *
     * @param IpAddress $address IP address to check.
     * @return bool true if the address matches.
     */
    public function match(IpAddress $address): bool {
        $width = $this->base->width;
        if($address->width !== $width)
            return false;

        if($this->mask < 1)
            return true;

        $base = $this->base->raw;
        $match = $address->raw;
        $remaining = $this->mask;

        for($i = 0; $i < $width && $remaining > 0; ++$i) {
            $b = ord($base[$i]);
            $m = ord($match[$i]);

            $remainingMod = $remaining % 8;
            if($remainingMod) {
                $mask = 0xFF << (8 - $remainingMod);
                $b &= $mask;
                $m &= $mask;
            }

            if($b !== $m)
                return false;

            $remaining -= 8;
        }

        return true;
    }

    public function jsonSerialize(): mixed {
        return $this->cidr;
    }

    public function __serialize(): array {
        return [
            'b' => $this->base->raw,
            'm' => $this->mask,
        ];
    }

    /** @param array{b: string, m: int} $serialized */
    public function __unserialize(array $serialized): void {
        $this->base = new IpAddress($serialized['b']);
        $this->mask = $serialized['m'];
    }

    /**
     * Attempts to parse a string into an IP address range.
     *
     * @param string $string String to parse.
     * @throws InvalidArgumentException If $string does not contain a valid CIDR range string.
     * @return IpAddressRange Representation of the given string.
     */
    public static function parse(string $string): IpAddressRange {
        $parts = explode('/', $string, 2);
        if(empty($parts[0]) || empty($parts[1]))
            throw new InvalidArgumentException('$string is not a valid CIDR range.');
        return new static(IpAddress::parse($parts[0]), (int)$parts[1]);
    }
}

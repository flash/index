<?php
// XNumber.php
// Created: 2023-09-15
// Updated: 2025-01-18

namespace Index;

/**
 * Provides various helper methods for numbers.
 */
final class XNumber {
    public const string BASE62 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    private static ?IntegerBaseConverter $base62Converter = null;

    private static function base62Converter(): IntegerBaseConverter {
        self::$base62Converter ??= new IntegerBaseConverter(self::BASE62);
        return self::$base62Converter;
    }

    /**
     * Converts a base10 integer to a base62 integer.
     *
     * @param int $integer The integer to encode.
     * @return string The encoded data, as a string.
     */
    public static function toBase62(int $integer): string {
        return self::base62Converter()->encode($integer);
    }

    /**
     * Converts a base62 integer to a base10 integer.
     *
     * @param string $string The encoded integer.
     * @return int|false Returns the decoded integer or false on failure.
     */
    public static function fromBase62(string $string): int|false {
        return self::base62Converter()->decode($string);
    }

    public static function weighted(float $num1, float $num2, float $weight): float {
        $weight = min(1, max(0, $weight));
        return ($num1 * $weight) + ($num2 * (1 - $weight));
    }

    public static function almostEquals(float $value1, float $value2): bool {
        return abs($value1 - $value2) <= PHP_FLOAT_EPSILON;
    }

    public static function easeInQuad(float $n): float {
        return $n * $n;
    }

    public static function easeOutQuad(float $n): float {
        return 1 - (1 - $n) * (1 - $n);
    }
}

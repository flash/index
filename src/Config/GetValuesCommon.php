<?php
// GetValuesCommon.php
// Created: 2023-10-20
// Updated: 2025-01-18

namespace Index\Config;

use InvalidArgumentException;

/**
 * Provides implementation for {@see Config::getValues} based on {@see Config::getValueInfos}.
 */
trait GetValuesCommon {
    /**
     * Format described in {@see Config::getValues}.
     *
     * @param array<string|array{0: string, 1?: mixed, 2?: string}> $specs Specification of what items to grab.
     * @throws InvalidArgumentException If $specs contains an invalid entry.
     * @return array<string, mixed>
     */
    public function getValues(array $specs): array {
        $names = [];
        $evald = [];

        foreach($specs as $key => $spec) {
            if(is_string($spec)) {
                $name = $spec;
                $default = null;
                $alias = null;
            } elseif(is_array($spec) && !empty($spec)) {
                $name = $spec[0];
                $default = $spec[1] ?? null;
                $alias = $spec[2] ?? null;
            } else
                throw new InvalidArgumentException('$specs array contains an invalid entry');

            $nameLength = strlen($name);
            if($nameLength > 3 && ($colon = strrpos($name, ':')) === $nameLength - 2) {
                $type = substr($name, $colon + 1, 1);
                $name = substr($name, 0, $colon);
            } else $type = '';

            $names[] = $name;
            $evald[$key] = [
                'name' => $name,
                'type' => $type,
                'default' => $default,
                'alias' => $alias,
            ];
        }

        $infos = $this->getValueInfos($names);
        $results = [];

        foreach($evald as $spec) {
            foreach($infos as $infoTest)
                if($infoTest->name === $spec['name']) {
                    $info = $infoTest;
                    break;
                }

            $resultName = $spec['alias'] ?? $spec['name'];

            if(!isset($info)) {
                $defaultValue = $spec['default'] ?? null;
                if($spec['type'] !== '')
                    settype($defaultValue, match($spec['type']) {
                        's' => 'string',
                        'a' => 'array',
                        'i' => 'int',
                        'b' => 'bool',
                        'f' => 'float',
                        'd' => 'double',
                        default => throw new InvalidArgumentException(sprintf('invalid type letter encountered: "%s"', $spec['type'])),
                    });

                $results[$resultName] = $defaultValue;
                continue;
            }

            $results[$resultName] = match($spec['type']) {
                's' => $info->string,
                'a' => $info->array,
                'i' => $info->int,
                'b' => $info->bool,
                'f' => $info->float,
                'd' => $info->float,
                '' => $info->value,
                default => throw new InvalidArgumentException('unknown type encountered in $specs'),
            };

            unset($info);
        }

        return $results;
    }
}

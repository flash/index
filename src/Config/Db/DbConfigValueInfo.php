<?php
// DbConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2025-02-27

namespace Index\Config\Db;

use UnexpectedValueException;
use Index\Config\ConfigValueInfo;
use Index\Db\DbResult;

/**
 * Provides information about a databased configuration value.
 */
class DbConfigValueInfo implements ConfigValueInfo {
    public private(set) string $name;
    private string $raw;

    /**
     * @param DbResult $result Database result for this config value.
     */
    public function __construct(DbResult $result) {
        $this->name = $result->getString(0);
        $this->raw = $result->getString(1);
    }

    public string $type {
        get => match($this->raw[0]) {
            'b' => 'bool',
            'a' => 'array',
            'd' => 'float',
            'i' => 'int',
            's' => 'string',
            default => 'unknown',
        };
    }

    public bool $isBool   { get => $this->raw[0] === 'b'; }
    public bool $isArray  { get => $this->raw[0] === 'a'; }
    public bool $isFloat  { get => $this->raw[0] === 'd'; }
    public bool $isInt    { get => $this->raw[0] === 'i'; }
    public bool $isString { get => $this->raw[0] === 's'; }

    public mixed $value {
        get => unserialize($this->raw);
    }

    public string $string {
        get {
            $value = $this->value;
            return is_string($value) ? $value : '';
        }
    }

    public int $int {
        get {
            $value = $this->value;
            return is_int($value) ? $value : 0;
        }
    }

    public float $float {
        get {
            $value = $this->value;
            return is_float($value) ? $value : 0.0;
        }
    }

    public bool $bool {
        get {
            $value = $this->value;
            return is_bool($value) ? $value : false;
        }
    }

    public array $array {
        get {
            $value = $this->value;
            return is_array($value) ? $value : [];
        }
    }

    public function __toString(): string {
        $value = $this->value;
        if(is_array($value))
            return implode(', ', $value);
        if(is_scalar($value))
            return (string)$value;

        return '';
    }
}

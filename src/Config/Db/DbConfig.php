<?php
// DbConfig.php
// Created: 2023-10-20
// Updated: 2025-01-18

namespace Index\Config\Db;

use InvalidArgumentException;
use Index\Config\{Config,GetValueInfoCommon,GetValuesCommon,MutableConfigCommon,ScopedConfig};
use Index\Db\{DbConnection,DbStatementCache,DbTools};

/**
 * Provides a configuration based on a {@see DbConnection} instance.
 *
 * @todo provide table name in constructor
 * @todo scan for vendor specific queries and generalise them
 * @todo getValues() parsing should probably be done external so it can be reused
 */
class DbConfig implements Config {
    use MutableConfigCommon, GetValueInfoCommon, GetValuesCommon;

    private DbStatementCache $cache;

    /** @var array<string, DbConfigValueInfo> */
    private array $values = [];

    /** @var string[] */
    private array $extraFieldNames;

    /** @var mixed[] */
    private array $extraFieldValues;

    public private(set) string $separator = '.';

    /**
     * @param DbConnection $dbConn
     * @param string $tableName
     * @param string $nameField
     * @param string $valueField
     * @param array<string, mixed> $extraFields
     */
    public function __construct(
        DbConnection $dbConn,
        private string $tableName,
        private string $nameField = 'config_name',
        private string $valueField = 'config_value',
        array $extraFields = [],
    ) {
        $this->cache = new DbStatementCache($dbConn);
        $this->extraFieldNames = array_keys($extraFields);
        $this->extraFieldValues = array_values($extraFields);
    }

    public static function validateName(string $name): bool {
        $parts = explode('.', $name);
        foreach($parts as $part) {
            if($part === '' || trim($part) !== $part)
                return false;

            if(preg_match('#^([a-z][a-zA-Z0-9_]+)$#', $part) !== 1)
                return false;
        }

        return true;
    }

    /**
     * Resets value cache.
     */
    public function reset(): void {
        $this->values = [];
    }

    /**
     * Unloads specifics items from the local cache.
     *
     * @param string|string[] $names Names of values to unload.
     */
    public function unload(string|array $names): void {
        if(empty($names))
            return;
        if(is_string($names))
            $names = [$names];

        foreach($names as $name)
            unset($this->values[$name]);
    }

    public function scopeTo(string ...$prefix): Config {
        return new ScopedConfig($this, $prefix);
    }

    public function hasValues(string|array $names): bool {
        if(empty($names))
            return true;
        if(is_string($names))
            $names = [$names];

        $cachedNames = array_keys($this->values);
        $names = array_diff($names, $cachedNames);

        if(!empty($names)) {
            // array_diff preserves keys, the for() later would fuck up without it
            $names = array_values($names);
            $nameCount = count($names);

            $query = sprintf(
                'SELECT COUNT(*) FROM %s WHERE %s IN (%s)',
                $this->tableName, $this->nameField,
                DbTools::prepareListString($nameCount)
            );
            foreach($this->extraFieldNames as $extraFieldName)
                $query .= sprintf(' AND %s = ?', $extraFieldName);

            $stmt = $this->cache->get($query);

            foreach($names as $name)
                $stmt->nextParameter($name);
            foreach($this->extraFieldValues as $extraFieldValue)
                $stmt->nextParameter($extraFieldValue);

            $stmt->execute();

            $result = $stmt->getResult();
            if($result->next())
                return $result->getInteger(0) >= $nameCount;
        }

        return true;
    }

    public function removeValues(string|array $names): void {
        if(empty($names))
            return;
        if(is_string($names))
            $names = [$names];

        foreach($names as $name)
            unset($this->values[$name]);

        $nameCount = count($names);
        $query = sprintf(
            'DELETE FROM %s WHERE %s IN (%s)',
            $this->tableName, $this->nameField,
            DbTools::prepareListString($nameCount)
        );
        foreach($this->extraFieldNames as $extraFieldName)
            $query .= sprintf(' AND %s = ?', $extraFieldName);

        $stmt = $this->cache->get($query);

        foreach($names as $name)
            $stmt->nextParameter($name);
        foreach($this->extraFieldValues as $extraFieldValue)
            $stmt->nextParameter($extraFieldValue);

        $stmt->execute();
    }

    public function getAllValueInfos(int $range = 0, int $offset = 0): array {
        $this->reset();
        $infos = [];

        $hasRange = $range !== 0;

        $query = sprintf('SELECT %s, %s FROM %s', $this->nameField, $this->valueField, $this->tableName);
        foreach($this->extraFieldNames as $i => $extraFieldName)
            $query .= sprintf(' %s %s = ?', $i < 1 ? 'WHERE' : 'AND', $extraFieldName);
        if($hasRange) {
            if($range < 0)
                throw new InvalidArgumentException('$range must be a positive integer');
            if($offset < 0)
                throw new InvalidArgumentException('$offset must be greater than zero if a range is specified');

            $query .= ' LIMIT ? OFFSET ?';
        }

        $stmt = $this->cache->get($query);

        foreach($this->extraFieldValues as $extraFieldValue)
            $stmt->nextParameter($extraFieldValue);
        if($hasRange) {
            $stmt->nextParameter($range);
            $stmt->nextParameter($offset);
        }

        $stmt->execute();

        $result = $stmt->getResult();
        while($result->next())
            $infos[] = $this->values[$result->getString(0)] = new DbConfigValueInfo($result);

        return $infos;
    }

    public function getValueInfos(string|array $names): array {
        if(empty($names))
            return [];
        if(is_string($names))
            $names = [$names];

        $infos = [];
        $skip = [];

        foreach($names as $name)
            if(array_key_exists($name, $this->values)) {
                $infos[] = $this->values[$name];
                $skip[] = $name;
            }

        $names = array_diff($names, $skip);

        if(!empty($names)) {
            // array_diff preserves keys, the for() later would fuck up without it
            $names = array_values($names);
            $nameCount = count($names);

            $query = sprintf(
                'SELECT %s, %s FROM %s WHERE %s IN (%s)',
                $this->nameField, $this->valueField, $this->tableName, $this->nameField,
                DbTools::prepareListString($nameCount)
            );
            foreach($this->extraFieldNames as $extraFieldName)
                $query .= sprintf(' AND %s = ?', $extraFieldName);

            $stmt = $this->cache->get($query);

            foreach($names as $name)
                $stmt->nextParameter($name);
            foreach($this->extraFieldValues as $extraFieldValue)
                $stmt->nextParameter($extraFieldValue);

            $stmt->execute();

            $result = $stmt->getResult();
            while($result->next())
                $infos[] = $this->values[$result->getString(0)] = new DbConfigValueInfo($result);
        }

        return $infos;
    }

    public function setValues(array $values): void {
        if(empty($values))
            return;

        $fields = [$this->nameField, $this->valueField];
        if(count($this->extraFieldNames) > 0)
            $fields = array_merge($fields, $this->extraFieldNames);
        $fieldCount = count($fields);
        $fields = implode(', ', $fields);

        $stmt = $this->cache->get(sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $this->tableName, $fields,
            DbTools::prepareListString($fieldCount)
        ));

        foreach($values as $name => $value) {
            if(!self::validateName($name))
                throw new InvalidArgumentException('invalid name encountered in $values');

            if(is_array($value)) {
                foreach($value as $entry)
                    if(!is_scalar($entry))
                        throw new InvalidArgumentException('an array value in $values contains a non-scalar type');
            } elseif(!is_scalar($value))
                throw new InvalidArgumentException('invalid value type encountered in $values');

            $this->removeValues($name);

            $stmt->reset();
            $stmt->nextParameter($name);
            $stmt->nextParameter(serialize($value));
            foreach($this->extraFieldValues as $extraFieldValue)
                $stmt->nextParameter($extraFieldValue);

            $stmt->execute();
        }
    }
}

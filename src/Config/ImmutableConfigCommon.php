<?php
// ImmutableConfigCommon.php
// Created: 2023-10-20
// Updated: 2025-01-18

namespace Index\Config;

use RuntimeException;

/**
 * Intercepts mutable methods required to be implemented by {@see Config} and returns exceptions.
 */
trait ImmutableConfigCommon {
    public function removeValues(string|array $names): void {
        throw new RuntimeException('this configuration is read only');
    }

    public function setValues(array $values): void {
        throw new RuntimeException('this configuration is read only');
    }

    public function setString(string $name, string $value): void {
        throw new RuntimeException('this configuration is read only');
    }

    public function setInteger(string $name, int $value): void {
        throw new RuntimeException('this configuration is read only');
    }

    public function setFloat(string $name, float $value): void {
        throw new RuntimeException('this configuration is read only');
    }

    public function setBoolean(string $name, bool $value): void {
        throw new RuntimeException('this configuration is read only');
    }

    public function setArray(string $name, array $value): void {
        throw new RuntimeException('this configuration is read only');
    }
}

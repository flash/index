<?php
// FsConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2025-02-27

namespace Index\Config\Fs;

use Index\Config\ConfigValueInfo;

/**
 * Value info for file configs.
 */
class FsConfigValueInfo implements ConfigValueInfo {
    /**
     * @param string $name Name of the config value.
     * @param string $value Value of the config value.
     */
    public function __construct(
        public private(set) string $name,
        public private(set) string $value = ''
    ) {
        $this->name = $name;
        $this->value = trim($value);
    }

    public private(set) string $type = 'string';
    public private(set) bool $isBool = true;
    public private(set) bool $isArray = true;
    public private(set) bool $isFloat = true;
    public private(set) bool $isInt = true;
    public private(set) bool $isString = true;

    public string $string { get => $this->value; }
    public int    $int    { get => (int)$this->value; }
    public float  $float  { get => (float)$this->value; }
    public bool   $bool   { get => $this->value !== '0' && strcasecmp($this->value, 'false') !== 0; }
    public array  $array  { get => explode(' ', $this->value); }

    public function __toString(): string {
        return $this->value;
    }
}

<?php
// GetValueInfoCommon.php
// Created: 2023-10-20
// Updated: 2025-01-18

namespace Index\Config;

/**
 * Provides implementations for things that are essentially macros for {@see Config::getValueInfos}.
 */
trait GetValueInfoCommon {
    public function getValueInfo(string $name): ?ConfigValueInfo {
        $infos = $this->getValueInfos($name);
        return empty($infos) ? null : $infos[0];
    }

    public function getString(string $name, string $default = ''): string {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isString ? $valueInfo->string : $default;
    }

    public function getInteger(string $name, int $default = 0): int {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isInt ? $valueInfo->int : $default;
    }

    public function getFloat(string $name, float $default = 0): float {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isFloat ? $valueInfo->float : $default;
    }

    public function getBoolean(string $name, bool $default = false): bool {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isBool ? $valueInfo->bool : $default;
    }

    public function getArray(string $name, array $default = []): array {
        $valueInfo = $this->getValueInfo($name);
        return $valueInfo?->isArray ? $valueInfo->array : $default;
    }
}

<?php
// MutableConfigCommon.php
// Created: 2023-10-20
// Updated: 2025-01-18

namespace Index\Config;

/**
 * Defines set aliases so you don't have to.
 */
trait MutableConfigCommon {
    public function setString(string $name, string $value): void {
        $this->setValues([$name => $value]);
    }

    public function setInteger(string $name, int $value): void {
        $this->setValues([$name => $value]);
    }

    public function setFloat(string $name, float $value): void {
        $this->setValues([$name => $value]);
    }

    public function setBoolean(string $name, bool $value): void {
        $this->setValues([$name => $value]);
    }

    public function setArray(string $name, array $value): void {
        $this->setValues([$name => $value]);
    }
}

<?php
// ConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2025-01-18

namespace Index\Config;

use Stringable;

/**
 * Provides a common interface for configuration values.
 */
interface ConfigValueInfo extends Stringable {
    /**
     * Name of this configuration value.
     *
     * @var string
     */
    public string $name { get; }

    /**
     * Type of this configuration value.
     *
     * @var string
     */
    public string $type { get; }

    /**
     * Whether the value is a string.
     *
     * @var bool
     */
    public bool $isString { get; }

    /**
     * Whether the value is an integer.
     *
     * @var bool
     */
    public bool $isInt { get; }

    /**
     * Whether the value is a floating point number.
     *
     * @var bool
     */
    public bool $isFloat { get; }

    /**
     * Whether the value is a boolean.
     *
     * @var bool
     */
    public bool $isBool { get; }

    /**
     * Whether the value is an array.
     *
     * @var bool
     */
    public bool $isArray { get; }

    /**
     * Raw value without any type validation.
     *
     * @var mixed
     */
    public mixed $value { get; }

    /**
     * Ensures the value is a string and returns the value.
     *
     * @var string
     */
    public string $string { get; }

    /**
     * Ensures the value is an integer and returns the value.
     *
     * @var int
     */
    public int $int { get; }

    /**
     * Ensures the value is a floating point number and returns the value.
     *
     * @var float
     */
    public float $float { get; }

    /**
     * Ensures the value is a boolean and returns the value.
     *
     * @var bool
     */
    public bool $bool { get; }

    /**
     * Ensures the value is an array and returns the value.
     *
     * @var mixed[]
     */
    public array $array { get; }
}

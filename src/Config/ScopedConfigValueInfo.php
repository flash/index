<?php
// ScopedConfigValueInfo.php
// Created: 2023-10-20
// Updated: 2025-02-27

namespace Index\Config;

/**
 * Provides information about a scoped configuration value.
 */
class ScopedConfigValueInfo implements ConfigValueInfo {
    /**
     * @param ConfigValueInfo $info Base config value info instance.
     * @param int $prefixLength Length of the prefix.
     */
    public function __construct(
        private ConfigValueInfo $info,
        private int $prefixLength
    ) {}

    public string $name {
        get => substr($this->info->name, $this->prefixLength);
    }

    /**
     * Real name of the configuration value, without removing the prefix.
     *
     * @var string
     */
    public string $realName {
        get => $this->info->name;
    }

    public string $type { get => $this->info->type; }
    public mixed $value { get => $this->info->value; }

    public bool $isString { get => $this->info->isString; }
    public bool $isInt    { get => $this->info->isInt; }
    public bool $isFloat  { get => $this->info->isFloat; }
    public bool $isBool   { get => $this->info->isBool; }
    public bool $isArray  { get => $this->info->isArray; }

    public string $string { get => $this->info->string; }
    public int    $int    { get => $this->info->int; }
    public float  $float  { get => $this->info->float; }
    public bool   $bool   { get => $this->info->bool; }
    public array  $array  { get => $this->info->array; }

    public function __toString(): string {
        return (string)$this->info;
    }
}

<?php
// IntegerBaseConverter.php
// Created: 2024-07-31
// Updated: 2025-01-18

namespace Index;

use InvalidArgumentException;

/**
 * Provides methods for encoding integers in different bases.
 */
class IntegerBaseConverter {
    private int $base;

    /**
     * @param string $chars Specifies the provided character set, must be a string consisting out of at least two unique characters.
     * @throws InvalidArgumentException If $chars contains less than two characters.
     * @throws InvalidArgumentException If $chars contains duplicate characters.
     */
    public function __construct(
        private string $chars
    ) {
        $length = strlen($chars);
        if($length < 2)
            throw new InvalidArgumentException('$chars must contain at least two characters');
        if(XString::countUnique($chars) !== $length)
            throw new InvalidArgumentException('$chars must contain a set of entirely unique characters');

        $this->base = $length;
    }

    /**
     * Converts a positive base10 integer to another base.
     *
     * @param int $integer The integer to encode.
     * @return string The encoded data, as a string.
     */
    public function encode(int $integer): string {
        if($integer < 0)
            throw new InvalidArgumentException('$integer contains a negative value, which cannot be represented');
        if($integer === 0)
            return $this->chars[0];

        $output = '';

        while($integer > 0) {
            $remainder = $integer % $this->base;
            $output = $this->chars[$remainder] . $output;
            $integer = intdiv($integer, $this->base);
        }

        return $output;
    }

    /**
     * Converts another base encoded integer to a base10 integer.
     *
     * @param string $string The encoded integer.
     * @return int|false Returns the decoded integer or false on failure.
     */
    public function decode(string $string): int|false {
        if($string === '')
            return 0;

        $output = 0;

        for($i = 0; $i < strlen($string); ++$i) {
            $pos = strpos($this->chars, $string[$i]);
            if($pos === false)
                return false;

            $output = $output * $this->base + $pos;
        }

        return (int)$output;
    }
}

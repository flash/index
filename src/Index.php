<?php
// Index.php
// Created: 2021-04-30
// Updated: 2025-01-18

namespace Index;

/**
 * Provides information about the Index library.
 */
final class Index {
    public const string PATH_SOURCE = __DIR__;
    public const string PATH_ROOT = self::PATH_SOURCE . DIRECTORY_SEPARATOR . '..';
    public const string PATH_VERSION = self::PATH_ROOT . DIRECTORY_SEPARATOR . 'VERSION';

    /**
     * Gets the current version of the Index library.
     *
     * Reads the VERSION file in the root of the Index directory.
     * Returns 0.0.0 if reading the file failed for any reason.
     *
     * @return string Current version string.
     */
    public static function version(): string {
        if(!is_file(self::PATH_VERSION))
            return '0.0.0';

        $version = file_get_contents(self::PATH_VERSION);
        if($version === false)
            return '0.0.0';

        return trim($version);
    }
}

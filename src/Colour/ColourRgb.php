<?php
// ColourRgb.php
// Created: 2023-01-02
// Updated: 2025-01-18

namespace Index\Colour;

/**
 * Represents an RGB colour.
 */
class ColourRgb extends Colour {
    public private(set) bool $inherits = false;

    /**
     * @param int<0, 255> $red Red property.
     * @param int<0, 255> $green Green property.
     * @param int<0, 255> $blue Blue property.
     * @param float $alpha Alpha property.
     */
    public function __construct(
        public private(set) int $red,
        public private(set) int $green,
        public private(set) int $blue,
        public private(set) float $alpha = 1.0
    ) {
        $this->red = max(0, min(255, $red));
        $this->green = max(0, min(255, $green));
        $this->blue = max(0, min(255, $blue));
        $this->alpha = max(0.0, min(1.0, $alpha));
    }

    public function __toString(): string {
        if($this->alpha < 1.0) {
            $alpha = (string)round($this->alpha, 3);
            return sprintf('rgba(%d, %d, %d, %s)', $this->red, $this->green, $this->blue, $alpha);
        }
        return sprintf('#%02x%02x%02x', $this->red, $this->green, $this->blue);
    }

    /**
     * Create a ColourRgb instance from a raw RGB value.
     *
     * @param int $raw A raw RGB colour in 0xRRGGBB format.
     * @return ColourRgb An instance of ColourRgb.
     */
    public static function fromRawRgb(int $raw): ColourRgb {
        return new ColourRgb(
            (($raw >> 16) & 0xFF),
            (($raw >> 8) & 0xFF),
            ($raw & 0xFF),
            1.0
        );
    }

    /**
     * Create a ColourRgb instance from a raw ARGB value.
     *
     * @param int $raw A raw ARGB colour in 0xAARRGGBB format.
     * @return ColourRgb An instance of ColourRgb.
     */
    public static function fromRawArgb(int $raw): ColourRgb {
        return new ColourRgb(
            (($raw >> 16) & 0xFF),
            (($raw >> 8) & 0xFF),
            ($raw & 0xFF),
            (($raw >> 24) & 0xFF) / 255.0,
        );
    }

    /**
     * Create a ColourRgb instance from a raw RGBA value.
     *
     * @param int $raw A raw RGBA colour in 0xRRGGBBAA format.
     * @return ColourRgb An instance of ColourRgb.
     */
    public static function fromRawRgba(int $raw): ColourRgb {
        return new ColourRgb(
            (($raw >> 24) & 0xFF),
            (($raw >> 16) & 0xFF),
            (($raw >> 8) & 0xFF),
            ($raw & 0xFF) / 255.0,
        );
    }

    /**
     * Converts any other colour to a ColourRgb.
     *
     * @param Colour $colour Original colour.
     * @return ColourRgb Converted colour.
     */
    public static function convert(Colour $colour): ColourRgb {
        if($colour instanceof ColourRgb)
            return $colour;
        return new ColourRgb(
            $colour->red,
            $colour->green,
            $colour->blue,
            $colour->alpha
        );
    }
}

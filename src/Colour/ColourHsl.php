<?php
// ColourHsl.php
// Created: 2023-01-02
// Updated: 2025-01-18

namespace Index\Colour;

/**
 * Represents a colour using Hue, Saturation and Lightness.
 */
class ColourHsl extends Colour {
    public private(set) int $red;
    public private(set) int $green;
    public private(set) int $blue;

    public private(set) bool $inherits = false;

    /**
     * @param float $hue Hue property.
     * @param float $saturation Saturation property.
     * @param float $lightness Lightness property.
     * @param float $alpha Alpha property.
     */
    public function __construct(
        public private(set) float $hue,
        public private(set) float $saturation,
        public private(set) float $lightness,
        public private(set) float $alpha = 1.0
    ) {
        $saturation = max(0.0, min(1.0, $saturation));
        $lightness = max(0.0, min(1.0, $lightness));
        $alpha = max(0.0, min(1.0, $alpha));

        $c = (1 - abs(2 * $lightness - 1)) * $saturation;
        $x = $c * (1 - abs(fmod($hue / 60, 2) - 1));
        $m = $lightness - ($c / 2);

        $r = $g = $b = 0;

        if($hue < 60) {
            $r = $c;
            $g = $x;
        } elseif($hue < 120) {
            $r = $x;
            $g = $c;
        } elseif($hue < 180) {
            $g = $c;
            $b = $x;
        } elseif($hue < 240) {
            $g = $x;
            $b = $c;
        } elseif($hue < 300) {
            $r = $x;
            $b = $c;
        } else {
            $r = $c;
            $b = $x;
        }

        $this->red = max(0, min(255, (int)round(($r + $m) * 255, 0, PHP_ROUND_HALF_DOWN)));
        $this->green = max(0, min(255, (int)round(($g + $m) * 255, 0, PHP_ROUND_HALF_DOWN)));
        $this->blue = max(0, min(255, (int)round(($b + $m) * 255, 0, PHP_ROUND_HALF_DOWN)));
    }

    public function __toString(): string {
        $hue = (string)round($this->hue, 2);
        $sat = (string)round($this->saturation * 100);
        $lig = (string)round($this->lightness * 100);

        if($this->alpha < 1.0) {
            $alpha = (string)round($this->alpha, 3);
            return sprintf('hsla(%sdeg, %s%%, %s%%, %s)', $hue, $sat, $lig, $alpha);
        }

        return sprintf('hsl(%sdeg, %s%%, %s%%)', $hue, $sat, $lig);
    }

    /**
     * Converts any other colour to a ColourHsl.
     *
     * @param Colour $colour Original colour.
     * @return ColourHsl Converted colour.
     */
    public static function convert(Colour $colour): ColourHsl {
        if($colour instanceof ColourHsl)
            return $colour;

        $r = $colour->red / 255.0;
        $g = $colour->green / 255.0;
        $b = $colour->blue / 255.0;

        $max = max($r, $g, $b);
        $min = min($r, $g, $b);

        $h = $s = 0;
        $l = ($max + $min) / 2;
        $d = $max - $min;

        if($d <> 0) {
            $s = $d / (1 - abs(2 * $l - 1));

            if($max == $r) {
                $h = 60 * fmod((($g - $b) / $d), 6);
                if($b > $g)
                    $h += 360;
            } elseif($max == $g) {
                $h = 60 * (($b - $r) / $d + 2);
            } else {
                $h = 60 * (($r - $g) / $d + 4);
            }
        }

        return new ColourHsl(
            round($h, 3),
            round($s, 3),
            round($l, 3),
            $colour->alpha
        );
    }
}

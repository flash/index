<?php
// ColourNull.php
// Created: 2023-01-02
// Updated: 2025-01-18

namespace Index\Colour;

/**
 * Represents an empty colour.
 *
 * If possible, use Colour::empty() to get the global instance of this class.
 */
final class ColourNull extends Colour {
    public private(set) int $red = 0;
    public private(set) int $green = 0;
    public private(set) int $blue = 0;
    public private(set) float $alpha = 1.0;
    public private(set) bool $inherits = true;

    public function __toString(): string {
        return 'inherit';
    }
}

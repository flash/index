<?php
// Colour.php
// Created: 2023-01-02
// Updated: 2025-02-27

namespace Index\Colour;

use Stringable;
use Index\XNumber;

/**
 * Abstract class for representing colours.
 */
abstract class Colour implements Stringable {
    /**
     * Retrieves the Red RGB byte.
     *
     * @var int<0, 255>
     */
    abstract public int $red { get; }

    /**
     * Retrieves the Green RGB byte.
     *
     * @var int<0, 255>
     */
    abstract public int $green { get; }

    /**
     * Retrieves the Blue RGB byte.
     *
     * @var int<0, 255>
     */
    abstract public int $blue { get; }

    /**
     * Retrieves the alpha component.
     *
     * @return float A number ranging from 0.0 to 1.0.
     */
    abstract public float $alpha { get; }

    /**
     * Whether this colour should be ignored and another should be used instead.
     *
     * @var bool
     */
    abstract public bool $inherits { get; }

    /**
     * Returns this colour in a format CSS understands.
     *
     * @return string CSS compatible colour.
     */
    abstract public function __toString(): string;

    private const float READABILITY_THRESHOLD = 186.0;
    private const float LUMINANCE_WEIGHT_RED = .299;
    private const float LUMINANCE_WEIGHT_GREEN = .587;
    private const float LUMINANCE_WEIGHT_BLUE = .114;

    // luminance shit might need further review

    /**
     * Calculates the luminance value of this colour.
     *
     * @var float Luminance value for this colour.
     */
    public float $luminance {
        get => self::LUMINANCE_WEIGHT_RED   * $this->red
             + self::LUMINANCE_WEIGHT_GREEN * $this->green
             + self::LUMINANCE_WEIGHT_BLUE  * $this->blue;
    }

    /**
     * Checks whether this colour is on the brighter side of things.
     *
     * @var bool
     */
    public bool $light {
        get => $this->luminance > self::READABILITY_THRESHOLD;
    }

    /**
     * Checks whether this colour is on the darker side of things.
     *
     * @var bool
     */
    public bool $dark {
        get => $this->luminance <= self::READABILITY_THRESHOLD;
    }

    private static ColourNull $none;

    /**
     * Gets an empty colour.
     *
     * @return Colour A global instance of ColourNull.
     */
    public static function none(): Colour {
        return self::$none;
    }

    /**
     * Mixes two colours.
     * 0.0 -> Entirely $colour1
     * 0.5 -> Midpoint between $colour1 and $colour2
     * 1.0 -> Entirely $colour2
     *
     * @param Colour $colour1 Starting colour.
     * @param Colour $colour2 Ending colour.
     * @param float $weight Weight of $colour2.
     * @return Colour Mixed colour.
     */
    public static function mix(Colour $colour1, Colour $colour2, float $weight): Colour {
        $weight = min(1, max(0, $weight));

        if(XNumber::almostEquals($weight, 0.0))
            return $colour1;
        if(XNumber::almostEquals($weight, 1.0))
            return $colour2;
        if($colour1->inherits || $colour2->inherits)
            return self::none();

        return new ColourRgb(
            max(0, min(255, (int)round(XNumber::weighted($colour2->red, $colour1->red, $weight)))),
            max(0, min(255, (int)round(XNumber::weighted($colour2->green, $colour1->green, $weight)))),
            max(0, min(255, (int)round(XNumber::weighted($colour2->blue, $colour1->blue, $weight)))),
            XNumber::weighted($colour2->alpha, $colour1->alpha, $weight),
        );
    }

    private const int MSZ_INHERIT = 0x40000000;

    /**
     * Creates a Colour object from raw Misuzu format.
     *
     * If bit 31 is set the global instance of ColourNull will always be returned,
     *  otherwise an instance of ColourRgb will be created using the fromRawRgb method (top 8 bits ignored).
     *
     * @param int $raw Raw RGB colour in Misuzu format.
     * @return Colour Colour instance representing the Misuzu colour.
     */
    public static function fromMisuzu(int $raw): Colour {
        if($raw & self::MSZ_INHERIT)
            return self::$none;
        return ColourRgb::fromRawRgb($raw);
    }

    /**
     * Converts a Colour object to raw Misuzu format.
     *
     * If inherits is true, an integer with only bit 31 will be returned,
     *  otherwise a raw RGB value will be returned.
     *
     * @param Colour $colour Colour to be converted to raw Misuzu format.
     * @return int Raw Misuzu format colour.
     */
    public static function toMisuzu(Colour $colour): int {
        if($colour->inherits)
            return self::MSZ_INHERIT;
        return ($colour->red << 16) | ($colour->green << 8) | $colour->blue;
    }

    /**
     * Packs a colour into an RGB integer.
     *
     * @param Colour $colour Colour to pack.
     * @return int Packed RGB colour.
     */
    public static function toRawRgb(Colour $colour): int {
        return ($colour->red << 16) | ($colour->green << 8) | $colour->blue;
    }

    /**
     * Packs a colour into an ARGB integer.
     *
     * @param Colour $colour Colour to pack.
     * @return int Packed ARGB colour.
     */
    public static function toRawArgb(Colour $colour): int {
        $alpha = max(0, min(255, round($colour->alpha * 255)));
        return ($alpha << 24) | ($colour->red << 16) | ($colour->green << 8) | $colour->blue;
    }

    /**
     * Packs a colour into an RGBA integer.
     *
     * @param Colour $colour Colour to pack.
     * @return int Packed RGBA colour.
     */
    public static function toRawRgba(Colour $colour): int {
        $alpha = max(0, min(255, round($colour->alpha * 255)));
        return ($colour->red << 24) | ($colour->green << 16) | ($colour->blue << 8) | $alpha;
    }

    /**
     * Attempts to parse a CSS format colour.
     *
     * @param ?string $value CSS format colour.
     * @return Colour Parsed CSS colour.
     */
    public static function parse(?string $value): Colour {
        if($value === null)
            return self::$none;

        $value = strtolower(trim($value));
        if($value === '' || $value === 'inherit')
            return self::$none;

        if(ctype_alpha($value) && ColourNamed::isValidName($value))
            return new ColourNamed($value);

        if($value[0] === '#') {
            $value = substr($value, 1);

            if(ctype_xdigit($value)) {
                $length = strlen($value);

                if($length === 3) {
                    $value = $value[0] . $value[0] . $value[1] . $value[1] . $value[2] . $value[2];
                    $length *= 2;
                } elseif($length === 4) {
                    $value = $value[0] . $value[0] . $value[1] . $value[1] . $value[2] . $value[2] . $value[3] . $value[3];
                    $length *= 2;
                }

                if($length === 6)
                    return ColourRgb::fromRawRgb((int)hexdec($value));
                if($length === 8)
                    return ColourRgb::fromRawRgba((int)hexdec($value));
            }

            return self::$none;
        }

        if(str_starts_with($value, 'rgb(') || str_starts_with($value, 'rgba(')) {
            $open = strpos($value, '(');
            if($open === false)
                return self::$none;

            $close = strpos($value, ')', $open);
            if($close === false)
                return self::$none;

            $open += 1;
            $value = substr($value, $open, $close - $open);

            if(strpos($value, ',') === false) {
                // todo: support comma-less syntax
                return self::$none;
            } else {
                $value = explode(',', $value, 4);
                $parts = count($value);
                if($parts !== 3 && $parts !== 4)
                    return self::$none;

                $value = [
                    max(0, min(255, (int)trim($value[0]))),
                    max(0, min(255, (int)trim($value[1]))),
                    max(0, min(255, (int)trim($value[2]))),
                    (float)trim($value[3] ?? '1'),
                ];
            }

            return new ColourRgb(...$value);
        }

        if(str_starts_with($value, 'hsl(') || str_starts_with($value, 'hsla(')) {
            $open = strpos($value, '(');
            if($open === false)
                return self::$none;

            $close = strpos($value, ')', $open);
            if($close === false)
                return self::$none;

            $open += 1;
            $value = substr($value, $open, $close - $open);

            if(strpos($value, ',') === false) {
                // todo: support comma-less syntax
                return self::$none;
            } else {
                $value = explode(',', $value, 4);
                $parts = count($value);
                if($parts !== 3 && $parts !== 4)
                    return self::$none;

                for($i = 0; $i < $parts; ++$i)
                    $value[$i] = trim($value[$i]);

                $hue = $value[0];
                $saturation = $value[1];
                $lightness = $value[2];
                $alpha = $value[3] ?? null;

                if(str_ends_with($saturation, '%'))
                    $saturation = substr($saturation, 0, -1);

                if(str_ends_with($lightness, '%'))
                    $lightness = substr($lightness, 0, -1);

                $saturation = (float)$saturation;
                $lightness = (float)$lightness;

                if($saturation < 0 || $saturation > 100 || $lightness < 0 || $lightness > 100)
                    return self::$none;

                $saturation /= 100.0;
                $lightness /= 100.0;

                if(ctype_digit($hue)) {
                    $hue = (float)$hue;
                } else {
                    if(str_ends_with($hue, 'deg')) {
                        $hue = (float)substr($hue, 0, -3);
                    } elseif(str_ends_with($hue, 'grad')) {
                        $hue = 0.9 * (float)substr($hue, 0, -4);
                    } elseif(str_ends_with($hue, 'rad')) {
                        $hue = round(rad2deg((float)substr($hue, 0, -3)));
                    } elseif(str_ends_with($hue, 'turn')) {
                        $hue = 360.0 * ((float)substr($hue, 0, -4));
                    } else {
                        return self::$none;
                    }
                }

                $alpha = (float)trim($alpha ?? '1');
            }

            return new ColourHsl($hue, $saturation, $lightness, $alpha);
        }

        return self::$none;
    }

    /**
     * Initialises the Colour class.
     */
    public static function init(): void {
        self::$none = new ColourNull;
    }
}

Colour::init();

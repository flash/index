<?php
// Equatable.php
// Created: 2021-04-26
// Updated: 2025-01-18

namespace Index;

/**
 * Provides an interface for determining the value-equality of two objects.
 */
interface Equatable {
    /**
     * Checks whether the current object is equal to another.
     *
     * @return bool true if the objects are equals, false if not.
     */
    public function equals(mixed $other): bool;
}

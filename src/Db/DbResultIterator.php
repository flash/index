<?php
// DbResultIterator.php
// Created: 2024-02-06
// Updated: 2024-10-04

namespace Index\Db;

use InvalidArgumentException;
use Iterator;

/**
 * Implements an iterator and constructor wrapper for DbResult.
 *
 * @implements Iterator<int, object>
 */
class DbResultIterator implements Iterator {
    private bool $wasValid = false;
    private object $current;

    /**
     * Call this through an DbResult instance instead!
     *
     * @param DbResult $result Result to operate on.
     * @param callable(DbResult): object $construct Constructor callback.
     */
    public function __construct(
        private DbResult $result,
        private $construct
    ) {
        if(!is_callable($construct))
            throw new InvalidArgumentException('$construct must be a callable.');

        $this->current = (object)[];
    }

    public function current(): mixed {
        return $this->current;
    }

    public function key(): mixed {
        return spl_object_id($this->current);
    }

    private function moveNext(): void {
        if($this->wasValid = $this->result->next())
            $this->current = ($this->construct)($this->result);
    }

    public function next(): void {
        $this->moveNext();
    }

    public function rewind(): void {
        $this->moveNext();
    }

    public function valid(): bool {
        return $this->wasValid;
    }
}

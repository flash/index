<?php
// SqliteTransaction.php
// Created: 2024-10-19
// Updated: 2024-10-19

namespace Index\Db\Sqlite;

use SQLite3;
use Index\Db\DbTransaction;

/**
 * Represents an SQLite transaction.
 */
class SqliteTransaction implements DbTransaction {
    /**
     * @param SqliteConnection $conn Underlying connection.
     */
    public function __construct(
        private SqliteConnection $conn
    ) {}

    public function commit(): void {
        $this->conn->executeThrow('COMMIT;');
    }

    public function save(string $name): void {
        $this->conn->executeThrow(sprintf('SAVEPOINT %s;', SQLite3::escapeString($name)));
    }

    public function release(string $name): void {
        $this->conn->executeThrow(sprintf('RELEASE SAVEPOINT %s;', SQLite3::escapeString($name)));
    }

    public function rollback(?string $name = null): void {
        $this->conn->executeThrow(
            $name === null
                ? 'ROLLBACK;'
                : sprintf('ROLLBACK TO SAVEPOINT %s;', SQLite3::escapeString($name))
        );
    }
}

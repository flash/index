<?php
// SqliteStatement.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\Sqlite;

use SQLite3Result;
use SQLite3Stmt;
use InvalidArgumentException;
use RuntimeException;
use Index\Db\{DbStatement,DbStatementCommon,DbTools,DbType,DbResult};

/**
 * Represents a prepared SQLite SQL statement.
 */
class SqliteStatement implements DbStatement {
    use DbStatementCommon;

    private ?SqliteResult $result = null;
    private int $lastOrdinal = 1;

    /**
     * Creates a new SqliteStatement instance.
     *
     * @param SqliteConnection $connection A reference to the connection which creates this statement.
     * @param SQLite3Stmt $statement The raw statement instance.
     * @return SqliteStatement A new statement instance.
     */
    public function __construct(
        private SqliteConnection $connection,
        private SQLite3Stmt $statement
    ) {}

    public int $paramCount {
        get => $this->statement->paramCount();
    }

    private static function convertType(DbType $type): int {
        return match($type) {
            DbType::Null => SQLITE3_NULL,
            DbType::Int => SQLITE3_INTEGER,
            DbType::Float => SQLITE3_FLOAT,
            DbType::String => SQLITE3_TEXT,
            DbType::Blob => SQLITE3_BLOB,
            default => throw new InvalidArgumentException('$type is not a supported type.'),
        };
    }

    private function bindParameter(int $ordinal, mixed $value, DbType $type): void {
        if($type === DbType::Auto)
            $type = DbTools::detectType($value);
        if($type === DbType::Null)
            $value = null;

        if(!$this->statement->bindValue($ordinal, $value, self::convertType($type)))
            throw new RuntimeException($this->connection->lastErrorString, $this->connection->lastErrorCode);
    }

    public function addParameter(int $ordinal, mixed $value, DbType $type = DbType::Auto): void {
        if($ordinal < 1 || $ordinal > $this->paramCount)
            throw new InvalidArgumentException('$ordinal is not a valid parameter number.');

        if($ordinal > $this->lastOrdinal)
            $this->lastOrdinal = $ordinal;

        $this->bindParameter($ordinal, $value, $type);
    }

    public function nextParameter(mixed $value, DbType $type = DbType::Auto): void {
        if($this->lastOrdinal > $this->paramCount)
            throw new RuntimeException('Attempted to specify too many parameters.');

        $this->bindParameter($this->lastOrdinal++, $value, $type);
    }

    public int|string $lastInsertId {
        get => $this->connection->lastInsertId;
    }

    public function getResult(): SqliteResult {
        if($this->result === null)
            throw new RuntimeException('no result available');

        return $this->result;
    }

    public function execute(): int|string {
        $result = $this->statement->execute();
        if($result === false)
            throw new RuntimeException($this->connection->lastErrorString, $this->connection->lastErrorCode);
        $this->result = new SqliteResult($result);

        return $this->connection->affectedRows;
    }

    public function reset(): void {
        $this->result = null;
        $this->lastOrdinal = 1;
        if(!$this->statement->clear())
            throw new RuntimeException($this->connection->lastErrorString, $this->connection->lastErrorCode);
        if(!$this->statement->reset())
            throw new RuntimeException($this->connection->lastErrorString, $this->connection->lastErrorCode);
    }
}

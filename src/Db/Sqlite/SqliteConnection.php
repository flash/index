<?php
// SqliteConnection.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\Sqlite;

use RuntimeException;
use SQLite3;
use Index\Db\{DbConnection,DbResult,DbStatement,DbTransaction};

/**
 * Represents a client for an SQLite database.
 */
class SqliteConnection implements DbConnection {
    /**
     * CREATE INDEX authorizer.
     *
     * Second parameter will be the name of the index.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int CREATE_INDEX = SQLite3::CREATE_INDEX;

    /**
     * CREATE TABLE authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int CREATE_TABLE = SQLite3::CREATE_TABLE;

    /**
     * CREATE TEMP INDEX authorizer.
     *
     * Second parameter will be the name of the index.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int CREATE_TEMP_INDEX = SQLite3::CREATE_TEMP_INDEX;

    /**
     * CREATE TEMP TABLE authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int CREATE_TEMP_TABLE = SQLite3::CREATE_TEMP_TABLE;

    /**
     * CREATE TEMP TRIGGER authorizer.
     *
     * Second parameter will be the name of the trigger.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int CREATE_TEMP_TRIGGER = SQLite3::CREATE_TEMP_TRIGGER;

    /**
     * CREATE TEMP VIEW authorizer.
     *
     * Second parameter will be the name of the view.
     *
     * @var int
     */
    public const int CREATE_TEMP_VIEW = SQLite3::CREATE_TEMP_VIEW;

    /**
     * CREATE TRIGGER authorizer.
     *
     * Second parameter will be the name of the trigger.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int CREATE_TRIGGER = SQLite3::CREATE_TRIGGER;

    /**
     * CREATE VIEW authorizer.
     *
     * Second parameter will be the name of the view.
     *
     * @var int
     */
    public const int CREATE_VIEW = SQLite3::CREATE_VIEW;

    /**
     * DELETE authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int DELETE = SQLite3::DELETE;

    /**
     * DROP INDEX authorizer.
     *
     * Second parameter will be the name of the index.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int DROP_INDEX = SQLite3::DROP_INDEX;

    /**
     * DROP TABLE authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int DROP_TABLE = SQLite3::DROP_TABLE;

    /**
     * DROP TEMP INDEX authorizer.
     *
     * Second parameter will be the name of the index.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int DROP_TEMP_INDEX = SQLite3::DROP_TEMP_INDEX;

    /**
     * DROP TEMP TABLE authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int DROP_TEMP_TABLE = SQLite3::DROP_TEMP_TABLE;

    /**
     * DROP TEMP TRIGGER authorizer.
     *
     * Second parameter will be the name of the trigger.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int DROP_TEMP_TRIGGER = SQLite3::DROP_TEMP_TRIGGER;

    /**
     * DROP TEMP VIEW authorizer.
     *
     * Second parameter will be the name of the view.
     *
     * @var int
     */
    public const int DROP_TEMP_VIEW = SQLite3::DROP_TEMP_VIEW;

    /**
     * DROP TRIGGER authorizer.
     *
     * Second parameter will be the name of the trigger.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int DROP_TRIGGER = SQLite3::DROP_TRIGGER;

    /**
     * DROP VIEW authorizer.
     *
     * Second parameter will be the name of the view.
     *
     * @var int
     */
    public const int DROP_VIEW = SQLite3::DROP_VIEW;

    /**
     * INSERT authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int INSERT = SQLite3::INSERT;

    /**
     * PRAGMA authorizer.
     *
     * Second parameter will be the name of the pragma.
     * Third parameter may be the first argument passed to the pragma.
     *
     * @var int
     */
    public const int PRAGMA = SQLite3::PRAGMA;

    /**
     * READ authorizer.
     *
     * Second parameter will be the name of the table.
     * Third parameter will be the name of the column.
     *
     * @var int
     */
    public const int READ = SQLite3::READ;

    /**
     * SELECT authorizer.
     *
     * @var int
     */
    public const int SELECT = SQLite3::SELECT;

    /**
     * TRANSACTION authorizer.
     *
     * Second parameter will be the operation.
     *
     * @var int
     */
    public const int TRANSACTION = SQLite3::TRANSACTION;

    /**
     * UPDATE authorizer.
     *
     * Second parameter will be the name of the table.
     * Third parameter will be the name of the column.
     *
     * @var int
     */
    public const int UPDATE = SQLite3::UPDATE;

    /**
     * ATTACH authorizer.
     *
     * Second parameter will be the filename.
     *
     * @var int
     */
    public const int ATTACH = SQLite3::ATTACH;

    /**
     * DETACH authorizer.
     *
     * Second parameter will be the name of the database.
     *
     * @var int
     */
    public const int DETACH = SQLite3::DETACH;

    /**
     * ALTER TABLE authorizer.
     *
     * Second parameter will be the name of the database.
     * Third parameter will be the name of the table.
     *
     * @var int
     */
    public const int ALTER_TABLE = SQLite3::ALTER_TABLE;

    /**
     * REINDEX authorizer.
     *
     * Second parameter will be the name of the index.
     *
     * @var int
     */
    public const int REINDEX = SQLite3::REINDEX;

    /**
     * ANALYZE authorizer.
     *
     * Second parameter will be the name of the table.
     *
     * @var int
     */
    public const int ANALYZE = SQLite3::ANALYZE;

    /**
     * CREATE VTABLE authorizer.
     *
     * Second parameter will be the name of the table.
     * Third parameter will be the name of the module.
     *
     * @var int
     */
    public const int CREATE_VTABLE = SQLite3::CREATE_VTABLE;

    /**
     * DROP VTABLE authorizer.
     *
     * Second parameter will be the name of the table.
     * Third parameter will be the name of the module.
     *
     * @var int
     */
    public const int DROP_VTABLE = SQLite3::DROP_VTABLE;

    /**
     * FUNCTION authorizer.
     *
     * Third parameter will be the name of the function.
     *
     * @var int
     */
    public const int FUNCTION = SQLite3::FUNCTION;

    /**
     * SAVEPOINT authorizer.
     *
     * Second parameter will be the operation.
     * Third parameter will be the name of the savepoint.
     *
     * @var int
     */
    public const int SAVEPOINT = SQLite3::SAVEPOINT;

    /**
     * RECURSIVE authorizer.
     *
     * @var int
     */
    public const int RECURSIVE = SQLite3::RECURSIVE;

    /**
     * DETERMINISTIC flag for SQLite registered functions.
     *
     * When specified the function will always return the same result given the same inputs within a single SQL statement.
     *
     * @var int
     */
    public const int DETERMINISTIC = SQLITE3_DETERMINISTIC;

    /**
     * OK authorization status.
     *
     * @var int
     */
    public const int OK = SQLite3::OK;

    /**
     * DENY authorization status.
     *
     * @var int
     */
    public const int DENY = SQLite3::DENY;

    /**
     * IGNORE authorization status.
     *
     * @var int
     */
    public const int IGNORE = SQLite3::IGNORE;

    private SQLite3 $connection;

    /**
     * Creates a new SQLite client.
     *
     * @param SqliteConnectionInfo $connectionInfo Information about the client.
     * @return SqliteConnection A new SQLite client.
     */
    public function __construct(SqliteConnectionInfo $connectionInfo) {
        $flags = 0;
        if($connectionInfo->readOnly)
            $flags |= SQLITE3_OPEN_READONLY;
        else
            $flags |= SQLITE3_OPEN_READWRITE;

        if($connectionInfo->create)
            $flags |= SQLITE3_OPEN_CREATE;

        $this->connection = new SQLite3(
            $connectionInfo->fileName,
            $flags,
            $connectionInfo->encryptionKey
        );
        $this->connection->enableExceptions(true);
    }

    public int|string $affectedRows {
        get => $this->connection->changes();
    }

    /**
     * Registers a PHP function for use as an SQL aggregate function.
     *
     * @see https://www.php.net/manual/en/sqlite3.createaggregate.php
     * @param string $name Name of the SQL aggregate.
     * @param callable $stepCallback Callback function to be called for each row of the result set.
     *                               Definition: step(mixed $context, int $rowNumber, mixed $value, mixed ...$values): mixed
     * @param callable $finalCallback Callback function to be called to finalize the aggregate operation and return the value.
     *                                Definition: fini(mixed $content, int $rowNumber): mixed
     * @param int $argCount Number of arguments this aggregate function takes, -1 for any amount.
     * @return bool true if the aggregate function was created successfully, false if not.
     */
    public function createAggregate(string $name, callable $stepCallback, callable $finalCallback, int $argCount = -1): bool {
        return $this->connection->createAggregate($name, $stepCallback, $finalCallback, $argCount);
    }

    /**
     * Registers a PHP function for use as an SQL collating function.
     *
     * @see https://www.php.net/manual/en/sqlite3.createcollation.php
     * @param string $name Name of the SQL collating function.
     * @param callable $callback Callback function that returns -1, 0 or 1 to indicate sort order.
     *                           Definition: collation(mixed $value1, mixed $value2): int
     * @return bool true if the collating function was registered, false if not.
     */
    public function createCollation(string $name, callable $callback): bool {
        return $this->connection->createCollation($name, $callback);
    }

    /**
     * Register a PHP function for use as an SQL scalar function.
     *
     * @see https://www.php.net/manual/en/sqlite3.createfunction.php
     * @param string $name Name of the SQL function.
     * @param callable $callback Callback function to register.
     *                           Definition: callback(mixed $value, mixed ...$values): mixed
     * @param int $argCount Number of arguments this aggregate function takes, -1 for any amount.
     * @param int $flags A bitset of flags.
     */
    public function createFunction(string $name, callable $callback, int $argCount = -1, int $flags = 0): bool {
        return $this->connection->createFunction($name, $callback, $argCount, $flags);
    }

    /**
     * Sets a callback to be used as an authorizer to limit what statements can do.
     *
     * @param callable|null $callback An authorizer callback or null to remove the previous authorizer.
     *                                Definition: authorizer(int $actionCode, mixed $param2, mixed $param3, string $database, mixed $trigger): int
     *                                $param2 and $param3 differ based on $actionCode.
     *                                Must return one of the OK, DENY or IGNORE constants.
     * @return bool true if the action was successful, false if not.
     */
    public function setAuthorizer(callable|null $callback): bool {
        return $this->connection->setAuthorizer($callback);
    }

    /**
     * Last error string.
     *
     * @var string
     */
    public string $lastErrorString {
        get => $this->connection->lastErrorMsg();
    }

    /**
     * Last error code.
     *
     * @var int
     */
    public int $lastErrorCode {
        get => $this->connection->lastErrorCode();
    }

    /**
     * Throws the last error as a RuntimeException.
     *
     * @throws RuntimeException Based on the last error code and string.
     */
    public function throwLastError(): never {
        throw new RuntimeException($this->lastErrorString, $this->lastErrorCode);
    }

    /**
     * Opens a BLOB field as a resource.
     *
     * @param string $table Name of the source table.
     * @param string $column Name of the source column.
     * @param int $rowId ID of the source row.
     * @throws RuntimeException If opening the BLOB failed.
     * @return resource BLOB field as a file resource.
     */
    public function getBlobStream(string $table, string $column, int $rowId): mixed {
        $handle = $this->connection->openBlob($table, $column, $rowId);
        if(!is_resource($handle))
            $this->throwLastError();

        return $handle;
    }

    public int|string $lastInsertId {
        get => $this->connection->lastInsertRowID();
    }

    public function prepare(string $query): DbStatement {
        $statement = $this->connection->prepare($query);
        if($statement === false)
            $this->throwLastError();

        return new SqliteStatement($this, $statement);
    }

    public function query(string $query): DbResult {
        $result = $this->connection->query($query);
        if($result === false)
            $this->throwLastError();

        return new SqliteResult($result);
    }

    /**
     * Tries to execute a query but throws an exception if it fails.
     *
     * @param string $query Command to execute.
     * @throws RuntimeException If the command failed to execute.
     */
    public function executeThrow(string $query): void {
        if(!$this->connection->exec($query))
            $this->throwLastError();
    }

    public function execute(string $query): int|string {
        $this->executeThrow($query);
        return $this->connection->changes();
    }

    public function beginTransaction(): DbTransaction {
        $this->executeThrow('BEGIN;');
        return new SqliteTransaction($this);
    }

    public function __destruct() {
        $this->connection->close();
    }
}

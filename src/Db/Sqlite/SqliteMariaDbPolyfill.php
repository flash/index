<?php
// SqliteMariaDbPolyfill.php
// Created: 2024-10-21
// Updated: 2024-12-02

namespace Index\Db\Sqlite;

use DateTimeImmutable;
use RuntimeException;
use Random\Randomizer;
use Random\Engine\Mt19937;

/**
 * Provides implementations of MariaDB functions for use in SQLite.
 */
final class SqliteMariaDbPolyfill {
    /**
     * @throws RuntimeException Will always throw because this is a static class exclusively.
     */
    public function __construct() {
        throw new RuntimeException('This is a static class, you cannot create an instance of it.');
    }

    /**
     * Adds all functions to a given SQLite connection.
     *
     * @param SqliteConnection $conn Connection to add functions to.
     */
    public static function apply(SqliteConnection $conn): void {
        $conn->createFunction('IF', self::mdbIf(...));
        $conn->createFunction('NOW', self::mdbNow(...));
        $conn->createFunction('RAND', self::mdbRand(...));
        $conn->createFunction('UNIX_TIMESTAMP', self::mdbUnixTimestamp(...));
        $conn->createFunction('FROM_UNIXTIME', self::mdbFromUnixtime(...));
        $conn->createFunction('INET6_NTOA', self::mdbInet6ntoa(...));
        $conn->createFunction('INET6_ATON', self::mdbInet6aton(...));
        $conn->createFunction('UNHEX', self::mdbUnhex(...));
    }

    /**
     * MariaDB compatible IF function.
     *
     * @see https://mariadb.com/kb/en/if-function/
     * @param mixed $condition Condition that was tested.
     * @param mixed $true Return value if true ($condition <> 0 and $condition <> null)
     * @param mixed $false Return value if false.
     * @return mixed Either $true or $false.
     */
    public static function mdbIf(mixed $condition, mixed $true, mixed $false): mixed {
        return $condition ? $true : $false;
    }

    /**
     * Returns a number in the range 0 <= v < 1.0.
     *
     * @see https://mariadb.com/kb/en/rand/
     * @param mixed $seed Seed value.
     * @return float Random number.
     */
    public static function mdbRand(mixed $seed = null): float {
        return (new Randomizer(new Mt19937(is_scalar($seed) ? (int)$seed : null)))->nextFloat();
    }

    /**
     * MariaDB compatible NOW/CURRENT_TIMESTAMP function.
     *
     * Returns current date/time in YYYY-MM-DD HH:MM:SS[.uuuuuu] format.
     * Does not support numeric mode.
     *
     * @see https://mariadb.com/kb/en/now/
     * @param mixed $precision Millisecond precision.
     * @return string Date.
     */
    public static function mdbNow(mixed $precision = null): mixed {
        $dt = new DateTimeImmutable('now');
        $precision = is_scalar($precision) ? min(6, max(0, (int)$precision)) : 0;

        $ts = $dt->format('Y-m-d H:i:s');
        if($precision > 0)
            $ts .= '.' . substr($dt->format('u'), 0, $precision);

        return $ts;
    }

    /**
     * Returns the current UNIX timestamp or gets it from a date/time string.
     *
     * @see https://mariadb.com/kb/en/unix_timestamp/
     * @param mixed $arg Null or a scalar value.
     * @return int|float Timestamp.
     */
    public static function mdbUnixTimestamp(mixed $arg = null): int|float {
        if(is_scalar($arg)) {
            $dt = new DateTimeImmutable((string)$arg);
            $ts = $dt->format('U');
            $ms = $dt->format('u');

            if(trim($ms, '0') !== '')
                return (float)sprintf('%d.%d', $ts, $ms);

            return (int)$ts;
        }

        return time();
    }

    /**
     * Returns the current UNIX timestamp or gets it from a date/time string.
     *
     * @see https://mariadb.com/kb/en/from_unixtime/
     * @param mixed $ts Timestamp.
     * @param mixed $format Format string like in the specified MariaDB config.
     * @return ?string Formatted timestamp.
     */
    public static function mdbFromUnixtime(mixed $ts, mixed $format = null): ?string {
        if(!is_scalar($ts))
            return null;

        $ts = (float)$ts;
        $hasMs = abs($ts - (int)$ts) > 0;
        $ts = new DateTimeImmutable(sprintf('@%F', $ts));

        if(is_scalar($format)) {
            $format = strtr((string)$format, [
                '%a' => 'D',
                '%b' => 'M',
                '%c' => 'n',
                '%D' => 'jS',
                '%d' => 'd',
                '%e' => 'j',
                '%f' => 'u',
                '%H' => 'H',
                '%h' => 'h',
                '%I' => 'H',
                '%i' => 'i',
                '%j' => 'z', // uh-oh this should be +1
                '%k' => 'G',
                '%l' => 'g',
                '%M' => 'F',
                '%m' => 'm',
                '%p' => 'A',
                '%r' => 'g:i:s A',
                '%S' => 's',
                '%s' => 's',
                '%T' => 'H:i:s',
                '%U' => 'W', // whoops these are also all approximations
                '%u' => 'W', // ^
                '%V' => 'W', // ^
                '%v' => 'W', // ^
                '%W' => 'l',
                '%w' => 'w',
                '%X' => 'Y', // also wrong
                '%x' => 'Y', // ^
                '%Y' => 'Y',
                '%y' => 'y',
                '%#' => '#', // unsupported
                '%.' => '.', // ^
                '%@' => '@', // ^
                '%%' => '%',
            ]);
        } else {
            $format = 'Y-m-d H:i:s';
            if($hasMs)
                $format .= '.u';
        }

        return $ts->format($format);
    }

    /**
     * Converts a binary IPv4 or IPv6 address and converts it to a human readable representation.
     *
     * @see https://mariadb.com/kb/en/inet6_ntoa/
     * @param mixed $addr Binary IP address.
     * @return ?string Formatted IP address or NULL if the input could not be understood.
     */
    public static function mdbInet6ntoa(mixed $addr): ?string {
        if(!is_scalar($addr))
            return null;

        $addr = inet_ntop((string)$addr);
        if($addr === false)
            return null;

        return $addr;
    }

    /**
     * Converts a human readable IPv4 or IPv6 address and converts it to a binary representation.
     *
     * @see https://mariadb.com/kb/en/inet6_aton/
     * @param mixed $addr Human readable IP address.
     * @return ?string Binary IP address or NULL if the input was not understood.
     */
    public static function mdbInet6aton(mixed $addr): ?string {
        if(!is_scalar($addr))
            return null;

        $addr = inet_pton((string)$addr);
        if($addr === false)
            return null;

        return $addr;
    }

    /**
     * Unhex function.
     *
     * HEX() is defined but UNHEX() isn't???
     *
     * @see https://mariadb.com/kb/en/unhex/
     * @param mixed $str Hex string.
     * @return ?string Binary value.
     */
    public static function mdbUnhex(mixed $str): ?string {
        if(!is_scalar($str))
            return null;

        $str = hex2bin((string)$str);
        if($str === false)
            return '';

        return $str;
    }
}

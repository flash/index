<?php
// SqliteConnectionInfo.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db\Sqlite;

use Index\Db\DbConnectionInfo;

/**
 * Represents information about a SQLite Client.
 */
class SqliteConnectionInfo implements DbConnectionInfo {
    /**
     * Creates a new SqliteConnectionInfo instance.
     *
     * @param string $fileName Path to the SQLite database.
     * @param string $encryptionKey Key to encrypt the database with.
     * @param bool $readOnly Set to true if the database should be opened read-only.
     * @param bool $create Set to true to create the database if it does not exist.
     * @return SqliteConnectionInfo Information to create an SQLite database client.
     */
    public function __construct(
        public private(set) string $fileName,
        #[\SensitiveParameter] public private(set) string $encryptionKey,
        public private(set) bool $readOnly,
        public private(set) bool $create
    ) {}

    /**
     * Creates a connection info instance with a path.
     *
     * @param string $path Path to the SQLite database.
     * @param string $encryptionKey Key to encrypt the database with.
     * @param bool $readOnly Set to true if the database should be opened read-only.
     * @param bool $create Set to true to create the database if it does not exist.
     * @return SqliteConnectionInfo Information to create an SQLite database client.
     */
    public static function createPath(
        string $path,
        #[\SensitiveParameter] string $encryptionKey = '',
        bool $readOnly = false,
        bool $create = true
    ): SqliteConnectionInfo {
        return new SqliteConnectionInfo(
            $path,
            $encryptionKey,
            $readOnly,
            $create
        );
    }

    /**
     * Creates a connection info instance for an in-memory database.
     *
     * @param string $encryptionKey Key to encrypt the database with.
     * @return SqliteConnectionInfo Information to create an SQLite database client.
     */
    public static function createMemory(#[\SensitiveParameter] string $encryptionKey = ''): SqliteConnectionInfo {
        return new SqliteConnectionInfo(':memory:', $encryptionKey, false, true);
    }

    /**
     * Creates a connection info instance for a database stored in a temporary file.
     *
     * @param string $encryptionKey Key to encrypt the database with.
     * @return SqliteConnectionInfo Information to create an SQLite database client.
     */
    public static function createTemp(#[\SensitiveParameter] string $encryptionKey = ''): SqliteConnectionInfo {
        return new SqliteConnectionInfo('', $encryptionKey, false, true);
    }
}

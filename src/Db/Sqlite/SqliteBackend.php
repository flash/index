<?php
// SqliteBackend.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\Sqlite;

use SQLite3;
use InvalidArgumentException;
use Index\Db\{DbBackend,DbConnection,DbConnectionInfo};

/**
 * Information about the SQLite 3 database layer.
 */
class SqliteBackend implements DbBackend {
    public bool $available {
        get => extension_loaded('sqlite3');
    }

    /**
     * Version of the underlying library.
     *
     * @var string
     */
    public string $version {
        get {
            $version = SQLite3::version();
            return isset($version['versionString']) && is_scalar($version['versionString'])
                ? (string)$version['versionString'] : '';
        }
    }

    /**
     * Creates a new SQLite client.
     *
     * @param SqliteConnectionInfo $connectionInfo Object that describes the desired connection.
     * @return SqliteConnection A client for an SQLite database.
     */
    public function createConnection(DbConnectionInfo $connectionInfo): DbConnection {
        if(!($connectionInfo instanceof SqliteConnectionInfo))
            throw new InvalidArgumentException('$connectionInfo must by of type SqliteConnectionInfo');

        return new SqliteConnection($connectionInfo);
    }

    /**
     * Constructs a connection info instance from a dsn.
     *
     * SQLite DSNs have no username, password, host or port parts.
     *
     * The path can be set to any location on the filesystem to specify what file to use, or the special `:memory:` value to use an in-memory database.
     *
     * Supported query parameters are:
     *  - `key=<string>` to specify an encryption key.
     *  - `readOnly` to open a database in read-only mode.
     *  - `openOnly` to prevent a new file from being created if the specified path does not exist.
     *
     * @param string|array<string, int|string> $dsn DSN with connection information.
     * @return SqliteConnectionInfo SQLite connection info.
     */
    public function parseDsn(string|array $dsn): DbConnectionInfo {
        if(is_string($dsn)) {
            $dsn = parse_url($dsn);
            if($dsn === false)
                throw new InvalidArgumentException('$dsn is not a valid uri.');
        }

        $encKey = '';

        $path = (string)($dsn['path'] ?? '');

        if($path === 'memory:')
            $path = ':memory:';

        parse_str(str_replace('+', '%2B', (string)($dsn['query'] ?? '')), $query);

        $encKey = $query['key'] ?? '';
        if(!is_string($encKey)) $encKey = '';

        $readOnly = isset($query['readOnly']);
        $create = !isset($query['openOnly']);

        return new SqliteConnectionInfo($path, $encKey, $readOnly, $create);
    }
}

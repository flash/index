<?php
// SqliteResult.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\Sqlite;

use SQLite3Result;
use InvalidArgumentException;
use Index\Db\{DbResult,DbResultCommon};

/**
 * Represents an SQLite result set.
 */
class SqliteResult implements DbResult {
    use DbResultCommon;

    /** @var array<int|string, mixed> */
    private array $currentRow = [];

    /**
     * Creates a new instance of SqliteResult.
     *
     * @param SQLite3Result $result Raw underlying result class.
     * @return SqliteResult A new SqliteResult instance.
     */
    public function __construct(
        private SQLite3Result $result
    ) {}

    /**
     * Number of columns per row in the result.
     *
     * @var int|string
     */
    public int|string $fieldCount {
        get => $this->result->numColumns();
    }

    public function next(): bool {
        $result = $this->result->fetchArray(SQLITE3_BOTH);
        if($result === false)
            return false;
        $this->currentRow = $result;
        return true;
    }

    public function isNull(int|string $index): bool {
        return array_key_exists($index, $this->currentRow) && $this->currentRow[$index] === null;
    }

    public function getValue(int|string $index): mixed {
        return $this->currentRow[$index] ?? null;
    }
}

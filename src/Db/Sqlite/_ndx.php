<?php
// _ndx.php
// Created: 2024-10-18
// Updated: 2024-10-18

namespace Index\Db\Sqlite;

use Index\Db\DbBackends;

DbBackends::register('sqlite', SqliteBackend::class);
DbBackends::register('sqlite3', SqliteBackend::class);

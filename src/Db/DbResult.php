<?php
// DbResult.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db;

/**
 * Represents a database result set.
 */
interface DbResult {
    /**
     * Fetches the next result set.
     *
     * @return bool true if the result set was loaded, false if no more results are available.
     */
    public function next(): bool;

    /**
     * Checks if a given index has a NULL value.
     *
     * @param int|string $index Target index.
     * @return bool true if the value is null, false if not.
     */
    public function isNull(int|string $index): bool;

    /**
     * Gets the value from the target index without any additional casting.
     *
     * @param int|string $index Target index.
     * @return mixed Target value.
     */
    public function getValue(int|string $index): mixed;

    /**
     * Gets the value from the target index cast as a native string.
     *
     * @param int|string $index Target index.
     * @return string Returns a string of the value.
     */
    public function getString(int|string $index): string;

    /**
     * Checks if the field is null, then gets the value from the target index cast as a native string.
     *
     * @param int|string $index Target index.
     * @return ?string Returns a string of the value or null.
     */
    public function getStringOrNull(int|string $index): ?string;

    /**
     * Gets the value from the target index cast as an integer.
     *
     * @param int|string $index Target index.
     * @return int Returns the value cast to an integer.
     */
    public function getInteger(int|string $index): int;

    /**
     * Checks if the field is null, then gets the value from the target index cast as an integer.
     *
     * @param int|string $index Target index.
     * @return int Returns the value cast to an integer or null.
     */
    public function getIntegerOrNull(int|string $index): ?int;

    /**
     * Gets the value from the target index cast as a floating point number.
     *
     * @param int|string $index Target index.
     * @return float Returns the value cast to a floating point number.
     */
    public function getFloat(int|string $index): float;

    /**
     * Checks if the field is null, then gets the value from the target index cast as a floating point number.
     *
     * @param int|string $index Target index.
     * @return float Returns the value cast to a floating point number or null.
     */
    public function getFloatOrNull(int|string $index): ?float;

    /**
     * Gets the value from the target index cast as a boolean value.
     *
     * @param int|string $index Target index.
     * @return bool Returns the value cast to a boolean value.
     */
    public function getBoolean(int|string $index): bool;

    /**
     * Checks if the field is null, then gets the value from the target index cast as a boolean value.
     *
     * @param int|string $index Target index.
     * @return ?bool Returns the value cast to a boolean value or null.
     */
    public function getBooleanOrNull(int|string $index): ?bool;

    /**
     * Creates an iterator for this result.
     *
     * @param callable(DbResult): object $construct Result info constructor.
     * @return DbResultIterator Result iterator.
     */
    public function getIterator(callable $construct): DbResultIterator;
}

<?php
// DbResultCommon.php
// Created: 2023-11-09
// Updated: 2025-01-18

namespace Index\Db;

/**
 * Implements common DbResult methods.
 */
trait DbResultCommon {
    public function getString(int|string $index): string {
        $value = $this->getValue($index);
        return is_scalar($value) ? (string)$value : '';
    }

    public function getStringOrNull(int|string $index): ?string {
        return $this->isNull($index) ? null : $this->getString($index);
    }

    public function getInteger(int|string $index): int {
        $value = $this->getValue($index);
        return is_scalar($value) ? (int)$value : 0;
    }

    public function getIntegerOrNull(int|string $index): ?int {
        return $this->isNull($index) ? null : $this->getInteger($index);
    }

    public function getFloat(int|string $index): float {
        $value = $this->getValue($index);
        return is_scalar($value) ? (float)$value : 0.0;
    }

    public function getFloatOrNull(int|string $index): ?float {
        return $this->isNull($index) ? null : $this->getFloat($index);
    }

    public function getBoolean(int|string $index): bool {
        return $this->getInteger($index) !== 0;
    }

    public function getBooleanOrNull(int|string $index): ?bool {
        return $this->isNull($index) ? null : $this->getBoolean($index);
    }

    public function getIterator(callable $construct): DbResultIterator {
        return new DbResultIterator($this, $construct);
    }
}

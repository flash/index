<?php
// NullDbStatement.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db\NullDb;

use Index\Db\{DbType,DbResult,DbResultIterator,DbStatement};

/**
 * Represents a dummy database statement.
 */
class NullDbStatement implements DbStatement {
    public private(set) int $parameterCount = 0;

    public function addParameter(int $ordinal, mixed $value, DbType $type = DbType::Auto): void {}

    public function nextParameter(mixed $value, DbType $type = DbType::Auto): void {}

    public function getResult(): DbResult {
        return new NullDbResult;
    }

    public function getResultIterator(callable $construct): DbResultIterator {
        return $this->getResult()->getIterator($construct);
    }

    public private(set) int|string $lastInsertId = 0;

    public function execute(): int|string {
        return 0;
    }

    public function reset(): void {}
}

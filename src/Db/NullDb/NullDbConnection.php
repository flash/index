<?php
// NullDbConnection.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db\NullDb;

use Index\Db\{DbConnection,DbResult,DbStatement,DbTransaction};

/**
 * Represents a dummy database connection.
 */
class NullDbConnection implements DbConnection {
    public private(set) int|string $lastInsertId = 0;
    public private(set) int|string $affectedRows = 0;

    public function prepare(string $query): DbStatement {
        return new NullDbStatement;
    }

    public function query(string $query): DbResult {
        return new NullDbResult;
    }

    public function execute(string $query): int|string {
        return 0;
    }

    public function beginTransaction(): DbTransaction {
        return new NullDbTransaction;
    }
}

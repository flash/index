<?php
// NullDbTransaction.php
// Created: 2024-10-19
// Updated: 2024-10-19

namespace Index\Db\NullDb;

use Index\Db\{DbConnection,DbTransaction};
/**
 * Represents a dummy database transaction.
 */
class NullDbTransaction implements DbTransaction {
    public function commit(): void {}

    public function save(string $name): void {}

    public function release(string $name): void {}

    public function rollback(?string $name = null): void {}
}

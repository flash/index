<?php
// NullDbBackend.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db\NullDb;

use Index\Db\{DbBackend,DbConnection,DbConnectionInfo};

/**
 * Information about the dummy database layer.
 */
class NullDbBackend implements DbBackend {
    public private(set) bool $available = true;

    /**
     * Creates a dummy database connection.
     *
     * @param NullDbConnectionInfo $connectionInfo Dummy connection info.
     * @return NullDbConnection Dummy connection instance.
     */
    public function createConnection(DbConnectionInfo $connectionInfo): DbConnection {
        return new NullDbConnection;
    }

    /**
     * Constructs a connection info instance from a dsn.
     *
     * NullDb has no parameters that can be controlled using the DSN.
     *
     * @param string|array<string, int|string> $dsn DSN with connection information.
     * @return NullDbConnectionInfo Dummy connection info instance.
     */
    public function parseDsn(string|array $dsn): DbConnectionInfo {
        return new NullDbConnectionInfo;
    }
}

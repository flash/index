<?php
// NullDbConnectionInfo.php
// Created: 2021-05-02
// Updated: 2024-10-04

namespace Index\Db\NullDb;

use Index\Db\DbConnectionInfo;

/**
 * Represents dummy connection info.
 */
class NullDbConnectionInfo implements DbConnectionInfo {}

<?php
// NullDbResult.php
// Created: 2021-05-02
// Updated: 2024-10-19

namespace Index\Db\NullDb;

use Index\Db\{DbResult,DbResultIterator};

/**
 * Represents a dummy database result.
 */
class NullDbResult implements DbResult {
    public function next(): bool {
        return false;
    }

    public function isNull(int|string $index): bool {
        return true;
    }

    public function getValue(int|string $index): mixed {
        return null;
    }

    public function getString(int|string $index): string {
        return '';
    }

    public function getStringOrNull(int|string $index): ?string {
        return null;
    }

    public function getInteger(int|string $index): int {
        return 0;
    }

    public function getIntegerOrNull(int|string $index): ?int {
        return null;
    }

    public function getFloat(int|string $index): float {
        return 0.0;
    }

    public function getFloatOrNull(int|string $index): ?float {
        return null;
    }

    public function getBoolean(int|string $index): bool {
        return false;
    }

    public function getBooleanOrNull(int|string $index): ?bool {
        return null;
    }

    public function getIterator(callable $construct): DbResultIterator {
        return new DbResultIterator($this, $construct);
    }
}

<?php
// _ndx.php
// Created: 2024-10-18
// Updated: 2024-10-18

namespace Index\Db\NullDb;

use Index\Db\DbBackends;

DbBackends::register('null', NullDbBackend::class);

<?php
// DbTransaction.php
// Created: 2024-10-19
// Updated: 2025-01-18

namespace Index\Db;

use RuntimeException;

/**
 * Represents a transaction to be performed on the database.
 */
interface DbTransaction {
    /**
     * Commits the actions done during a transaction and ends the transaction.
     * A new transaction will be started if auto-commit is disabled.
     *
     * @throws RuntimeException If the commit failed.
     */
    public function commit(): void;

    /**
     * Creates a save point in the transaction that can be rolled back to.
     *
     * @param string $name Name for the save point.
     * @throws RuntimeException If save point creation failed.
     */
    public function save(string $name): void;

    /**
     * Releases a specified save point.
     *
     * @param string $name Name of the save point.
     * @throws RuntimeException If save points are not supported by this implementation.
     */
    public function release(string $name): void;

    /**
     * Rolls back to the state before a transaction start or to a specified save point.
     *
     * @param ?string $name Name of the save point, null for the entire transaction.
     * @throws RuntimeException If rollback failed.
     * @throws RuntimeException If save points are not supported by this implementation and $name was non-null.
     */
    public function rollback(?string $name = null): void;
}

<?php
// MariaDbParameter.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

use Index\Db\{DbTools,DbType};

/**
 * Represents a bound parameter.
 */
class MariaDbParameter {
    /**
     * @param int $ordinal Parameter number.
     * @param DbType $type DbType number.
     * @param mixed $value Value to bind.
     */
    public function __construct(
        public private(set) int $ordinal,
        public private(set) DbType $type,
        public private(set) mixed $value
    ) {
        if($type === DbType::Auto)
            $this->type = DbTools::detectType($value);
        if($type === DbType::Null)
            $this->value = null;
    }

    /**
     * Type character for the mysqli bind.
     *
     * @var string
     */
    public string $bindType {
        get => match($this->type) {
            DbType::Int => 'i',
            DbType::Float => 'd',
            DbType::Blob => 'b',
            default => 's',
        };
    }
}

<?php
// MariaDbWarning.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db\MariaDb;

use mysqli_warning;
use Stringable;
use Index\XArray;

/**
 * Represents a MariaDB/MySQL warning.
 */
class MariaDbWarning implements Stringable {
    /**
     * Creates a new MariaDbWarning object.
     *
     * @param string $message Warning message string.
     * @param string $sqlState SQL State of the warning.
     * @param int $code Error code of the warning.
     * @return MariaDbWarning A new warning instance.
     */
    public function __construct(
        public private(set) string $message,
        public private(set) string $sqlState,
        public private(set) int $code
    ) {}

    public function __toString(): string {
        return sprintf('[%s] %d: %s', $this->sqlState, $this->code, $this->message);
    }

    /**
     * Creates an array of warning objects from the output of $last_errors.
     *
     * @param array<int, array{errno: int, sqlstate: string, error: string}> $errors Array of warning arrays.
     * @return MariaDbWarning[] Array of warnings objects.
     */
    public static function fromLastErrors(array $errors): array {
        return XArray::select($errors, fn($error) => new MariaDbWarning($error['error'], $error['sqlstate'], $error['errno']));
    }

    /**
     * Creates an array of warning objects from a mysqli_warning instance.
     *
     * @param mysqli_warning|false $warnings Warning object.
     * @return MariaDbWarning[] Array of warning objects.
     */
    public static function fromGetWarnings(mysqli_warning|false $warnings): array {
        $array = [];

        if($warnings !== false)
            do {
                $array[] = new MariaDbWarning(
                    $warnings->message,
                    $warnings->sqlstate,
                    $warnings->errno
                );
            } while($warnings->next());

        return $array;
    }
}

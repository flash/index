<?php
// MariaDbStatement.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

use mysqli_stmt;
use InvalidArgumentException;
use RuntimeException;
use Stringable;
use Index\Db\{DbType,DbStatement,DbStatementCommon};

/**
 * Represents a MariaDB/MySQL prepared statement.
 */
class MariaDbStatement implements DbStatement {
    use DbStatementCommon;

    /** @var array<int, MariaDbParameter> */
    private array $params = [];

    /**
     * Creates a MariaDbStatement.
     *
     * @param mysqli_stmt $statement Underlying statement object.
     * @return MariaDbStatement A new statement instance.
     */
    public function __construct(
        private mysqli_stmt $statement
    ) {}

    private static bool $constructed = false;

    /** @var class-string<MariaDbResult> */
    private static string $resultImplementation;

    /**
     * Determines which MariaDbResult implementation should be used.
     */
    public static function construct(): void {
        if(self::$constructed !== false)
            throw new RuntimeException('Static constructor was already called.');
        self::$constructed = true;
        self::$resultImplementation = function_exists('mysqli_stmt_get_result')
            ? MariaDbResultNative::class
            : MariaDbResultLib::class;
    }

    public int $paramCount {
        get => $this->statement->param_count;
    }

    public function addParameter(int $ordinal, mixed $value, DbType $type = DbType::Auto): void {
        if($ordinal < 1 || $ordinal > $this->paramCount)
            throw new InvalidArgumentException('$ordinal is not a valid parameter number.');

        $this->params[$ordinal - 1] = new MariaDbParameter($ordinal, $type, $value);
    }

    public function nextParameter(mixed $value, DbType $type = DbType::Auto): void {
        $nextIndex = count($this->params);
        if($nextIndex >= $this->paramCount)
            throw new RuntimeException('Attempted to specify too many parameters.');

        $this->params[$nextIndex] = new MariaDbParameter($nextIndex + 1, $type, $value);
    }

    /**
     * Gets the last error code.
     *
     * @var int
     */
    public int $lastErrorCode {
        get => $this->statement->errno;
    }

    /**
     * Gets the last error string.
     *
     * @var string
     */
    public string $lastErrorString {
        get => $this->statement->error;
    }

    /**
     * Gets the current SQL State of this statement.
     *
     * @var string
     */
    public string $sqlState {
        get => $this->statement->sqlstate;
    }

    /**
     * Gets a list of errors from the last command.
     *
     * @var MariaDbWarning[]
     */
    public array $lastErrors {
        get => MariaDbWarning::fromLastErrors($this->statement->error_list);
    }

    /**
     * Gets list of warnings.
     *
     * The result of SHOW WARNINGS;
     *
     * @var MariaDbWarning[]
     */
    public array $warnings {
        get => MariaDbWarning::fromGetWarnings($this->statement->get_warnings());
    }

    private bool $executed = false;

    public function getResult(): MariaDbResult {
        if(!$this->executed)
            throw new RuntimeException('no result available');

        return new self::$resultImplementation($this->statement);
    }

    public int|string $lastInsertId {
        get => $this->statement->insert_id;
    }

    public function execute(): int|string {
        $types = '';
        $args = [null];

        foreach($this->params as $key => $param) {
            $types .= $param->bindType;
            $value = $param->value;

            if($param->type === DbType::Null)
                $value = null;
            elseif($param->type === DbType::Blob) {
                if(is_resource($value)) {
                    while($data = fread($value, 8192))
                        $this->statement->send_long_data($key, $data);
                } elseif(is_scalar($value) || $value instanceof Stringable)
                    $this->statement->send_long_data($key, (string)$value);

                $value = null;
            }


            ${"value{$key}"} = $value;
            $args[] = &${"value{$key}"};
        }

        if(!empty($types)) {
            $args[0] = $types;
            call_user_func_array([$this->statement, 'bind_param'], $args);
        }

        if(!$this->statement->execute())
            throw new RuntimeException($this->lastErrorString, $this->lastErrorCode);

        $this->executed = true;

        return $this->statement->affected_rows;
    }

    public function reset(): void {
        $this->executed = false;
        $this->params = [];
        if(!$this->statement->reset())
            throw new RuntimeException($this->lastErrorString, $this->lastErrorCode);
    }

    public function __destruct() {
        $this->statement->close();
    }
}

MariaDbStatement::construct();

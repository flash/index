<?php
// MariaDbTransaction.php
// Created: 2024-10-19
// Updated: 2024-10-19

namespace Index\Db\MariaDb;

use mysqli;
use Index\Db\DbTransaction;

/**
 * Represents a MariaDB transaction.
 */
class MariaDbTransaction implements DbTransaction {
    /**
     * @param MariaDbConnection $conn Underlying connection
     * @param mysqli $connRaw Underlying connection (off vocal)
     */
    public function __construct(
        private MariaDbConnection $conn,
        private mysqli $connRaw
    ) {}

    public function commit(): void {
        if(!$this->connRaw->commit())
            $this->conn->throwLastError();
    }

    public function save(string $name): void {
        if(!$this->connRaw->savepoint($name))
            $this->conn->throwLastError();
    }

    public function release(string $name): void {
        if(!$this->connRaw->release_savepoint($name))
            $this->conn->throwLastError();
    }

    public function rollback(?string $name = null): void {
        if(!$this->connRaw->rollback(0, $name))
            $this->conn->throwLastError();
    }
}

<?php
// MariaDbCharacterSetInfo.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

/**
 * Contains information about the character set.
 *
 * @see https://www.php.net/manual/en/mysqli.get-charset
 */
class MariaDbCharacterSetInfo {
    /**
     * Creates a new character set info instance.
     *
     * @param object $charSetValue Anonymous object containing the information.
     * @return MariaDbCharacterSetInfo Character set information class.
     */
    public function __construct(
        private object $charSetValue
    ) {}

    /**
     * Name of the current character set.
     *
     * @var string
     */
    public string $charSet {
        get => isset($this->charSetValue->charset) && is_scalar($this->charSetValue->charset)
            ? (string)$this->charSetValue->charset : '';
    }

    /**
     * Name of the default collation.
     *
     * @var string
     */
    public string $defaultCollation {
        get => isset($this->charSetValue->collation) && is_scalar($this->charSetValue->collation)
            ? (string)$this->charSetValue->collation : '';
    }

    /**
     * Path to the directory the character was read from.
     * May be empty for built-in character sets.
     *
     * @var string
     */
    public string $directory {
        get => isset($this->charSetValue->dir) && is_scalar($this->charSetValue->dir)
            ? (string)$this->charSetValue->dir : '';
    }

    /**
     * Minimum character width in bytes for this character set.
     *
     * @var int
     */
    public int $minWidth {
        get => isset($this->charSetValue->min_length) && is_scalar($this->charSetValue->min_length)
            ? (int)$this->charSetValue->min_length : 0;
    }

    /**
     * Maximum character width in bytes for this character set.
     *
     * @var int
     */
    public int $maxWidth {
        get => isset($this->charSetValue->max_length) && is_scalar($this->charSetValue->max_length)
            ? (int)$this->charSetValue->max_length : 0;
    }

    /**
     * Internal numeric identifier for this character set.
     *
     * @var int
     */
    public int $id {
        get => isset($this->charSetValue->number) && is_scalar($this->charSetValue->number)
            ? (int)$this->charSetValue->number : 0;
    }

    /**
     * Character set status.
     *
     * Whatever that means. Given the (?) in the official documentation, not even they know.
     *
     * @var int
     */
    public int $state {
        get => isset($this->charSetValue->state) && is_scalar($this->charSetValue->state)
            ? (int)$this->charSetValue->state : 0;
    }
}

<?php
// MariaDbResultNative.php
// Created: 2021-05-02
// Updated: 2024-10-19

namespace Index\Db\MariaDb;

use mysqli_result;
use mysqli_stmt;
use RuntimeException;

/**
 * Implementation of MariaDbResult for mysqlnd.
 */
class MariaDbResultNative extends MariaDbResult {
    public function __construct(mysqli_stmt|mysqli_result $result) {
        if($result instanceof mysqli_stmt) {
            $_result = $result->get_result();
            if($_result === false)
                throw new RuntimeException($result->error, $result->errno);
            $result = $_result;
        }

        parent::__construct($result);
    }

    public function next(): bool {
        if(!($this->result instanceof mysqli_result))
            return false;

        $result = $this->result->fetch_array(MYSQLI_BOTH);
        if($result === null || $result === false)
            return false;

        $this->currentRow = $result;
        return true;
    }

    public function __destruct() {
        try {
           $this->result->close();
        } catch(\Error $ex) {}
    }
}

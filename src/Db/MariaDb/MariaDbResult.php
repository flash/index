<?php
// MariaDbResult.php
// Created: 2021-05-02
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

use mysqli_result;
use mysqli_stmt;
use InvalidArgumentException;
use Index\Db\{DbResult,DbResultCommon};

/**
 * Represents a MariaDB/MySQL database result.
 */
abstract class MariaDbResult implements DbResult {
    use DbResultCommon;

    /** @var array<int|string, mixed> */
    protected array $currentRow = [];

    /**
     * Creates a new MariaDbResult instance.
     *
     * @param mysqli_stmt|mysqli_result $result A result to work with.
     * @return MariaDbResult A result instance.
     */
    public function __construct(
        protected mysqli_stmt|mysqli_result $result
    ) {}

    /**
     * Gets the number of available rows in this result.
     *
     * @var int|string
     */
    public int|string $rowCount {
        get => $this->result->num_rows;
    }

    /**
     * Gets the number of fields in the result per row.
     *
     * @var int
     */
    public int $fieldCount {
        get => $this->result->field_count;
    }

    /**
     * Seek to a specific row.
     *
     * @param int $offset Offset of the row.
     */
    public function seekRow(int $offset): void {
        if(!$this->result->data_seek($offset))
            throw new InvalidArgumentException('$offset is not a valid row offset.');
    }

    abstract public function next(): bool;

    public function isNull(int|string $index): bool {
        return array_key_exists($index, $this->currentRow) && $this->currentRow[$index] === null;
    }

    public function getValue(int|string $index): mixed {
        return $this->currentRow[$index] ?? null;
    }
}

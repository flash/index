<?php
// MariaDbConnectionInfo.php
// Created: 2021-04-30
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

use InvalidArgumentException;
use Index\Db\DbConnectionInfo;
use Index\Net\{EndPoint,DnsEndPoint,IpEndPoint,UnixEndPoint};

/**
 * Describes a MariaDB or MySQL connection.
 */
class MariaDbConnectionInfo implements DbConnectionInfo {
    /**
     * Creates an instance of MariaDbConnectionInfo.
     *
     * @param EndPoint $endPoint End point at which the server can be found.
     * @param string $userName User name to use for the connection.
     * @param string $password Password with which the user authenticates.
     * @param string $dbName Default database name.
     * @param ?string $charSet Default character set.
     * @param ?string $initCommand Command to execute on connect.
     * @param ?string $keyPath Path at which to find the private key for SSL.
     * @param ?string $certPath Path at which to find the certificate file for SSL.
     * @param ?string $certAuthPath Path at which to find the certificate authority file for SSL.
     * @param ?string $trustedCertsPath Path at which to find the trusted SSL CA certificates for SSL in PEM format.
     * @param ?string $cipherAlgos List of SSL encryption cipher that are allowed.
     * @param bool $verifyCert true if the client should verify the server's SSL certificate, false if not.
     * @param bool $useCompression true if compression should be used, false if not.
     * @return MariaDbConnectionInfo A connection info instance representing the given information.
     */
    public function __construct(
        public private(set) EndPoint $endPoint,
        public private(set) string $userName,
        #[\SensitiveParameter] public private(set) string $password,
        public private(set) string $dbName,
        public private(set) ?string $charSet,
        public private(set) ?string $initCommand,
        public private(set) ?string $keyPath,
        public private(set) ?string $certPath,
        public private(set) ?string $certAuthPath,
        public private(set) ?string $trustedCertsPath,
        public private(set) ?string $cipherAlgos,
        public private(set) bool $verifyCert,
        public private(set) bool $useCompression
    ) {
        if(!($endPoint instanceof IpEndPoint)
            && !($endPoint instanceof DnsEndPoint)
            && !($endPoint instanceof UnixEndPoint))
            throw new InvalidArgumentException('$endPoint must be of type IpEndPoint, DnsEndPoint or UnixEndPoint.');

        if(empty($charSet))
            $this->charSet = null;
        if(empty($initCommand))
            $this->initCommand = null;
        if(empty($keyPath))
            $this->keyPath = null;
        if(empty($certPath))
            $this->certPath = null;
        if(empty($certAuthPath))
            $this->certAuthPath = null;
        if(empty($trustedCertsPath))
            $this->trustedCertsPath = null;
        if(empty($cipherAlgos))
            $this->cipherAlgos = null;
    }

    /**
     * Returns whether the end point is a unix socket path.
     *
     * @var bool
     */
    public bool $unixSocket {
        get => $this->endPoint instanceof UnixEndPoint;
    }

    /**
     * Gets the host value of the end point.
     *
     * Will return an empty string if the end point is a unix socket path.
     *
     * @var string
     */
    public string $host {
        get {
            if($this->endPoint instanceof DnsEndPoint)
                return $this->endPoint->host;
            if($this->endPoint instanceof IpEndPoint)
                return $this->endPoint->address->clean;
            return '';
        }
    }

    /**
     * Gets the port value of the end point.
     *
     * Will return -1 if the end point is a unix socket path.
     *
     * @var int<-1, 65535>
     */
    public int $port {
        get {
            if(($this->endPoint instanceof DnsEndPoint) || ($this->endPoint instanceof IpEndPoint)) {
                $port = $this->endPoint->port;
                return $port < 1 ? 3306 : min(0xFFFF, $port);
            }

            return -1;
        }
    }

    /**
     * Gets the unix socket path.
     *
     * Will return an empty string if the end point is not a unix socket path.
     *
     * @var string
     */
    public string $unixSocketPath {
        get => $this->endPoint instanceof UnixEndPoint ? $this->endPoint->path : '';
    }

    /**
     * Creates a connection info instance with a hostname and port number.
     *
     * @param string $host Server host name.
     * @param int $port Server port.
     * @param string $userName User name to use for the connection.
     * @param string $password Password with which the user authenticates.
     * @param string $dbName Default database name.
     * @param ?string $charSet Default character set.
     * @param ?string $initCommand Command to execute on connect.
     * @param ?string $keyPath Path at which to find the private key for SSL.
     * @param ?string $certPath Path at which to find the certificate file for SSL.
     * @param ?string $certAuthPath Path at which to find the certificate authority file for SSL.
     * @param ?string $trustedCertsPath Path at which to find the trusted SSL CA certificates for SSL in PEM format.
     * @param ?string $cipherAlgos List of SSL encryption cipher that are allowed.
     * @param bool $verifyCert true if the client should verify the server's SSL certificate, false if not.
     * @param bool $useCompression true if compression should be used, false if not.
     * @return MariaDbConnectionInfo A connection info instance representing the given information.
     */
    public static function createHost(
        string $host,
        int $port,
        string $userName,
        #[\SensitiveParameter] string $password,
        string $dbName = '',
        ?string $charSet = null,
        ?string $initCommand = null,
        ?string $keyPath = null,
        ?string $certPath = null,
        ?string $certAuthPath = null,
        ?string $trustedCertsPath = null,
        ?string $cipherAlgos = null,
        bool $verifyCert = false,
        bool $useCompression = false
    ): MariaDbConnectionInfo {
        return new MariaDbConnectionInfo(
            new DnsEndPoint($host, $port),
            $userName,
            $password,
            $dbName,
            $charSet,
            $initCommand,
            $keyPath,
            $certPath,
            $certAuthPath,
            $trustedCertsPath,
            $cipherAlgos,
            $verifyCert,
            $useCompression
        );
    }

    /**
     * Creates a connection info instance with a unix socket path.
     *
     * @param string $path Path to the unix socket.
     * @param string $userName User name to use for the connection.
     * @param string $password Password with which the user authenticates.
     * @param string $dbName Default database name.
     * @param ?string $charSet Default character set.
     * @param ?string $initCommand Command to execute on connect.
     * @param ?string $keyPath Path at which to find the private key for SSL.
     * @param ?string $certPath Path at which to find the certificate file for SSL.
     * @param ?string $certAuthPath Path at which to find the certificate authority file for SSL.
     * @param ?string $trustedCertsPath Path at which to find the trusted SSL CA certificates for SSL in PEM format.
     * @param ?string $cipherAlgos List of SSL encryption cipher that are allowed.
     * @param bool $verifyCert true if the client should verify the server's SSL certificate, false if not.
     * @param bool $useCompression true if compression should be used, false if not.
     * @return MariaDbConnectionInfo A connection info instance representing the given information.
     */
    public static function createUnix(
        string $path,
        string $userName,
        #[\SensitiveParameter] string $password,
        string $dbName = '',
        ?string $charSet = null,
        ?string $initCommand = null,
        ?string $keyPath = null,
        ?string $certPath = null,
        ?string $certAuthPath = null,
        ?string $trustedCertsPath = null,
        ?string $cipherAlgos = null,
        bool $verifyCert = false,
        bool $useCompression = false
    ): MariaDbConnectionInfo {
        return new MariaDbConnectionInfo(
            UnixEndPoint::parse($path),
            $userName,
            $password,
            $dbName,
            $charSet,
            $initCommand,
            $keyPath,
            $certPath,
            $certAuthPath,
            $trustedCertsPath,
            $cipherAlgos,
            $verifyCert,
            $useCompression
        );
    }

    /**
     * Tries to parse an end point string and creates a connection info instance using it..
     *
     * Supports IPv4, IPv6 and DNS hostnames with and without a port number and Unix socket paths prefixed with unix://
     *
     * @param string $endPoint End point string.
     * @param string $userName User name to use for the connection.
     * @param string $password Password with which the user authenticates.
     * @param string $dbName Default database name.
     * @param ?string $charSet Default character set.
     * @param ?string $initCommand Command to execute on connect.
     * @param ?string $keyPath Path at which to find the private key for SSL.
     * @param ?string $certPath Path at which to find the certificate file for SSL.
     * @param ?string $certAuthPath Path at which to find the certificate authority file for SSL.
     * @param ?string $trustedCertsPath Path at which to find the trusted SSL CA certificates for SSL in PEM format.
     * @param ?string $cipherAlgos List of SSL encryption cipher that are allowed.
     * @param bool $verifyCert true if the client should verify the server's SSL certificate, false if not.
     * @param bool $useCompression true if compression should be used, false if not.
     * @return MariaDbConnectionInfo A connection info instance representing the given information.
     */
    public static function create(
        string $endPoint,
        string $userName,
        #[\SensitiveParameter] string $password,
        string $dbName = '',
        ?string $charSet = null,
        ?string $initCommand = null,
        ?string $keyPath = null,
        ?string $certPath = null,
        ?string $certAuthPath = null,
        ?string $trustedCertsPath = null,
        ?string $cipherAlgos = null,
        bool $verifyCert = false,
        bool $useCompression = false
    ): MariaDbConnectionInfo {
        return new MariaDbConnectionInfo(
            EndPoint::parse($endPoint),
            $userName,
            $password,
            $dbName,
            $charSet,
            $initCommand,
            $keyPath,
            $certPath,
            $certAuthPath,
            $trustedCertsPath,
            $cipherAlgos,
            $verifyCert,
            $useCompression
        );
    }
}

<?php
// _ndx.php
// Created: 2024-10-18
// Updated: 2024-10-18

namespace Index\Db\MariaDb;

use Index\Db\DbBackends;

DbBackends::register('mariadb', MariaDbBackend::class);
DbBackends::register('mysql', MariaDbBackend::class);

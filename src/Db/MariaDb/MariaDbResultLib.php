<?php
// MariaDbResultLib.php
// Created: 2021-05-02
// Updated: 2024-10-19

namespace Index\Db\MariaDb;

use mysqli_stmt;
use RuntimeException;

/**
 * Implementation of MariaDbResult for libmysql.
 */
class MariaDbResultLib extends MariaDbResult {
    /** @var mixed[] */
    private array $fields = [];

    /** @var mixed[] */
    private array $data = [];

    public function __construct(mysqli_stmt $statement) {
        parent::__construct($statement);

        if(!$statement->store_result())
            throw new RuntimeException($statement->error, $statement->errno);

        $metadata = $statement->result_metadata();
        if($metadata === false)
            throw new RuntimeException('result_metadata output was false');

        while($field = $metadata->fetch_field())
            $this->fields[] = &$this->data[$field->name];

        call_user_func_array([$statement, 'bind_result'], $this->fields);
    }

    public function next(): bool {
        if(!($this->result instanceof mysqli_stmt))
            return false;

        $result = $this->result->fetch();
        if($result === false)
            throw new RuntimeException($this->result->error, $this->result->errno);
        if($result === null)
            return false;

        $i = 0;
        $this->currentRow = [];
        foreach($this->data as $key => $value) {
            $this->currentRow[$i++] = $value;
            $this->currentRow[$key] = $value;
        }

        return true;
    }

    public function __destruct() {
        try {
           $this->result->free_result();
        } catch(\Error $ex) {}
    }
}

<?php
// MariaDbConnection.php
// Created: 2021-04-30
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

use mysqli;
use mysqli_sql_exception;
use stdClass;
use InvalidArgumentException;
use RuntimeException;
use Index\Db\{DbConnection,DbTransaction};

/**
 * Represents a connection with a MariaDB or MySQL database server.
 */
class MariaDbConnection implements DbConnection {
    private mysqli $connection;

    /**
     * Creates a new instance of MariaDbConnection.
     *
     * @param MariaDbConnectionInfo $connectionInfo Information about the connection.
     * @return MariaDbConnection A new instance of MariaDbConnection.
     */
    public function __construct(MariaDbConnectionInfo $connectionInfo) {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        // I'm not sure if calling "new mysqli" without arguments is equivalent to this
        //  the documentation would suggest it's not and that it just pulls from the config
        //  nothing suggests otherwise too.
        // The output of mysqli_init is just an object anyway so we can safely use it instead
        //  but continue to use it as an object.
        $connection = mysqli_init();
        if($connection === false)
            throw new RuntimeException('mysqli_init() returned false!');

        $this->connection = $connection;
        $this->connection->options(MYSQLI_OPT_LOCAL_INFILE, 0);

        if(!empty($connectionInfo->charSet))
            $this->connection->options(MYSQLI_SET_CHARSET_NAME, $connectionInfo->charSet);

        if(!empty($connectionInfo->initCommand))
            $this->connection->options(MYSQLI_INIT_COMMAND, $connectionInfo->initCommand);

        $flags = $connectionInfo->useCompression ? MYSQLI_CLIENT_COMPRESS : 0;

        if(!empty($connectionInfo->keyPath)) {
            $flags |= MYSQLI_CLIENT_SSL;

            if($connectionInfo->verifyCert)
                $this->connection->options(MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, 1);
            else
                $flags |= MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT;

            $this->connection->ssl_set(
                $connectionInfo->keyPath,
                $connectionInfo->certPath,
                $connectionInfo->certAuthPath,
                $connectionInfo->trustedCertsPath,
                $connectionInfo->cipherAlgos
            );
        }

        try {
            if($connectionInfo->unixSocket)
                $this->connection->real_connect(
                    '',
                    $connectionInfo->userName,
                    $connectionInfo->password,
                    $connectionInfo->dbName,
                    -1,
                    $connectionInfo->unixSocketPath,
                    $flags
                );
            else
                $this->connection->real_connect(
                    $connectionInfo->host,
                    $connectionInfo->userName,
                    $connectionInfo->password,
                    $connectionInfo->dbName,
                    $connectionInfo->port,
                    '',
                    $flags
                );
        } catch(mysqli_sql_exception $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }

        $this->connection->autocommit(true);
    }

    public int|string $affectedRows {
        get => $this->connection->affected_rows;
    }

    /**
     * Currently active character set.
     *
     * @throws InvalidArgumentException Switching to new character set failed.
     * @var string
     */
    public string $charSet {
        get => $this->connection->character_set_name();
        set(string $value) {
            if(!$this->connection->set_charset($value))
                throw new InvalidArgumentException('$charSet is not a supported character set.');
        }
    }

    /**
     * Info about the currently character set and collation.
     *
     * @var MariaDbCharacterSetInfo
     */
    public MariaDbCharacterSetInfo $charSetInfo {
        get => new MariaDbCharacterSetInfo($this->connection->get_charset());
    }

    /**
     * Switches the connection to a different user and default database.
     *
     * @param string $userName New user name.
     * @param string $password New password.
     * @param string|null $database New default database.
     * @throws RuntimeException If the user switch action failed.
     */
    public function switchUser(string $userName, string $password, string|null $database = null): void {
        if(!$this->connection->change_user($userName, $password, $database === null ? null : $database))
            $this->throwLastError();
    }

    /**
     * Switches the default database for this connection.
     *
     * @param string $database New default database.
     * @throws RuntimeException If the database switch failed.
     */
    public function switchDatabase(string $database): void {
        if(!$this->connection->select_db($database))
            $this->throwLastError();
    }

    /**
     * Version of the server.
     *
     * @var string
     */
    public string $serverVersion {
        get => MariaDbBackend::intToVersion($this->connection->server_version);
    }

    /**
     * Last error code.
     *
     * @var int
     */
    public int $lastErrorCode {
        get => $this->connection->errno;
    }

    /**
     * Last error string.
     *
     * @var string
     */
    public string $lastErrorString {
        get => $this->connection->error;
    }

    /**
     * Throws the last error as a RuntimeException.
     *
     * @throws RuntimeException Based on the last error code and string.
     */
    public function throwLastError(): never {
        throw new RuntimeException((string)$this->lastErrorString, $this->lastErrorCode);
    }

    /**
     * Current SQL State of the connection.
     *
     * @var string
     */
    public string $sqlState {
        get => $this->connection->sqlstate;
    }

    /**
     * List of errors from the last command.
     *
     * @var MariaDbWarning[]
     */
    public array $lastErrors {
        get {
            // imagine if stdlib stuff had type annotations, couldn't be me
            /** @var array<int, array{errno: int, sqlstate: string, error: string}> */
            $errorList = $this->connection->error_list;
            return MariaDbWarning::fromLastErrors($errorList);
        }
    }

    /**
     * Number of columns for the last query.
     *
     * @var int
     */
    public int $lastFieldCount {
        get => $this->connection->field_count;
    }

    /**
     * List of warnings.
     *
     * The result of SHOW WARNINGS;
     *
     * @var MariaDbWarning[]
     */
    public array $warnings {
        get => MariaDbWarning::fromGetWarnings($this->connection->get_warnings());
    }

    /**
     * Number of warnings for the last query.
     *
     * @var int
     */
    public int $warningCount {
        get => $this->connection->warning_count;
    }

    public int|string $lastInsertId {
        get => $this->connection->insert_id;
    }

    public function beginTransaction(): DbTransaction {
        if(!$this->connection->begin_transaction())
            $this->throwLastError();

        return new MariaDbTransaction($this, $this->connection);
    }

    /**
     * Thread ID for the current connection.
     *
     * @var int
     */
    public int $threadId {
        get => $this->connection->thread_id;
    }

    /**
     * @return MariaDbStatement A database statement.
     */
    public function prepare(string $query): MariaDbStatement {
        try {
            $statement = $this->connection->prepare($query);
        } catch(mysqli_sql_exception $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }
        if($statement === false)
            $this->throwLastError();
        return new MariaDbStatement($statement);
    }

    /**
     * @return MariaDbResult A database result.
     */
    public function query(string $query): MariaDbResult {
        try {
            $result = $this->connection->query($query, MYSQLI_STORE_RESULT);
        } catch(mysqli_sql_exception $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }

        if($result === false)
            $this->throwLastError();

        // the mysql library is very adorable
        if($result === true)
            throw new RuntimeException('Query succeeded but you should have use the execute method for that query.');

        // Yes, this always uses Native, for some reason the stupid limitation in libmysql only applies to preparing
        return new MariaDbResultNative($result);
    }

    public function execute(string $query): int|string {
        try {
            if(!$this->connection->real_query($query))
                $this->throwLastError();
        } catch(mysqli_sql_exception $ex) {
            throw new RuntimeException($ex->getMessage(), $ex->getCode(), $ex);
        }
        return $this->connection->affected_rows;
    }

    /**
     * Closes the connection and associated resources.
     */
    public function __destruct() {
        try {
           $this->connection->close();
        } catch(\Error $ex) {}
    }
}

<?php
// MariaDbBackend.php
// Created: 2021-04-30
// Updated: 2025-02-27

namespace Index\Db\MariaDb;

use InvalidArgumentException;
use Index\Db\{DbBackend,DbConnection,DbConnectionInfo};
use Index\Net\{EndPoint,UnixEndPoint};

/**
 * Information about the MariaDB/MySQL database layer.
 */
class MariaDbBackend implements DbBackend {
    public bool $available {
        get => extension_loaded('mysqli');
    }

    /**
     * Formats a MySQL integer version as a string.
     *
     * @param int $version MySQL version number.
     * @return string
     */
    public static function intToVersion(int $version): string {
        $sub = $version % 100;
        $version = floor($version / 100);
        $minor = $version % 100;
        $version = floor($version / 100);
        $major = $version % 100;
        return sprintf('%d.%d.%d', $major, $minor, $sub);
    }

    /**
     * Gets the version of the underlying client library.
     *
     * @var string
     */
    public string $clientVersion {
        get => self::intToVersion(mysqli_get_client_version());
    }

    /**
     * Creates a connection with a MariaDB or MySQL server.
     *
     * @param MariaDbConnectionInfo $connectionInfo Object that describes the desired connection.
     * @return MariaDbConnection A connection with a MariaDB or MySQL server.
     */
    public function createConnection(DbConnectionInfo $connectionInfo): DbConnection {
        if(!($connectionInfo instanceof MariaDbConnectionInfo))
            throw new InvalidArgumentException('$connectionInfo must by of type MariaDbConnectionInfo');

        return new MariaDbConnection($connectionInfo);
    }

    /**
     * Constructs a connection info instance from a dsn.
     *
     * A username and password can be specified in their respective parts of the URL.
     *
     * The host part of the URL can be any DNS name, or special value `:unix:`, documented further down.
     *
     * The path part of the URL is used as the database name. Any prefix or suffix slashes (`/`) are trimmed and others are converted to an underscore (`_`).
     * Meaning `/slash/test/` is converted to `slash_test`.
     *
     * In order to use a Unix socket path, set the host part to `:unix:` and specify `socket=/path/to/socket.sock` in the query.
     *
     * Other supported query parameters are:
     *  - `charset=<name>`: Specifies the character set to use for the connection.
     *  - `init=<query>`: Any arbitrary SQL command to execute open connecting.
     *  - `enc_key=<path>`: Path to a private key file for SSL.
     *  - `enc_cert=<path>`: Path to a certificate file for SSL.
     *  - `enc_authority=<path>`: Path to a certificate authority file for SSL.
     *  - `enc_trusted_certs=<path>`: Path to a directory that contains PEM formatted SSL CA certificates.
     *  - `enc_ciphers=<list>`: A list of allowable ciphers to use for SSL encryption.
     *  - `enc_no_verify`: Disables verification of the server certificate, allows for MITM attacks.
     *  - `compress`: Enables protocol compression.
     *
     * Previously supported query parameters:
     *  - `enc_verify=<anything>`: Enabled verification of server certificate. Replaced with `enc_no_verify` as it now defaults to on.
     *
     * @param string|array<string, int|string> $dsn DSN with connection information.
     * @return MariaDbConnectionInfo MariaDB connection info.
     */
    public function parseDsn(string|array $dsn): DbConnectionInfo {
        if(is_string($dsn)) {
            $dsn = parse_url($dsn);
            if($dsn === false)
                throw new InvalidArgumentException('$dsn is not a valid uri.');
        }

        if(!isset($dsn['host']))
            throw new InvalidArgumentException('Host is missing from DSN.');
        if(!isset($dsn['path']))
            throw new InvalidArgumentException('Path is missing from DSN.');

        $host = $dsn['host'];
        $needsUnix = $host === ':unix';

        if(!$needsUnix && isset($dsn['port']))
            $host .= ':' . $dsn['port'];

        $user = (string)($dsn['user'] ?? '');
        $pass = (string)($dsn['pass'] ?? '');
        $endPoint = $needsUnix ? null : EndPoint::parse((string)$host);
        $dbName = str_replace('/', '_', trim((string)$dsn['path'], '/')); // cute for table prefixes i think

        parse_str(str_replace('+', '%2B', (string)($dsn['query'] ?? '')), $query);

        $unixPath = $query['socket'] ?? null;
        if(!is_string($unixPath)) $unixPath = null;

        $charSet = $query['charset'] ?? null;
        if(!is_string($charSet)) $charSet = null;

        $initCommand = $query['init'] ?? null;
        if(!is_string($initCommand)) $initCommand = null;

        $keyPath = $query['enc_key'] ?? null;
        if(!is_string($keyPath)) $keyPath = null;

        $certPath = $query['enc_cert'] ?? null;
        if(!is_string($certPath)) $certPath = null;

        $certAuthPath = $query['enc_authority'] ?? null;
        if(!is_string($certAuthPath)) $certAuthPath = null;

        $trustedCertsPath = $query['enc_trusted_certs'] ?? null;
        if(!is_string($trustedCertsPath)) $trustedCertsPath = null;

        $cipherAlgos = $query['enc_ciphers'] ?? null;
        if(!is_string($cipherAlgos)) $cipherAlgos = null;

        $verifyCert = !isset($query['enc_no_verify']);
        $useCompression = isset($query['compress']);

        if($needsUnix) {
            if(empty($unixPath))
                throw new InvalidArgumentException('Unix socket path is missing from DSN.');
            $endPoint = new UnixEndPoint($unixPath);
        }

        if($endPoint === null)
            throw new InvalidArgumentException('No end point could be determined from the provided DSN.');

        return new MariaDbConnectionInfo(
            $endPoint, $user, $pass, $dbName,
            $charSet, $initCommand, $keyPath, $certPath,
            $certAuthPath, $trustedCertsPath, $cipherAlgos,
            $verifyCert, $useCompression
        );
    }
}

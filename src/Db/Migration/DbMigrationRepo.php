<?php
// DbMigrationRepo.php
// Created: 2023-01-07
// Updated: 2025-01-18

namespace Index\Db\Migration;

/**
 * Represents a repository of migrations.
 */
interface DbMigrationRepo {
    /**
     * Migrations contained in this repository.
     *
     * @return DbMigrationInfo[]
     */
    public function getMigrations(): array;
}

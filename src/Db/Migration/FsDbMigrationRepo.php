<?php
// FsDbMigrationRepo.php
// Created: 2023-01-07
// Updated: 2025-01-18

namespace Index\Db\Migration;

use RuntimeException;

class FsDbMigrationRepo implements DbMigrationRepo {
    /**
     * @param string $path Filesystem path to the directory containing the migration files.
     */
    public function __construct(
        private string $path
    ) {}

    public function getMigrations(): array {
        if(!is_dir($this->path))
            return [];

        $files = glob(realpath($this->path) . '/*.php');
        if($files === false)
            throw new RuntimeException('Failed to glob migration files.');

        $migrations = [];

        foreach($files as $file)
            $migrations[] = new FsDbMigrationInfo($file);

        return $migrations;
    }

    /**
     * Saves a migratinon template to a file within the directory of this migration repository.
     * If the repository directory does not exist, it will be created.
     *
     * @param string $name Name for the migration PHP file.
     * @param string $body Body for the migration PHP file.
     */
    public function saveMigrationTemplate(string $name, string $body): void {
        if(!is_dir($this->path))
            mkdir($this->path, 0777, true);

        file_put_contents(realpath($this->path) . '/' . $name . '.php', $body);
    }
}

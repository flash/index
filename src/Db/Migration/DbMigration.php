<?php
// DbMigration.php
// Created: 2023-01-07
// Updated: 2024-10-04

namespace Index\Db\Migration;

use Index\Db\DbConnection;

/**
 * Interface for migration classes to inherit.
 */
interface DbMigration {
    /**
     * Runs the migration implemented by this class. This process is irreversible!
     *
     * @param DbConnection $conn Database connection to execute this migration on.
     */
    public function migrate(DbConnection $conn): void;
}

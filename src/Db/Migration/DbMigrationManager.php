<?php
// DbMigrationManager.php
// Created: 2023-01-07
// Updated: 2025-01-18

namespace Index\Db\Migration;

use stdClass;
use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use RuntimeException;
use Index\XDateTime;
use Index\Db\{DbConnection,DbStatement,DbType};
use Index\Db\Sqlite\SqliteConnection;

/**
 * Provides a common interface for database migrations.
 */
class DbMigrationManager {
    /**
     * Default table name for migrations.
     *
     * @var string
     */
    public const string DEFAULT_TABLE = 'ndx_migrations';

    private const string CREATE_TRACK_TABLE = 'CREATE TABLE IF NOT EXISTS %1$s (migration_name %2$s PRIMARY KEY, migration_completed %2$s NOT NULL);';
    private const string CREATE_TRACK_INDEX = 'CREATE INDEX IF NOT EXISTS %1$s_completed_index ON %1$s (migration_completed);';
    private const string DESTROY_TRACK_TABLE = 'DROP TABLE IF EXISTS %s;';

    private const string CHECK_STMT = 'SELECT migration_completed IS NOT NULL FROM %s WHERE migration_name = ?;';
    private const string INSERT_STMT = 'INSERT INTO %s (migration_name, migration_completed) VALUES (?, ?);';

    private const string TEMPLATE = <<<EOF
<?php
use Index\Db\DbConnection;
use Index\Db\Migration\DbMigration;

final class %s implements DbMigration {
    public function migrate(DbConnection \$conn): void {
        \$conn->execute('CREATE TABLE ...');
    }
}

EOF;

    private ?DbStatement $checkStmt = null;
    private ?DbStatement $insertStmt = null;

    /**
     * @param DbConnection $conn Connection to apply to migrations to.
     * @param string $tableName Name of the migration tracking table.
     */
    public function __construct(
        private DbConnection $conn,
        private string $tableName = self::DEFAULT_TABLE,
    ) {}

    /**
     * Creates the migration tracking table if it doesn't exist.
     */
    public function createTrackingTable(): void {
        // HACK: this is not ok but it works for now, there should probably be a generic type bag alias thing
        // TODO: ACTUALLY get rid of this
        $nameType = $this->conn instanceof SqliteConnection ? 'TEXT' : 'VARCHAR(255)';

        $this->conn->execute(sprintf(self::CREATE_TRACK_TABLE, $this->tableName, $nameType));
        $this->conn->execute(sprintf(self::CREATE_TRACK_INDEX, $this->tableName));
    }

    /**
     * Deletes the migration tracking table.
     */
    public function destroyTrackingTable(): void {
        $this->conn->execute(sprintf(self::DESTROY_TRACK_TABLE, $this->tableName));
    }

    /**
     * Prepares statements required for migrations.
     */
    public function prepareStatements(): void {
        $this->checkStmt = $this->conn->prepare(sprintf(self::CHECK_STMT, $this->tableName));
        $this->insertStmt = $this->conn->prepare(sprintf(self::INSERT_STMT, $this->tableName));
    }

    /**
     * Runs all preparations required for running migrations.
     */
    public function init(): void {
        $this->createTrackingTable();
        $this->prepareStatements();
    }

    /**
     * Checks if a particular migration is present in the tracking table.
     *
     * @param string $name Name of the migration.
     * @throws RuntimeException If the migration manager has not been initialised.
     * @return bool true if the migration has been run, false otherwise.
     */
    public function checkMigration(string $name): bool {
        if($this->checkStmt === null)
            throw new RuntimeException('Database migration manager has not been initialised.');

        $this->checkStmt->reset();
        $this->checkStmt->nextParameter($name, DbType::String);
        $this->checkStmt->execute();

        $result = $this->checkStmt->getResult();
        return $result->next() && !$result->isNull(0);
    }

    /**
     * Marks a migration as completed in the tracking table.
     *
     * @param string $name Name of the migration.
     * @param ?DateTimeInterface $dateTime Timestamp of when the migration was run, null for now.
     * @throws RuntimeException If the migration manager has not been initialised.
     */
    public function completeMigration(string $name, ?DateTimeInterface $dateTime = null): void {
        if($this->insertStmt === null)
            throw new RuntimeException('Database migration manager has not been initialised.');

        $dateTime = XDateTime::toIso8601String($dateTime);

        $this->insertStmt->reset();
        $this->insertStmt->nextParameter($name, DbType::String);
        $this->insertStmt->nextParameter($dateTime, DbType::String);
        $this->insertStmt->execute();
    }

    /**
     * Generates a template for a migration PHP file.
     *
     * @param string $name Name of the migration.
     * @return string Migration template.
     */
    public function template(string $name): string {
        return sprintf(self::TEMPLATE, $name);
    }

    /**
     * Generates a filename for a migration PHP file.
     *
     * @param string $name Name of the migration.
     * @param ?DateTimeInterface $dateTime Timestamp of when the migration was created, null for now.
     * @throws InvalidArgumentException If $name is empty.
     * @throws InvalidArgumentException If $name contains invalid characters.
     * @return string Migration filename.
     */
    public function createFileName(string $name, ?DateTimeInterface $dateTime = null): string {
        if(empty($name))
            throw new InvalidArgumentException('$name may not be empty.');

        $name = str_replace(' ', '_', strtolower($name));
        if(!preg_match('#^([a-z_]+)$#', $name))
            throw new InvalidArgumentException('$name may only contain alphabetical, spaces and _ characters.');

        $dateTime ??= new DateTimeImmutable('now');

        return $dateTime->format('Y_m_d_His_') . trim($name, '_');
    }

    /**
     * Generates a class name for a migration PHP file.
     *
     * @param string $name Name of the migration.
     * @param ?DateTimeInterface $dateTime Timestamp of when the migration was created, null for now.
     * @throws InvalidArgumentException If $name is empty.
     * @throws InvalidArgumentException If $name contains invalid characters.
     * @return string Migration class name.
     */
    public function createClassName(string $name, ?DateTimeInterface $dateTime = null): string {
        if(empty($name))
            throw new InvalidArgumentException('$name may not be empty.');

        $name = str_replace(' ', '_', strtolower($name));
        if(!preg_match('#^([a-z_]+)$#', $name))
            throw new InvalidArgumentException('$name may only contain alphabetical, spaces and _ characters.');

        $dateTime ??= new DateTimeImmutable('now');

        $parts = explode('_', trim($name, '_'));
        $name = '';

        foreach($parts as $part)
            $name .= ucfirst($part);

        return $name . $dateTime->format('_Ymd_His');
    }

    /**
     * Generate names for a PHP migration file.
     *
     * @param string $baseName Name of the migration.
     * @param ?DateTimeInterface $dateTime Timestamp of when the migration was created, null for now.
     * @return object Object containing names for a PHP migration file.
     */
    public function createNames(string $baseName, ?DateTimeInterface $dateTime = null): object {
        $dateTime ??= new DateTimeImmutable('now');

        $names = new stdClass;
        $names->name = $this->createFileName($baseName, $dateTime);
        $names->className = $this->createClassName($baseName, $dateTime);

        return $names;
    }

    /**
     * Runs all migrations present in a migration repository. This process is irreversible!
     *
     * @param DbMigrationRepo $migrations Migrations repository to fetch migrations from.
     * @return string[] Names of migrations that have been completed.
     */
    public function processMigrations(DbMigrationRepo $migrations): array {
        $migrations = $migrations->getMigrations();
        $completed = [];

        foreach($migrations as $migration) {
            if($this->checkMigration($migration->name))
                continue;

            $migration->migrate($this->conn);
            $this->completeMigration($migration->name);
            $completed[] = $migration->name;
        }

        return $completed;
    }
}

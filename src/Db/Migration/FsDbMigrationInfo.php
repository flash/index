<?php
// FsDbMigrationInfo.php
// Created: 2023-01-07
// Updated: 2025-01-18

namespace Index\Db\Migration;

use Index\Db\DbConnection;

class FsDbMigrationInfo implements DbMigrationInfo {
    public private(set) string $name;
    public private(set) string $className;

    /**
     * @param string $path Filesystem path to the migration file.
     */
    public function __construct(
        private string $path
    ) {
        $this->name = $name = pathinfo($path, PATHINFO_FILENAME);

        $dateTime = substr($name, 0, 17);
        $dateTime = str_replace('_', '', substr($dateTime, 0, 9)) . substr($dateTime, -8);

        $classParts = explode('_', substr($name, 18));
        $className = '';

        foreach($classParts as $part)
            $className .= ucfirst($part);

        $this->className = $className . '_' . $dateTime;
    }

    public function migrate(DbConnection $conn): void {
        require_once $this->path;

        $migration = new $this->className;
        if($migration instanceof DbMigration)
            $migration->migrate($conn);
    }
}

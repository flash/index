<?php
// DbMigrationInfo.php
// Created: 2023-01-07
// Updated: 2025-01-18

namespace Index\Db\Migration;

use Index\Db\DbConnection;

/**
 * Information on a migration repository item.
 */
interface DbMigrationInfo {
    /**
     * Name of the migration.
     *
     * @var string
     */
    public string $name { get; }

    /**
     * Class name of the migration.
     *
     * @var string
     */
    public string $className { get; }

    /**
     * Creates an instance of the underlying migration and runs it. This process is irreversible!
     *
     * @param DbConnection $conn Database connection to execute this migration on.
     */
    public function migrate(DbConnection $conn): void;
}

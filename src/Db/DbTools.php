<?php
// DbTools.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db;

use Countable;

/**
 * Common database actions.
 */
final class DbTools {
    /**
     * Constructs a partial query for prepared statements of lists.
     *
     * @param Countable|mixed[]|int $count Amount of times to repeat the string in the $repeat parameter.
     * @param string $repeat String to repeat.
     * @param string $glue Glue character.
     * @return string Glued string ready for being thrown into your prepared statement.
     */
    public static function prepareListString(Countable|array|int $count, string $repeat = '?', string $glue = ', '): string {
        if(is_countable($count))
            $count = count($count);
        return implode($glue, array_fill(0, $count, $repeat));
    }

    /**
     * Detects the DbType of the passed argument. Should be used for DbType::Auto.
     *
     * @param mixed $value A value of unknown type.
     * @return DbType Type of the value passed in the argument.
     */
    public static function detectType(mixed $value): DbType {
        if(is_null($value))
            return DbType::Null;
        if(is_float($value))
            return DbType::Float;
        if(is_int($value))
            return DbType::Int;
        if(is_resource($value))
            return DbType::Blob;
        return DbType::String;
    }
}

<?php
// DbStatement.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db;

use InvalidArgumentException;
use RuntimeException;

/**
 * Represents a prepared database statement.
 */
interface DbStatement {
    /**
     * How many parameters there are.
     *
     * @var int
     */
    public int $paramCount { get; }

    /**
     * Assigns a value to a parameter.
     *
     * @param int $ordinal Index of the target parameter.
     * @param mixed $value Value to assign to the parameter.
     * @param DbType $type Type of the value, if left to DbType::Auto DbTools::detectType will be used on $value.
     * @throws InvalidArgumentException If $ordinal exceeds bounds.
     */
    public function addParameter(int $ordinal, mixed $value, DbType $type = DbType::Auto): void;

    /**
     * Assigns a value to the next parameter.
     *
     * Behaviour of this method may be slightly undefined if addParameter is used at the same time.
     * Overwriting lower ordinals than the current cursor should be fine, but your mileage may vary.
     *
     * @param mixed $value Value to assign to the parameter.
     * @param DbType $type Type of the value, if left to DbType::Auto DbTools::detectType will be used on $value.
     * @throws RuntimeException If all parameters have already been specified.
     */
    public function nextParameter(mixed $value, DbType $type = DbType::Auto): void;

    /**
     * Gets the result after execution.
     *
     * @throws RuntimeException If there is no result available.
     * @return DbResult Instance of an implementation of DbResult.
     */
    public function getResult(): DbResult;

    /**
     * Gets a result iterator after execution.
     *
     * @param callable(DbResult): object $construct Result info constructor.
     * @throws RuntimeException If there is no result available.
     * @return DbResultIterator Iterator wrapping an instance of an implementation of DbResult.
     */
    public function getResultIterator(callable $construct): DbResultIterator;

    /**
     * ID of the last inserted row.
     *
     * @var int|string
     */
    public int|string $lastInsertId { get; }

    /**
     * Executes this statement.
     *
     * @return int|string Number of rows affected by the query.
     */
    public function execute(): int|string;

    /**
     * Resets this statement for reuse.
     */
    public function reset(): void;
}

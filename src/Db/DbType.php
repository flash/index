<?php
// DbType.php
// Created: 2021-05-02
// Updated: 2025-01-18

namespace Index\Db;

/**
 * Map of common database types.
 */
enum DbType {
    /**
     * Automatically detect the type. Should be used in combination with DbTools::detectType.
     */
    case Auto;

    /**
     * Represents a NULL value. If this type is specified, the value it was associated with should be overriden with NULL.
     */
    case Null;

    /**
     * An integer type.
     */
    case Int;

    /**
     * A double precision floating point.
     */
    case Float;

    /**
     * A textual string.
     */
    case String;

    /**
     * Binary blob data.
     */
    case Blob;
}

<?php
// DbBackend.php
// Created: 2021-04-30
// Updated: 2025-01-18

namespace Index\Db;

/**
 * Information about a database layer. Should not have any external dependencies.
 */
interface DbBackend {
    /**
     * Whether the driver is available and a connection can be made.
     *
     * @var bool
     */
    public bool $available { get; }

    /**
     * Creates a connection with the database described in the argument.
     *
     * @param DbConnectionInfo $connectionInfo Object that describes the desired connection.
     * @throws \InvalidArgumentException An invalid implementation of DbConnectionInfo was provided.
     * @return DbConnection A connection described in the connection info.
     */
    function createConnection(DbConnectionInfo $connectionInfo): DbConnection;

    /**
     * Constructs a connection info instance from a dsn.
     *
     * @param string|array<string, int|string> $dsn DSN with connection information.
     * @return DbConnectionInfo Connection info based on the dsn.
     */
    function parseDsn(string|array $dsn): DbConnectionInfo;
}

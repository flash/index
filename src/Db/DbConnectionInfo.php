<?php
// DbConnectionInfo.php
// Created: 2021-04-30
// Updated: 2024-10-04

namespace Index\Db;

/**
 * Base type for database connection info.
 *
 * Any database backend should have its own implementation of this, there are no baseline requirements.
 */
interface DbConnectionInfo {}

<?php
// DbConnection.php
// Created: 2021-04-30
// Updated: 2025-01-18

namespace Index\Db;

use RuntimeException;

/**
 * Represents a connection to a database service.
 */
interface DbConnection {
    /**
     * ID of the last inserted row.
     *
     * @var int|string
     */
    public int|string $lastInsertId { get; }

    /**
     * Number of rows affected by the last operation.
     *
     * @var int|string
     */
    public int|string $affectedRows { get; }

    /**
     * Prepares a statement for execution and returns a database statement instance.
     *
     * The statement should use question mark (?) parameters.
     *
     * @param string $query SQL query to prepare.
     * @return DbStatement An instance of an implementation of DbStatement.
     */
    public function prepare(string $query): DbStatement;

    /**
     * Executes a statement and returns a database result instance.
     *
     * @param string $query SQL query to execute.
     * @return DbResult An instance of an implementation of DbResult
     */
    public function query(string $query): DbResult;

    /**
     * Executes a statement and returns how many rows are affected.
     *
     * Does not request results back from the database and thus should have better
     *  performance if the consumer doesn't care about them.
     *
     * @param string $query SQL query to execute.
     * @return int|string Number of rows affected by the query.
     */
    public function execute(string $query): int|string;

    /**
     * Begins a transaction.
     *
     * @throws RuntimeException If beginning the transaction failed.
     */
    public function beginTransaction(): DbTransaction;
}

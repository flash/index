<?php
// DbStatementCommon.php
// Created: 2025-01-18
// Updated: 2025-01-18

namespace Index\Db;

/**
 * Implements common DbStatement methods.
 */
trait DbStatementCommon {
    public function getResultIterator(callable $construct): DbResultIterator {
        return $this->getResult()->getIterator($construct);
    }
}

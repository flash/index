<?php
// CacheProvider.php
// Created: 2024-04-10
// Updated: 2025-01-18

namespace Index\Cache;

/**
 * Represents a cache provider.
 */
interface CacheProvider {
    /**
     * Retrieve an item from the cache.
     *
     * @param string $key Key under which the value is stored.
     * @return mixed value stored in the cache or null otherwise.
     */
    public function get(string $key): mixed;

    /**
     * Store an item in the cache.
     *
     * @param string $key Key under which to store the value.
     * @param mixed $value Value to store.
     * @param int $ttl Amount of seconds to store the value for, 0 for indefinite.
     * @throws \RuntimeException if storing the value in the cache failed.
     * @throws \InvalidArgumentException if an argument is incorrectly formatted, e.g. $ttl set to a negative value.
     */
    public function set(string $key, mixed $value, int $ttl = 0): void;

    /**
     * Deletes an item from the cache.
     *
     * @param string $key Key to be deleted.
     * @throws \RuntimeException if an error occurred during deletion.
     */
    public function delete(string $key): void;

    /**
     * Sets a new expiration time on an item.
     *
     * @param string $key Key to be touched.
     * @param int $ttl Amount of seconds to store the value for, 0 for indefinite.
     * @throws \RuntimeException if storing the value in the cache failed.
     * @throws \InvalidArgumentException if an argument is incorrectly formatted, e.g. $ttl set to a negative value.
     */
    public function touch(string $key, int $ttl = 0): void;

    /**
     * Increments an item.
     *
     * @param string $key Key to be incremented.
     * @param int $amount Amount to increment by. Defaults to 1.
     * @throws \RuntimeException if storing the value in the cache failed.
     * @return int new, incremented value.
     */
    public function increment(string $key, int $amount = 1): int;

    /**
     * Decrements an item.
     *
     * @param string $key Key to be decremented.
     * @param int $amount Amount to decrement by. Defaults to 1.
     * @throws \RuntimeException if storing the value in the cache failed.
     * @return int new, decremented value.
     */
    public function decrement(string $key, int $amount = 1): int;
}

<?php
// _ndx.php
// Created: 2024-10-18
// Updated: 2024-10-18

namespace Index\Cache\ArrayCache;

use Index\Cache\CacheBackends;

CacheBackends::register('array', ArrayCacheBackend::class);
CacheBackends::register('null', ArrayCacheBackend::class);

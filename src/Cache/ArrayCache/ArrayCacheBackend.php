<?php
// ArrayCacheBackend.php
// Created: 2024-04-10
// Updated: 2025-01-18

namespace Index\Cache\ArrayCache;

use InvalidArgumentException;
use Index\Cache\{CacheBackend,CacheProvider,CacheProviderInfo};

/**
 * Information about the dummy cache backend.
 */
class ArrayCacheBackend implements CacheBackend {
    public private(set) bool $available = true;

    /**
     * Creates a dummy cache provider.
     *
     * @param ArrayCacheProviderInfo $providerInfo Dummy provider info.
     * @return ArrayCacheProvider Dummy provider instance.
     */
    public function createProvider(CacheProviderInfo $providerInfo): CacheProvider {
        if(!($providerInfo instanceof ArrayCacheProviderInfo))
            throw new InvalidArgumentException('$providerInfo must by of type ArrayCacheProviderInfo');

        return new ArrayCacheProvider;
    }

    /**
     * Constructs a cache info instance from a dsn.
     *
     * ArrayCache has no parameters that can be controlled using the DSN.
     *
     * @param string|array<string, string|int> $dsn DSN with provider information.
     * @return ArrayCacheProviderInfo Dummy provider info instance.
     */
    public function parseDsn(string|array $dsn): CacheProviderInfo {
        return new ArrayCacheProviderInfo;
    }
}

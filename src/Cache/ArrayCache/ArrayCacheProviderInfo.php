<?php
// ArrayCacheProviderInfo.php
// Created: 2024-04-10
// Updated: 2024-10-02

namespace Index\Cache\ArrayCache;

use Index\Cache\CacheProviderInfo;

/**
 * Represents dummy provider info.
 */
class ArrayCacheProviderInfo implements CacheProviderInfo {}

<?php
// ValkeyProvider.php
// Created: 2024-04-10
// Updated: 2025-01-18

namespace Index\Cache\Valkey;

use InvalidArgumentException;
use Redis;
use Index\Cache\CacheProvider;
use Index\Net\{DnsEndPoint,UnixEndPoint};

/**
 * Valkey provider implementation.
 */
class ValkeyProvider implements CacheProvider {
    private Redis $redis;
    private bool $persist;

    public function __construct(ValkeyProviderInfo $providerInfo) {
        $this->persist = $providerInfo->persist;
        $this->redis = new Redis;
        $this->redis->setOption(Redis::OPT_PREFIX, $providerInfo->prefix);
        $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);

        if($this->persist)
            $this->redis->pconnect($providerInfo->serverHost, $providerInfo->serverPort);
        else
            $this->redis->connect($providerInfo->serverHost, $providerInfo->serverPort);

        if(!empty($providerInfo->password)) {
            if(!empty($providerInfo->username))
                $this->redis->auth([$providerInfo->username, $providerInfo->password]);
            else
                $this->redis->auth($providerInfo->password);
        }

        if($providerInfo->dbNumber !== 0)
            $this->redis->select($providerInfo->dbNumber);
    }

    public function get(string $key): mixed {
        $result = $this->redis->get($key);
        return $result === false ? null : $result;
    }

    public function set(string $key, mixed $value, int $ttl = 0): void {
        if($ttl < 0)
            throw new InvalidArgumentException('$ttl must be equal to or greater than 0.');

        $this->redis->setEx($key, $ttl, $value);
    }

    public function delete(string $key): void {
        $this->redis->unlink($key);
    }

    public function touch(string $key, int $ttl = 0): void {
        if($ttl < 0)
            throw new InvalidArgumentException('$ttl must be equal to or greater than 0.');

        $this->redis->expire($key, $ttl);
    }

    public function increment(string $key, int $amount = 1): int {
        $result = $this->redis->incrBy($key, $amount);
        return is_int($result) ? $result : 0;
    }

    public function decrement(string $key, int $amount = 1): int {
        $result = $this->redis->decrBy($key, $amount);
        return is_int($result) ? $result : 0;
    }

    public function __destruct() {
        if(!$this->persist)
            $this->redis->close();
    }
}

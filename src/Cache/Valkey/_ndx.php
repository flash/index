<?php
// _ndx.php
// Created: 2024-10-18
// Updated: 2024-10-18

namespace Index\Cache\Valkey;

use Index\Cache\CacheBackends;

CacheBackends::register('valkey', ValkeyBackend::class);
CacheBackends::register('redis', ValkeyBackend::class);
CacheBackends::register('keydb', ValkeyBackend::class);

<?php
// ValkeyProviderInfo.php
// Created: 2024-04-10
// Updated: 2025-02-27

namespace Index\Cache\Valkey;

use InvalidArgumentException;
use RuntimeException;
use Index\Cache\CacheProviderInfo;
use Index\Net\{DnsEndPoint,EndPoint,IpEndPoint,UnixEndPoint};

/**
 * Represents Valkey provider info.
 */
class ValkeyProviderInfo implements CacheProviderInfo {
    /**
     * @param EndPoint $endPoint Server endpoint.
     * @param string $prefix Prefix that gets applied to every key.
     * @param bool $persist Whether the connection should be persistent.
     * @param string $username Username to authenticate with.
     * @param string $password Password to authenticate with.
     * @param int $dbNumber Database that should be selected.
     */
    public function __construct(
        public private(set) EndPoint $endPoint,
        public private(set) string $prefix,
        public private(set) bool $persist,
        public private(set) string $username,
        #[\SensitiveParameter] public private(set) string $password,
        public private(set) int $dbNumber,
    ) {}

    /**
     * Retrieves the server hostname.
     *
     * @throws RuntimeException Unsupported endpoint specified.
     * @var string
     */
    public string $serverHost {
        get {
            if($this->endPoint instanceof UnixEndPoint)
                return $this->endPoint->path;
            if($this->endPoint instanceof DnsEndPoint)
                return $this->endPoint->host;
            if($this->endPoint instanceof IpEndPoint)
                return $this->endPoint->address->clean;

            throw new RuntimeException('EndPoint type is not supported.');
        }
    }

    /**
     * Retrieves the server Unix path.
     *
     * @var int
     */
    public int $serverPort {
        get {
            if($this->endPoint instanceof DnsEndPoint || $this->endPoint instanceof IpEndPoint)
                return $this->endPoint->port;

            return 0;
        }
    }
}

<?php
// MemcachedProviderInfo.php
// Created: 2024-04-10
// Updated: 2025-02-27

namespace Index\Cache\Memcached;

use InvalidArgumentException;
use Index\Cache\CacheProviderInfo;
use Index\Net\EndPoint;

/**
 * Represents Memcached provider info.
 */
class MemcachedProviderInfo implements CacheProviderInfo {
    /**
     * @param array<int, array{EndPoint, int}> $endPoints Memcached end points.
     * @param string $prefixKey Prefix to apply to keys.
     * @param string $persistName Persistent connection name.
     * @param bool $useBinaryProto Use the binary protocol.
     * @param bool $enableCompression Use compression.
     * @param bool $tcpNoDelay Disable Nagle's algorithm.
     */
    public function __construct(
        public private(set) array $endPoints,
        public private(set) string $prefixKey,
        private string $persistName,
        public private(set) bool $useBinaryProto,
        public private(set) bool $enableCompression,
        public private(set) bool $tcpNoDelay,
    ) {}

    /**
     * Whether the connection should be persistent.
     *
     * @var bool
     */
    public bool $persistent {
        get => $this->persistName !== '';
    }

    /**
     * Name of persistent connection, will return null if disabled.
     *
     * @var ?string
     */
    public ?string $persistentName {
        get => $this->persistName === '' ? null : $this->persistName;
    }
}

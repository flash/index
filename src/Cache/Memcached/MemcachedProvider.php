<?php
// MemcachedProvider.php
// Created: 2024-04-10
// Updated: 2025-01-18

namespace Index\Cache\Memcached;

use Index\Cache\CacheProvider;

/**
 * Base Memcached provider implementation.
 */
abstract class MemcachedProvider implements CacheProvider {
    public const int MAX_TTL = 30 * 24 * 60 * 60;

    abstract public function get(string $key): mixed;
    abstract public function set(string $key, mixed $value, int $ttl = 0): void;
    abstract public function delete(string $key): void;
    abstract public function touch(string $key, int $ttl = 0): void;
    abstract public function increment(string $key, int $amount = 1): int;
    abstract public function decrement(string $key, int $amount = 1): int;
}

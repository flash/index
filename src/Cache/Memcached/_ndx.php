<?php
// _ndx.php
// Created: 2024-10-18
// Updated: 2024-10-18

namespace Index\Cache\Memcached;

use Index\Cache\CacheBackends;

CacheBackends::register('memcached', MemcachedBackend::class);
CacheBackends::register('memcache', MemcachedBackend::class);

<?php
// CacheProviderInfo.php
// Created: 2024-04-10
// Updated: 2024-10-02

namespace Index\Cache;

/**
 * Base type for cache provider info.
 *
 * Any cache backend should have its own implementation of this, there are no baseline requirements.
 */
interface CacheProviderInfo {}

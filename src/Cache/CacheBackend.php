<?php
// CacheBackend.php
// Created: 2024-04-10
// Updated: 2025-01-18

namespace Index\Cache;

/**
 * Information about a cache provider. Should not have any external dependencies.
 */
interface CacheBackend {
    /**
     * Checks whether the driver is available and a provider can be made.
     *
     * @var bool If true a provider can be made, if false a required extension is missing.
     */
    public bool $available { get; }

    /**
     * Creates the cache provider described in the argument.
     *
     * @param CacheProviderInfo $providerInfo Object that describes the desired provider.
     * @throws \InvalidArgumentException An invalid implementation of CacheProviderInfo was provided.
     * @throws \RuntimeException If you ignored the output of isAvailable and tried to create an instance anyway.
     * @return CacheProvider The provider described in the provider info.
     */
    public function createProvider(CacheProviderInfo $providerInfo): CacheProvider;

    /**
     * Constructs a cache info instance from a dsn.
     *
     * @param string|array<string, string|int> $dsn DSN with provider information.
     * @return CacheProviderInfo Provider info based on the dsn.
     */
    public function parseDsn(string|array $dsn): CacheProviderInfo;
}

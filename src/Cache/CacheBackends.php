<?php
// CacheBackends.php
// Created: 2024-10-18
// Updated: 2025-01-18

namespace Index\Cache;

use InvalidArgumentException;
use RuntimeException;

/**
 * Provides a registry of cache backends.
 */
final class CacheBackends {
    /** @var array<string, string> */
    private static array $schemes = [];

    /** @var array<string, CacheBackend> */
    private static array $backends = [];

    /**
     * @throws RuntimeException Will always throw because this is a static class exclusively.
     */
    public function __construct() {
        throw new RuntimeException('This is a static class, you cannot create an instance of it.');
    }

    /**
     * Registers a cache backend with a protocol scheme.
     *
     * @param string $scheme URL scheme.
     * @param string $className Fully qualified class name of the backend.
     * @throws InvalidArgumentException If $scheme is already registered.
     */
    public static function register(string $scheme, string $className): void {
        $scheme = strtolower($scheme);
        if(array_key_exists($scheme, self::$schemes))
            throw new InvalidArgumentException('$scheme has already been registered');

        self::$schemes[$scheme] = $className;
    }

    /**
     * Creates a cache provider from a DSN URI.
     *
     * @param string $uri DSN URI.
     * @throws RuntimeException If the requested cache backend is not available.
     * @return CacheProvider Connection instance.
     */
    public static function create(string $uri): CacheProvider {
        $parsed = self::parseUri($uri);

        $backend = self::backend($parsed['scheme']);
        if(!$backend->available)
            throw new RuntimeException('requested cache backend is not available');

        return $backend->createProvider($backend->parseDsn($parsed));
    }

    /**
     * Parses a DSN URI into a provider info class.
     *
     * @param string $uri DSN URI.
     * @return CacheProviderInfo Provider info instance.
     */
    public static function parse(string $uri): CacheProviderInfo {
        $parsed = self::parseUri($uri);
        return self::backend($parsed['scheme'])->parseDsn($parsed);
    }

    /**
     * Creates a cache provider information class from a scheme.
     *
     * @param string $scheme Cache protocol scheme.
     * @throws InvalidArgumentException If $scheme is not a registered protocol scheme.
     * @throws RuntimeException If the implementation class is not available or doesn't implement CacheBackend.
     * @return CacheBackend Cache backend information instance.
     */
    public static function backend(string $scheme): CacheBackend {
        $scheme = strtolower($scheme);
        if(!array_key_exists($scheme, self::$schemes))
            throw new InvalidArgumentException('$scheme is not a registered scheme');

        $className = self::$schemes[$scheme];
        if(array_key_exists($className, self::$backends))
            return self::$backends[$className];

        if(!class_exists($className))
            throw new RuntimeException('implementation class for the given $scheme does not exist');

        $backend = new $className;
        if(!($backend instanceof CacheBackend))
            throw new RuntimeException('implementation class for the given $scheme does not implement CacheBackend');

        self::$backends[$className] = $backend;
        return $backend;
    }

    /** @return array{scheme: string, host?: string, port?: int, user?: string, pass?: string, path?: string, query?: string, fragment?: string} */
    private static function parseUri(string $uri): array {
        $uri = parse_url($uri);
        if($uri === false)
            throw new InvalidArgumentException('$uri is not a valid uri');
        if(empty($uri['scheme']))
            throw new InvalidArgumentException('$uri must contain a non-empty scheme component');
        return $uri;
    }
}

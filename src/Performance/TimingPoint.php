<?php
// TimingPoint.php
// Created: 2022-02-16
// Updated: 2025-02-27

namespace Index\Performance;

/**
 * Represents a timing point.
 */
class TimingPoint {
    /**
     * @param int $timePoint Starting tick count.
     * @param int $duration Duration tick count.
     * @param string $name Name of the timing point.
     * @param string $comment Timing point comment.
     */
    public function __construct(
        public private(set) int $timePoint,
        public private(set) int $duration,
        public private(set) string $name,
        public private(set) string $comment
    ) {}

    /**
     * Duration time amount.
     *
     * @var int|float
     */
    public int|float $durationTime {
        get => $this->duration / PerformanceCounter::frequency();
    }
}

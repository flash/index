<?php
// Timings.php
// Created: 2022-02-16
// Updated: 2025-02-27

namespace Index\Performance;

use InvalidArgumentException;

/**
 * Represents a Stopwatch with timing points.
 */
class Timings {
    public private(set) Stopwatch $stopwatch;
    private int $lastLapTicks;

    /**
     * @param ?Stopwatch $stopwatch Stopwatch to measure timing points from, null to start a new one.
     */
    public function __construct(?Stopwatch $stopwatch = null) {
        $this->stopwatch = $stopwatch ?? Stopwatch::startNew();
        $this->lastLapTicks = $this->stopwatch->elapsedTicks;
    }

    /**
     * Timing points.
     *
     * @var TimingPoint[]
     */
    public private(set) array $laps = [];

    /**
     * Starts the underlying stopwatch.
     */
    public function start(): void {
        $this->stopwatch->start();
    }

    /**
     * Stops the underlying stopwatch.
     */
    public function stop(): void {
        $this->stopwatch->stop();
    }

    /**
     * Whether the underlying stopwatch is running.
     *
     * @var bool
     */
    public bool $running {
        get => $this->stopwatch->running;
    }

    /**
     * Record a timing point.
     *
     * @param string $name Timing point name, must be alphanumeric.
     * @param string $comment Timing point comment.
     * @throws InvalidArgumentException If $name is not alphanumeric.
     */
    public function lap(string $name, string $comment = ''): void {
        if(!ctype_alnum($name))
            throw new InvalidArgumentException('$name must be alphanumeric.');

        $lapTicks = $this->stopwatch->elapsedTicks;
        $elapsed = $lapTicks - $this->lastLapTicks;
        $this->lastLapTicks = $lapTicks;

        $this->laps[] = new TimingPoint(
            $lapTicks,
            $elapsed,
            $name,
            $comment
        );
    }
}

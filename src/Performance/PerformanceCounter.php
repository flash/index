<?php
// PerformanceCounter.php
// Created: 2022-02-16
// Updated: 2025-01-18

namespace Index\Performance;

/**
 * Represents a performance counter.
 */
final class PerformanceCounter {
    /**
     * Timer frequency in ticks per second.
     *
     * @return int Timer frequency.
     */
    public static function frequency(): int {
        return 1000000;
    }

    /**
     * Current ticks from the system.
     *
     * @return int Ticks count.
     */
    public static function ticks(): int {
        return (int)hrtime(true);
    }

    /**
     * Ticks elapsed since the given value.
     *
     * @param int $since Starting ticks value.
     * @return int Ticks count since.
     */
    public static function ticksSince(int $since): int {
        return self::ticks() - $since;
    }
}

<?php
// Stopwatch.php
// Created: 2021-04-26
// Updated: 2025-02-27

namespace Index\Performance;

/**
 * Provides a means to measure elapsed time at a high resolution.
 */
class Stopwatch {
    private int $start = -1;
    private int $elapsed = 0;
    private int $frequency;

    public function __construct() {
        $this->frequency = PerformanceCounter::frequency();
    }

    /**
     * Start time measurement.
     */
    public function start(): void {
        if($this->start < 0)
            $this->start = PerformanceCounter::ticks();
    }

    /**
     * Stop time measurement.
     */
    public function stop(): void {
        if($this->start < 0)
            return;
        $this->elapsed += PerformanceCounter::ticksSince($this->start);
        $this->start = -1;
    }

    /**
     * Whether the Stopwatch is currently running.
     *
     * @var bool
     */
    public bool $running {
        get => $this->start >= 0;
    }

    /**
     * Number of ticks that have elapsed.
     *
     * @var int
     */
    public int $elapsedTicks {
        get {
            $elapsed = $this->elapsed;
            if($this->start >= 0)
                $elapsed += PerformanceCounter::ticksSince($this->start);
            return $elapsed;
        }
    }

    /**
     * Number of elapsed milliseconds.
     *
     * @var int|float
     */
    public int|float $elapsedTime {
        get => $this->elapsedTicks / $this->frequency;
    }

    /**
     * Creates a new Stopwatch instance and starts measuring time.
     *
     * @return Stopwatch Newly created running Stopwatch.
     */
    public static function startNew(): Stopwatch {
        $sw = new Stopwatch;
        $sw->start();
        return $sw;
    }
}

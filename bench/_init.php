<?php
require_once __DIR__ . '/../index.php';

function bench(string $name, int $times, callable $body): void {
    printf('Running "%s" %d times%s', $name, $times, PHP_EOL);

    $sw = \Index\Performance\Stopwatch::startNew();

    while(--$times > 0)
        $body();

    $sw->stop();

    printf('Took: %Fms %s', $sw->getElapsedTime(), PHP_EOL);

    echo PHP_EOL;
}
